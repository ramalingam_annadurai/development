package com.site.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.bson.types.ObjectId;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;

@SuppressWarnings("serial")
@Entity
@Table(name = "user")
public class User implements Serializable, UserDetails {

	private Long id;
	private UUID token;
	private String userName;
	private String emailAddress;
	private Boolean active;
	private String password;
	private String confirmPassword;
	private String firstName;
	private String lastName;
	private String gender;
	private Date createDate;
	private Date updateDate;
	private String accountType;
	private ObjectId photoId;
	private byte[] photo;
	private String permanentAddress;
	private String shippingAddress;
	private String phoneNumber;

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	@Id
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public UUID getToken() {
		return token;
	}

	public void setToken(UUID token) {
		this.token = token;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getCreateDate() {
		return createDate;
	}

	@Transient
	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	private com.site.util.UserRole userRole;

	@Override
	@Transient
	public Collection<? extends GrantedAuthority> getAuthorities() {

		GrantedAuthority auth = new GrantedAuthorityImpl("User");
		Collection<GrantedAuthority> authorities = new LinkedList<GrantedAuthority>();
		authorities.add(auth);
		return authorities;
	}

	public com.site.util.UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(com.site.util.UserRole roleCorporate) {
		this.userRole = roleCorporate;
	}

	@Override
	@Transient
	public String getUsername() {
		return userName;
	}

	@Override
	@Transient
	public boolean isAccountNonLocked() {
		return active;
	}

	@Override
	@Transient
	public boolean isCredentialsNonExpired() {
		return active;
	}

	@Override
	@Transient
	public boolean isEnabled() {
		return active;
	}

	@Override
	@Transient
	public boolean isAccountNonExpired() {
		return active;
	}

	public void setParameter(String string, String username2) {

	}

	public ObjectId getPhotoId() {
		return photoId;
	}

	public void setPhotoId(ObjectId photoId) {
		this.photoId = photoId;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", token=" + token + ", userName=" + userName + ", emailAddress=" + emailAddress
				+ ", active=" + active + ", password=" + password + ", confirmPassword=" + confirmPassword
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", gender=" + gender + ", createDate="
				+ createDate + ", updateDate=" + updateDate + ", accountType=" + accountType + ", photoId=" + photoId
				+ ", photo=" + Arrays.toString(photo) + ", permanentAddress=" + permanentAddress + ", shippingAddress="
				+ shippingAddress + ", userRole=" + userRole + "]";
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}