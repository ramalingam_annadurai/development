package com.site.entity;

import java.awt.image.DataBufferByte;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.bson.types.ObjectId;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;

@SuppressWarnings({ "serial", "deprecation" })
@Entity
@Table(name = "addtocart")
public class AddToCart implements Serializable {
	private Long Id;
	private Long userId;
	private Long productId;
	private Long quantity;
	private Long gid;
	
	@Id
	@GeneratedValue
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	/*public Product getPro() {
		return pro;
	}

	public void setPro(Product pro) {
		this.pro = pro;
	}*/


	public Long getUserId() {
		return userId;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Column(name = "productId")
	
	public Long getProductId() {
		return productId;
	}
	/*@OneToMany(cascade = CascadeType.ALL)
	    @JoinTable(
	            name = "addtocart_product",
	            joinColumns = @JoinColumn(name = "productId"),
	            inverseJoinColumns = @JoinColumn(name = "id")
	    )*/
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Long getGid() {
		return gid;
	}

	public void setGid(Long gid) {
		this.gid = gid;
	}

	@Override
	public String toString() {
		return "AddToCart [Id=" + Id + ", userId=" + userId + ", productId=" + productId + ", quantity=" + quantity
				+ ", gid=" + gid + "]";
	}

}