package com.site.entity;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "quotation")
public class Quotation {
	private long id;
	private String productName;
	private String AvailableQuantity;
	private String Quantity;
	private byte[] image;
	private long price;
	private long totalPrice;
	private long grandPrice;
	private long userId;
	private Boolean active;
	private UUID token;
	private String emailAddress;
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public UUID getToken() {
		return token;
	}
	public void setToken(UUID token) {
		this.token = token;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	@Id
	@GeneratedValue
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getAvailableQuantity() {
		return AvailableQuantity;
	}
	public void setAvailableQuantity(String availableQuantity) {
		AvailableQuantity = availableQuantity;
	}
	public String getQuantity() {
		return Quantity;
	}
	public void setQuantity(String quantity) {
		Quantity = quantity;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public long getPrice() {
		return price;
	}
	public void setPrice(long price) {
		this.price = price;
	}
	public long getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(long totalPrice) {
		this.totalPrice = totalPrice;
	}
	public long getGrandPrice() {
		return grandPrice;
	}
	public void setGrandPrice(long grandPrice) {
		this.grandPrice = grandPrice;
	}
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "Quotation [id=" + id + ", productName=" + productName + ", AvailableQuantity=" + AvailableQuantity
				+ ", Quantity=" + Quantity + ", image=" + Arrays.toString(image) + ", price=" + price + ", totalPrice="
				+ totalPrice + ", grandPrice=" + grandPrice + ", userId=" + userId + "]";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
