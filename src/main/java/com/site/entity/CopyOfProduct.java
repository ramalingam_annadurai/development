package com.site.entity;

import java.awt.image.DataBufferByte;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.bson.types.ObjectId;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;

@SuppressWarnings({ "serial", "deprecation" })
@Entity
@Table(name = "copyproduct")
public class CopyOfProduct implements Serializable {
	@ManyToOne
	private Long id;
	private Long mainCategoryId;
	private Long subCategoryId;
	private Long brandid;
	private String productName;
	private String productDescription;
	private String productOriginalPrice;
	private String productSellingPrice;
	private String productSize;
	private String productColor;
	private String productCondition;
	private String productAvailability;
	private String productQuantity;
	private byte[] image;
	private byte[] image1;
	private byte[] image2;
	private Long sku;

	@Id
	@GeneratedValue
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	/* @ManyToOne(cascade = CascadeType.ALL) */
	public void setId(Long id) {
		this.id = id;
	}

	

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCondition() {
		return productCondition;
	}

	public void setProductCondition(String productCondition) {
		this.productCondition = productCondition;
	}

	public String getProductAvailability() {
		return productAvailability;
	}

	public void setProductAvailability(String productAvailability) {
		this.productAvailability = productAvailability;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] data) {
		this.image = data;
	}

	public Long getSku() {
		return sku;
	}

	public void setSku(Long sku) {
		this.sku = sku;
	}

	public String getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(String productQuantity) {
		this.productQuantity = productQuantity;
	}



	public String getProductOriginalPrice() {
		return productOriginalPrice;
	}

	public void setProductOriginalPrice(String productOriginalPrice) {
		this.productOriginalPrice = productOriginalPrice;
	}

	public String getProductSellingPrice() {
		return productSellingPrice;
	}

	public void setProductSellingPrice(String productSellingPrice) {
		this.productSellingPrice = productSellingPrice;
	}

	

	public String getProductSize() {
		return productSize;
	}

	public void setProductSize(String productSize) {
		this.productSize = productSize;
	}

	public String getProductColor() {
		return productColor;
	}

	public void setProductColor(String productColor) {
		this.productColor = productColor;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}


	public Long getMainCategoryId() {
		return mainCategoryId;
	}

	public void setMainCategoryId(Long mainCategoryId) {
		this.mainCategoryId = mainCategoryId;
	}

	public Long getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(Long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	
	public Long getBrandid() {
		return brandid;
	}

	public void setBrandid(Long brandid) {
		this.brandid = brandid;
	}

	public CopyOfProduct() {
		super();
	}

	public CopyOfProduct(Long id, Long mainCategoryId, Long subCategoryId,
			Long brandid, String productName, String productDescription,
			String productOriginalPrice, String productSellingPrice,
			String productSize, String productColor, String productCondition,
			String productAvailability, String productQuantity, byte[] image,
			Long sku) {
		super();
		this.id = id;
		this.mainCategoryId = mainCategoryId;
		this.subCategoryId = subCategoryId;
		this.brandid = brandid;
		this.productName = productName;
		this.productDescription = productDescription;
		this.productOriginalPrice = productOriginalPrice;
		this.productSellingPrice = productSellingPrice;
		this.productSize = productSize;
		this.productColor = productColor;
		this.productCondition = productCondition;
		this.productAvailability = productAvailability;
		this.productQuantity = productQuantity;
		this.image = image;
		this.sku = sku;
	}

	public byte[] getImage1() {
		return image1;
	}

	public void setImage1(byte[] image1) {
		this.image1 = image1;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", mainCategoryId=" + mainCategoryId
				+ ", subCategoryId=" + subCategoryId + ", brandid=" + brandid
				+ ", productName=" + productName + ", productDescription="
				/*+ productDescription + ", productOriginalPrice="
				+ productOriginalPrice + ", productSellingPrice="
				+ productSellingPrice + ", productSize=" + productSize
				+ ", productColor=" + productColor + ", productCondition="
				+ productCondition + ", productAvailability="
				+ productAvailability + ", productQuantity=" + productQuantity
			*/	+ ", image=" + Arrays.toString(image) + ", image1="
				+ Arrays.toString(image1) + ", sku=" + sku + "]";
	}

	public byte[] getImage2() {
		return image2;
	}

	public void setImage2(byte[] image2) {
		this.image2 = image2;
	}

	
}