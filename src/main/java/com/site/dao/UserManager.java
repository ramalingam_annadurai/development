package com.site.dao;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.persistence.NamedQueries;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.google.code.facebookapi.schema.Location;
import com.restfb.WebRequestor.Response;
import com.site.entity.AddToCart;
import com.site.entity.Brand;
import com.site.entity.ContactUs;
import com.site.entity.Feedback;
import com.site.entity.MainCategory;
import com.site.entity.Order;
import com.site.entity.Product;
import com.site.entity.Quotation;
import com.site.entity.SubCategory;
import com.site.entity.User;
import com.site.util.OrderItems;
import com.site.util.QuoteOrder;
import com.sun.java.swing.plaf.windows.resources.windows;

/**
 * @author S7
 *
 */
/**
 * @author S7
 *
 */
/**
 * @author S7
 * 
 */
@SuppressWarnings("unused")
@Component
@Scope("singleton")
public class UserManager {

	@Autowired(required = false)
	AuthenticationManager authManager;
	private Logger LOG = Logger.getLogger(UserManager.class);
	Session session = null;
	private HibernateTemplate ht;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.ht = new HibernateTemplate(sessionFactory);
	}

	public User getUser(String username) {
		User user = null;

		List<User> users = ht
				.find("FROM User u WHERE u.userName = ?", username);

		if (users != null && !users.isEmpty()) {
			user = users.get(0);
		}

		return user;
	}

	public User getEmailAddress(String emailAddress) {
		User user = null;

		List<User> users = ht.find("FROM User u WHERE u.emailAddress = ?",
				emailAddress);

		if (users != null && !users.isEmpty()) {
			user = users.get(0);
		}

		return user;
	}

	public void saveUser(User user) {
		ht.save(user);
	}

	public void mergeUser(User user) {
		ht.merge(user);
	}

	/**
	 * Method to ger user details by user name
	 * 
	 * @param username
	 * @return
	 */
	public User getUserByName(String name) {
		try {
			List<User> user = ht.find("from User u where u.userName=?", name);
			return user.get(0);
		} catch (Exception e) {
			System.out.println(e);
			return null;
		}
	}

	/**
	 * Method to get proudct details by product id
	 * 
	 * @param pdtid
	 * @return
	 */
	public List<Product> getProductByIdSingle(Long pdtid) {

		List<Product> products = ht
				.find("FROM Product p WHERE p.id = ?", pdtid);
		return products;
	}

	public List<Product> getProductById(Long id) {

		List<Product> products = ht.find(
				"FROM Product p WHERE p.mainCategoryId = ?", id);

		return products;

	}
//Get user details by user id
	public User getUserById(Long id) {
		User user = null;

		List<User> users = ht.find("FROM User u WHERE u.id = ?", id);

		if (users != null && !users.isEmpty()) {
			user = users.get(0);
		}

		return user;

	}

	public User getUserByToken(UUID token) {
		User user = null;

		List<User> users = ht.find("FROM User u WHERE u.token = ?", token);

		if (users != null && !users.isEmpty()) {
			user = users.get(0);
			String name = user.getEmailAddress();
			System.out.println(name);
		}

		return user;

	}

	public Quotation getQuotationByToken(UUID token) {
		Quotation user = null;

		List<Quotation> users = ht.find("FROM Quotation  u WHERE u.token = ?", token);

		if (users != null && !users.isEmpty()) {
			user = users.get(0);
			//String name = user.getEmailAddress();
			Long id=user.getUserId();
			System.out.println(id);
		}

		return user;

	}

	public User getActiveUser(String username) {
		User user = null;
		Boolean str = true;
		List<User> users = ht.find(
				"FROM User u WHERE u.active = ? and u.userName = ?", str,
				username);

		if (users != null && !users.isEmpty()) {
			user = users.get(0);
		}

		return user;
	}

	public List<String> getBrandsById(Long id) {
		List<String> brands = ht
				.find("select distinct p.productBrand FROM Product p WHERE p.mainProductId = ?",
						id);
		return brands;
	}

	public List<Product> getProductByCategory(String category) {
		List<Product> products = ht.find("FROM Product p WHERE p.category=?",
				category);
		return products;
	}

	public Product getProductByProductId(Long id) {
		List<Product> products = ht.find("FROM Product p WHERE p.id=?", id);
		Product product = products.get(0);
		return product;
	}

	public void mergeProduct(Product productFromDb) {
		ht.merge(productFromDb);
	}

	public Product getProduct(Long id) {
		List<Product> products = ht.find("FROM Product p WHERE p.id=?", id);
		Product product = products.get(0);
		return product;
	}

	public void addProductToCart(Long pid, Long qty) {

		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		List<User> user = ht.find("from User u where u.userName=?", uname);
		User usr = user.get(0);
		Long uid = usr.getId();
		AddToCart addToCart = new AddToCart();
		addToCart.setProductId(pid);
		addToCart.setQuantity(qty);
		addToCart.setUserId(uid);
		ht.save(addToCart);
	}

	public Long getCountOfCart() {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		List<User> user = ht.find("from User u where u.userName=?", uname);
		Long sum = (long) 0;
		if (!user.isEmpty()) {
			User usr = user.get(0);
			Long uid = usr.getId();

			List<AddToCart> cartItems = ht.find(
					"FROM AddToCart a WHERE a.userId=?", uid);
			if (cartItems != null)
				for (AddToCart ac : cartItems) {
					sum += ac.getQuantity();
				}
		}
		return sum;
	}

	public List<Product> getProductByBrand(String brand) {
		List<Product> products = ht.find(
				"FROM Product p WHERE p.productBrand=?", brand);

		return products;
	}

	public List<AddToCart> getProductfromCart(Long id) {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		List<User> user = ht.find("from User u where u.userName=?", uname);
		User usr = user.get(0);
		Long uid = usr.getId();
		List<AddToCart> products = new ArrayList<AddToCart>();
		try {
			products = ht.find("from AddToCart a where a.userId=?", uid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return products;
	}

	/**
	 * Method to get
	 */
	public void setProductToOrderTable() {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		List<User> user = ht.find("from User u where u.userName=?", uname);
		User usr = user.get(0);
		Long uid = usr.getId();
		List<AddToCart> list = ht
				.find("from AddToCart a where a.userId=?", uid);
		for (AddToCart addToCart : list) {
			List<Order> order = ht.find("from Order o where o.userId=?", uid);
			if (order.isEmpty()) {
				Order or = new Order();
				Date date = new Date();
				or.setDate(date);
				Long proid = addToCart.getProductId();
				Long proqty = addToCart.getQuantity();
				or.setProId(proid + "_" + proqty + "");
				or.setUserId(uid);
				or.setStatus("pending");
				ht.save(or);
			} else {
				Date date = new Date();
				Order ordersingle = order.get(0);
				ordersingle.setDate(date);
				Long proid = addToCart.getProductId();
				Long proqty = addToCart.getQuantity();
				String productidandQuantity = ordersingle.getProId();
				ordersingle.setProId(productidandQuantity + "," + proid + "_"
						+ proqty + "");
				ht.merge(ordersingle);
			}
		}
		for (AddToCart addToCart : list) {
			ht.delete(addToCart);
		}
	}

	public List<String> getMainCat() {
		List<String> list = ht
				.find("select distinct p.mainCategory FROM Product p");
		System.out.println(list);
		return list;
	}

	public int checkexistingInAddToCart(Long pid) {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		List<User> user = ht.find("from User u where u.userName=?", uname);
		User usr = user.get(0);
		Long uid = usr.getId();
		List<AddToCart> items = ht
				.find("from AddToCart a where a.productId=? and a.userId=?",
						pid, uid);
		if (items.isEmpty()) {
			return 0;
		}
		return 1;
	}

	public void mergeProductToCart(Long pid, Long qty) {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		List<User> user = ht.find("from User u where u.userName=?", uname);
		User usr = user.get(0);
		Long uid = usr.getId();
		List<AddToCart> items = ht
				.find("from AddToCart a where a.productId=? and a.userId=?",
						pid, uid);
		AddToCart item = items.get(0);
		Long qtty = item.getQuantity();
		qtty = qtty + 1;
		item.setQuantity(qtty);
		ht.merge(item);
	}

	public List<AddToCart> getProdFrmCartByUid() {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		List<User> user = ht.find("from User u where u.userName=?", uname);
		User usr = user.get(0);
		Long uid = usr.getId();
		List<AddToCart> a = ht.find("FROM AddToCart a WHERE a.userId=?", uid);
		return a;
	}

	public List<Product> getProductBySearchResult(String searchValue) {
		/*
		  List<Product> products = ht .find(
		  "from Product p where p.productName LIKE ? or p.productBrand LIKE ?",
		  "%" + searchValue + "%", "%" + searchValue + "%");*/
		  
		List<Product> products = ht.find(
				"from Product p where p.productName LIKE ?", "%" + searchValue
						+ "%");
		System.out.println("Searched products " + products);
		return products;
	}

	public void updatecartqty(Long id, Long qty) {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		List<User> user = ht.find("from User u where u.userName=?", uname);
		User usr = user.get(0);
		Long uid = usr.getId();
		List<AddToCart> items = ht.find("FROM AddToCart a WHERE a.userId=?",
				uid);
		for (AddToCart addToCart : items) {
			Long idFromDb = addToCart.getId();
			if (idFromDb.equals(id)) {
				addToCart.setQuantity(qty);
				ht.merge(addToCart);
			}
		}
	}

	public List<User> getUserDetails(String uname) {
		List<User> user = ht.find("from User u where u.userName=?", uname);
		return user;
	}

	public User getUserDetailsOrder(String uname) {
		List<User> user = ht.find("from User u where u.userName=?", uname);
		return null;
	}

	public List<AddToCart> getProductfromOrderTable() {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		List<User> user = ht.find("from User u where u.userName=?", uname);
		User usr = user.get(0);
		Long uid = usr.getId();
		List<AddToCart> items = ht.find("from AddToCart a where a.userId=?",
				uid);
		return items;
	}

	public List<User> getUserDetailsbytype(String usertype) {
		List<User> user = ht
				.find("from User u where u.accountType=?", usertype);
		return user;
	}

	public List<Product> getProductByproductId(Long pid) {
		List<Product> items = ht.find("from Product p where p.id=?", pid);
		return items;
	}

	public List<MainCategory> getmainlist() {
		List<MainCategory> items = ht.find("from MainCategory");
		return items;
	}

	public List<SubCategory> getsublist(Long mainid) {
		List<SubCategory> items = ht.find(
				"from SubCategory s where s.mainId=?", mainid);
		return items;
	}

	public List<com.site.entity.Brand> brandlistbymubidmainid(Long mainid,
			Long subid) {
		List<Brand> items = ht.find(
				"from Brand b where b.mainCategoryId=? and b.subCategoryId=?",
				mainid, subid);
		return items;
	}

	public void googleFblogin(String name, String email) {
		// existing user
		List<User> userdetails = ht.find("from User u where u.emailAddress=?",
				email);
		if (!userdetails.isEmpty()) {
			User userdetail = userdetails.get(0);
			String password = userdetail.getPassword();
			Authentication request = new UsernamePasswordAuthenticationToken(
					email, password);
			Authentication result = authManager.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);
		}
		// new user
		else {
			User user = new User();
			user.setEmailAddress(email);
			user.setUserName(email);
			user.setPassword("password");
			user.setActive(true);
			ht.save(user);
			List<User> userdetails1 = ht.find(
					"from User u where u.emailAddress=?", email);
			User userdetail1 = userdetails1.get(0);
			String password = userdetail1.getPassword();
			Authentication request = new UsernamePasswordAuthenticationToken(
					email, password);
			Authentication result = authManager.authenticate(request);
			SecurityContextHolder.getContext().setAuthentication(result);
		}
	}

	public List<Product> getProductByMainidAndSubid(Long mainid, Long subid) {
		List<Product> items = ht
				.find("from Product p where p.mainCategoryId=? AND p.subCategoryId=?",
						mainid, subid);
		return items;
	}

	public List<Brand> getBrandsByMainidAndSubid(Long mainid, Long subid) {
		List<Brand> items = ht.find(
				"from Brand b where b.mainCategoryId=? and b.subCategoryId=?",
				mainid, subid);
		return items;
	}

	public List<Product> getProductByMainidAndSubidAndBrandid(Long mainid,
			Long subid, Long brandid) {
		List<Product> items = ht
				.find("from Product p where p.mainCategoryId=? AND p.subCategoryId=? AND p.brandid=?",
						mainid, subid, brandid);
		return items;
	}

	public List<SubCategory> getSubCategory(Long mainid) {
		List<SubCategory> items = ht.find(
				"from SubCategory s where s.mainId=?", mainid);
		return items;
	}

	public List<Product> getProductByFilter(String query) {
		List<Product> items = ht.find(query);
		return items;
	}

	public void addProductCartForGuest(Long pid, Long qty, Random randNum) {
		Random gid = randNum;
		AddToCart addToCart = new AddToCart();
		addToCart.setProductId(pid);
		addToCart.setQuantity(qty);
		// addToCart.setGid(gid);
		ht.save(addToCart);
	}

	public Long getCountOfCartOfGuest(Random randNum) {
		Long sum = (long) 0;
		List<AddToCart> cartItems = ht.find("FROM AddToCart a WHERE a.gid=?",
				randNum);
		if (cartItems != null)
			for (AddToCart ac : cartItems) {
				sum += ac.getQuantity();
			}
		return sum;
	}

	public int checkexistingGuestInAddToCart(Long pid, Random randNum) {
		List<AddToCart> items = ht.find(
				"from AddToCart a where a.productId=? and a.gid=?", pid,
				randNum);
		if (items.isEmpty()) {
			return 0;
		}
		return 1;
	}

	public void mergeProductToCartForGuest(Long pid, Long qty, Random randNum) {
		List<AddToCart> items = ht.find(
				"from AddToCart a where a.productId=? and a.gid=?", pid,
				randNum);
		AddToCart item = items.get(0);
		Long qtty = item.getQuantity();
		qtty = qtty + 1;
		item.setQuantity(qtty);
		ht.merge(item);
	}

	public List<Order> getOrderList(Long id, String status) {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		System.out.println("name is " + uname);
		List<User> user = ht.find("from User u where u.userName=?", uname);
		User usr = user.get(0);
		Long uid = usr.getId();
		List<Order> orderList = new ArrayList<Order>();
		try {
			orderList = ht.find("from Order o where o.userName=?", uname);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orderList;

	}

	@SuppressWarnings("unchecked")
	public List<Order> cancelOrderList(String status) {
		List<Order> orderList = null;

		orderList = ht.find("from Order o where o.status=?", status);

		return orderList;
	}

	public void saveOrder(Order order) {
		ht.save(order);
	}

	public Feedback getFeedBack(String email) {
		// User user = null;
		Feedback feed = null;
		@SuppressWarnings("unchecked")
		List<Feedback> feedback = ht.find("FROM Feedback where email=?", email);

		if (feedback != null && !feedback.isEmpty()) {
			feed = feedback.get(0);
		}

		return feed;
	}

	public void saveFeedback(Feedback feed) {
		ht.save(feed);
		System.out.println(feed);
	}

	public Order getOrderById(Long oid) {
		Order order = null;
		@SuppressWarnings("unchecked")
		List<Order> orders = ht.find("FROM Order o WHERE o.orderId=?", oid);
		if (orders != null && orders.size() > 0) {
			order = orders.get(0);
		}

		return order;
	}

	public void mergeOrder(Order order) {
		ht.merge(order);		
	}

	/*public void addQuotation(String productName, String availableQuantity,
			String quantity, MultipartFile image, String price,
			String totalPrice) throws IOException {
		byte[] photo = image.getBytes();
		Quotation quot=new Quotation();
		quot.setProductName(productName);
		quot.setAvailableQuantity(availableQuantity);
		quot.setImage(photo);
		quot.setQuantity(quantity);
		quot.setPrice(price.toString());
		quot.setTotalPrice(totalPrice);
		ht.save(quot);
		System.out.println(quot);
	}*/

	public int delete(Long id) {
		//Long idd = Long.parseLong(id);
		
		List<AddToCart> sub = ht.find("FROM AddToCart s where s.productId=?", id);
		AddToCart br = sub.get(0);
		ht.delete(br);
		return 1;
	}
	
	/**
	 * Remove Product From Cart
	 * @param prdId
	 */public void removeProductFromCart(Long prdId, Long userId) {
		List<AddToCart> addToCarts = ht.find(" from AddToCart ac where ac.productId=? and ac.userId=?", prdId, userId);
                 if(addToCarts != null && addToCarts.size() >0)
		ht.delete(addToCarts.get(0));
		
	}

	public Long getCountOfCart(Long id) {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		List<User> user = ht.find("from User u where u.userName=?", uname);
		Long sum = (long) 0;
		if (!user.isEmpty()) {
			User usr = user.get(0);
			Long uid = usr.getId();

			List<AddToCart> cartItems = ht.find(
					"FROM AddToCart a WHERE a.userId=?", uid);
			if (cartItems != null)
				for (AddToCart ac : cartItems) {
					sum += ac.getQuantity();
				}
		}
		return sum;
	}

	public AddToCart getAddToCartByProductId(Long proId, Long userId) {
	AddToCart addToCart = null;
		List<AddToCart> addToCarts = ht.find("from AddToCart a where a.productId=? and a.userId = ?", proId, userId);
		if(addToCarts != null && addToCarts.size() > 0)
			addToCart = addToCarts.get(0);
	return addToCart;
}

	public List<QuoteOrder> saveQuote() {
		List<QuoteOrder> order= (List<QuoteOrder>) ht.save("From QuoteOrder q");
		System.out.println("searched order after submitting is "+order);
		return null;
	}

	public void saveQuotation(Quotation qu) {
		// TODO Auto-generated method stub
		ht.save(qu);
		
		
		//List<Quotation> pde=(List<Quotation>) ht.save(qu);
		//System.out.println("details are "+pde);
		//return null;
	}

	public List<Product> getProductList() {
		List<Product> prod =null;
		prod = ht.find("FROM Product p order by p.id desc limit 10").subList(0, 6);
		System.out.println("total  "+prod);
	
	return prod;
	}

	public void stoteContact(ContactUs contact) {
		ht.save(contact);
		
	}

	public void mergeQuotation(Quotation user) {
		ht.merge(user);
	}

	public List<User> getUserDetails(String name, Long id) {
		List<User> user = ht.find("from User u where u.userName=? and u.id=?", name,id);
		return user;
	}

	public List<Quotation> getQuotationList(String name) {
		String uname = SecurityContextHolder.getContext().getAuthentication()
				.getName();
		System.out.println("name is " + uname);
		List<User> user = ht.find("from User u where u.userName=?", uname);
		User usr = user.get(0);
		Long uid = usr.getId();
		List<Quotation> orderList = new ArrayList<Quotation>();
		try {
			orderList = ht.find("from Quotation o where o.emailAddress=?", uname);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orderList;
	}
}