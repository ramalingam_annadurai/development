package com.site.util;

public class FacebookConfig {
  public static String key;
  private static String retUrl;
	private static String fbAppId;


  
  public static String getOAuthDialogUrl() {
	 
    return "https://www.facebook.com/dialog/oauth?client_id="+fbAppId+"&redirect_uri="+retUrl+"&scope=email";
  }

  public static String getOAuthUrl(String code) {
	 
    return "https://graph.facebook.com/oauth/access_token?client_id="+fbAppId+"&redirect_uri="+retUrl+"&client_secret="+key+"&code="+code;
  }
  public static String logoutUrl(String token) {
		 
	 return "https://www.facebook.com/logout.php?next="+retUrl+"&access_token="+token;
	  }
  public static String getGraphUrl(String token) {
    return "https://graph.facebook.com/me?access_token="+token;
  }


public static String getKey() {
	return key;
}

public static void setKey(String key) {
	FacebookConfig.key = key;
}

public String getRetUrl() {
	return retUrl;
}

public void setRetUrl(String retUrl) {
	this.retUrl = retUrl;
}

public String getFbAppId() {
	return fbAppId;
}

public void setFbAppId(String fbAppId) {
	this.fbAppId = fbAppId;
}



/*  public static String getProfilePictureUrl(Listener profile) {
    return "https://graph.facebook.com/"+profile.getFacebookId()+"/picture";
  }

  public static String getProfilePictureUrl(Listener profile, String size) {
    return "https://graph.facebook.com/"+profile.getFacebookId()+"/picture?type="+size;
  }*/
  
}