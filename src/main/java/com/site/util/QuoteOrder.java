package com.site.util;

public class QuoteOrder {
	private int id;
	private String productName;
	private int quantity;
	private int amount;
	private int totalAmount;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(int totalAmount) {
		this.totalAmount = totalAmount;
	}
	@Override
	public String toString() {
		return "QuoteOrder [id=" + id + ", productName=" + productName + ", quantity=" + quantity + ", amount="
				+ amount + ", totalAmount=" + totalAmount + "]";
	}
	
	
}
