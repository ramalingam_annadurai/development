package com.site.util;

import java.util.Arrays;

public class QuotationItems {
	private String pdtId;
	private String productName;
	private String productPrice;
	private String quantity;
	private byte[] image;
	private String addToCardId;
	public String getPdtId() {
		return pdtId;
	}
	public void setPdtId(String pdtId) {
		this.pdtId = pdtId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(String productPrice) {
		this.productPrice = productPrice;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public String getAddToCardId() {
		return addToCardId;
	}
	public void setAddToCardId(String addToCardId) {
		this.addToCardId = addToCardId;
	}
	@Override
	public String toString() {
		return "QuotationItems [pdtId=" + pdtId + ", productName="
				+ productName + ", productPrice=" + productPrice
				+ ", quantity=" + quantity + ", image="
				+ Arrays.toString(image) + ", addToCardId=" + addToCardId + "]";
	}
	
	
}
