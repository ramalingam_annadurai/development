package com.site.web;
import com.site.entity.User;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class UserFormValidator implements Validator{

	@Override
	public boolean supports(Class<?> arg0) {
		return User.class.isAssignableFrom(arg0);
	}

	public void validate(Object command, Errors errors) {
	
		User userDetails = (User) command;
		if(isNullOrEmpty(userDetails.getUserName()))
			errors.rejectValue("userName", "UserName.Required");
	
		if(isNullOrEmpty(userDetails.getEmailAddress()))
			errors.rejectValue("emailAddress", "Email.Required");
		
		if(isNullOrEmpty(userDetails.getPassword()))
			errors.rejectValue("password", "Password.Required");
		
		if(isNullOrEmpty(userDetails.getConfirmPassword()))
			errors.rejectValue("confirmPassword", "ConfirmPassword.Required");
		
			if(isNullOrEmpty(userDetails.getFirstName()))
				errors.rejectValue("firstName", "FirstName.Required");
			
		
		if(isNullOrEmpty(userDetails.getLastName()))
			errors.rejectValue("lastName", "LastName.Required");
		
			

		
	}
	
	private boolean isNullOrEmpty(String value) {
		if(value==null || value.trim().equals("")) {    
			return true;
		}
		return false; 
	}
	
	
	
}
