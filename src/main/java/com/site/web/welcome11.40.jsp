
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html dir="ltr" class="ltr" lang="en">
<head>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>

<link
	href="http://kartrocket-mtp.s3.amazonaws.com/all-stores/image_portofaspirations/data/logo/alternate_logo.jpg"
	rel="icon" />

<link href="http://www.portofaspirations.com/" rel="canonical" />
<link
	href="http://cdn.kartrocket.co/1449078249/catalog/view/theme/pav_oneshop/stylesheet/bootstrap.css"
	rel="stylesheet" />
<link
	href="http://cdn.kartrocket.co/1449078249/catalog/view/theme/pav_oneshop/stylesheet/stylesheet.css"
	rel="stylesheet" />
<link
	href="http://cdn.kartrocket.co/1449078239/catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css"
	rel="stylesheet" />
<link
	href="http://cdn.kartrocket.co/1449078249/catalog/view/theme/pav_oneshop/stylesheet/animation.css"
	rel="stylesheet" />
<link
	href="http://cdn.kartrocket.co/1449078249/catalog/view/theme/pav_oneshop/stylesheet/font-awesome.min.css"
	rel="stylesheet" />
<link
	href="http://cdn.kartrocket.co/1449078249/catalog/view/theme/pav_oneshop/stylesheet/pavproductcarousel.css"
	rel="stylesheet" />
<link
	href="http://cdn.kartrocket.co/1449078249/catalog/view/theme/pav_oneshop/stylesheet/pavdeals.css"
	rel="stylesheet" />
<link
	href="http://cdn.kartrocket.co/1449078249/catalog/view/theme/pav_oneshop/stylesheet/sliderlayer/css/typo.css"
	rel="stylesheet" />
<link
	href="http://cdn.kartrocket.co/1449078249/catalog/view/theme/pav_oneshop/stylesheet/pavmegamenu.css"
	rel="stylesheet" />
<style>
#page .container {
	max-width: 100%;
	width: 2000px;
}
</style>


<link
	href='http://fonts.googleapis.com/css?family=Raleway:400,600,700,500'
	rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300'
	rel='stylesheet' type='text/css'>
<link
	href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300'
	rel='stylesheet' type='text/css'>

<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078249/catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078249/catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078249/catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>
<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078249/catalog/view/javascript/common.js"></script>
<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078249/catalog/view/javascript/jquery/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078249/catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078249/catalog/view/javascript/pavdeals/countdown.js"></script>
<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078249/catalog/view/javascript/layerslider/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078249/catalog/view/javascript/layerslider/jquery.themepunch.revolution.min.js"></script>



<!--[if lt IE 9]>
<script src="catalog/view/javascript/html5.js"></script>
<script src="catalog/view/javascript/respond.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/pav_oneshop/stylesheet/ie8.css" />
<![endif]-->

<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078239/catalog/view/javascript/jquery/ui/external/jquery.cookie.js"></script>
<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078238/catalog/view/javascript/jquery/colorbox/jquery.colorbox.js"></script>
<link rel="stylesheet" type="text/css"
	href="http://cdn.kartrocket.co/1449078238/catalog/view/javascript/jquery/colorbox/colorbox.css"
	media="screen" />
<link rel="stylesheet" type="text/css"
	href="http://cdn.kartrocket.co/1449078239/catalog/view/javascript/jquery/onefancybox/jquery.fancybox-1.3.4.css"
	media="screen" />
<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078239/catalog/view/javascript/jquery/onefancybox/jquery.fancybox-1.3.4.pack.js"></script>

<script type="text/javascript"
	src="http://cdn.kartrocket.co/1449078239/catalog/view/javascript/jquery/tabs.js"></script>

<link rel="stylesheet" type="text/css"
	href="catalog/view/theme/pav_oneshop/stylesheet/paneltool.css" />
<script type="text/javascript"
	src="catalog/view/javascript/jquery/colorpicker/js/colorpicker.js"></script>
<link rel="stylesheet" type="text/css"
	href="catalog/view/javascript/jquery/colorpicker/css/colorpicker.css" />


<link rel="stylesheet" type="text/css"
	href="catalog/view/theme/pav_oneshop/stylesheet/colorbox.css" />
<script type="text/javascript"
	src="catalog/view/theme/pav_oneshop/javascript/common.js"></script>
<link rel="stylesheet" type="text/css"
	href="catalog/view/theme/pav_oneshop/stylesheet/font-awesome.min.css"
	media="screen" />
<link rel="stylesheet" type="text/css"
	href="catalog/view/theme/pav_oneshop/stylesheet/font.css"
	media="screen" />
<link href="css/search-box.css" rel="stylesheet" />
<link href="css/login_form.css" rel="stylesheet" />
<!-- <script src="js/jquery-1.9.1.min.js"></script> -->
<link rel="stylesheet" href="css/jquery.rating.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<script src="js/bootstrap.min.1530170614.js"></script>

<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src="js/jquery.simplyscroll.js"></script>
<!-- <script type="text/javascript">
	function myFunction() {

		AjaxController.logout();
	}
	(function($) {
		$(function() {
			$("#scroller, #scroller1, #scroller2, #scroller3").simplyScroll();
		});
	})(jQuery);
</script> -->
<script type="text/javascript">
	$(document).ready(function() {
		$('#contact-box').stickyfloat({
			duration : 400,
			offsetY : 200
		});
	});
</script>
<script type="text/javascript">
	function showContact() {
		if ($("#frmContact").css('display') == 'none') {
			$("#frmContact").show();
		} else {
			/* document.getElementById("frmContact").style.display= "none"; */
			$("#frmContact").hide();
		}

	}
</script>
<style type="text/css">
.rating {
	overflow: hidden;
	display: inline-block;
}

.rating-input {
	position: absolute;
	left: 0;
	top: -50px;
}

.rating-star {
	display: block;
	float: right;
	width: 16px;
	height: 16px;
	background: url('http://kubyshkin.ru/samples/star-rating/star.png') 0
		-16px;
}

.rating-star:hover, .rating-star:hover           ~ .rating-star,
	.rating-input:checked 
	         ~ .rating-star {
	background-position: 0 0;
}

/* Just for the demo */
body {
	margin: 20px;
}

.simply-scroll .simply-scroll-clip {
	width: 657px !important;
}
</style>
<br>
<br>
<br>
<br>
</head>
<body onunload="myFunction()"
	style="padding: 0px; margin: 0px; background-color: white;">

	<!-- The following codes demostrate jssor slider work with jquery library -->

	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="js/jssor.slider-20.mini.js"></script>
	<script src="js/jquery.validate.js"></script>
	<script src="js/feedvalidation.js"></script>
	<script src="js/jquery.rating.js"></script>
	<!-- use jssor.slider-20.debug.js instead for debug -->
	<script>
		jQuery(document).ready(function($) {

			var jssor_1_options = {
				$AutoPlay : true,
				$BulletNavigatorOptions : {
					$Class : $JssorBulletNavigator$
				},
				$ThumbnailNavigatorOptions : {
					$Class : $JssorThumbnailNavigator$,
					$Cols : 3,
					$Align : 200
				}
			};

			var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

			//responsive code begin
			//you can remove responsive code if you don't want the slider scales while window resizes
			function ScaleSlider() {
				var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
				if (refSize) {
					refSize = Math.min(refSize, 600);
					jssor_1_slider.$ScaleWidth(refSize);
				} else {
					window.setTimeout(ScaleSlider, 30);
				}
			}
			ScaleSlider();
			$(window).bind("load", ScaleSlider);
			$(window).bind("resize", ScaleSlider);
			$(window).bind("orientationchange", ScaleSlider);
			//responsive code end
		});
	</script>
	<script type="text/javascript">
		function AddToCartFunction(d) {
			var productid = d;
			var productquantity = document.getElementById("pdtquantity").value;
			AjaxController.addProductToCart(productid, productquantity,
					productList);
		}
		function productList(productList) {
			var count = productList;
			alert(count);

			document.getElementById("cartnotification").innerHTML = productList;
		}
	</script>
	<script>
		$ = jQuery;
	</script>


	<style>
.deeper>div {
	background: #fff;
	display: table;
	min-width: 150px;
	position: absolute;
	z-index: 99;
	display: none;
}

.deeper>div>ul {
	float: inherit;
	position: static;
}

li.deeper:hover>div {
	display: block;
}

li.deeper:hover>div ul {
	visibility: visible !important;
}

.carousel-control {
	top: -30px;
}

.productcarousel .box-heading {
	padding-top: 36px;
	height: 4px;
}

.product-block .product-meta {
	padding: 11px;
}

.navbar-fixed-top .search {
	margin-top: 10px !important;
}

.header-wrap #logo {
	line-height: normal;
	padding-top: 7px;
}

.navbar-fixed-top .header-wrap #logo {
	padding-top: 0;
}

.page-category .wrap-topbar .quick-access .quickaccess-toggle,
	.page-category .wrap-topbar .quick-setting .quickaccess-toggle {
	padding: 26px 0;
}

.page-category .navbar-fixed-top .wrap-topbar .quick-access .quickaccess-toggle,
	.page-category .navbar-fixed-top .wrap-topbar .quick-setting .quickaccess-toggle
	{
	padding: 16px 0 !important;
}

#powered>div {
	text-align: center;
	padding-top: 5px;
	margin-top: 5px;
	border-top: 1px solid #333;
}

.page-product .producttabs .htabs a.vqmod-custom-tab-link {
	padding: 14px 15px 13px;
	border-right: 1px solid #dfdfdf;
	color: #999;
}

.page-product .producttabs .htabs a.vqmod-custom-tab-link:hover {
	background-color: #D45757;
	color: #fff;
}

.page-product .producttabs .htabs.selected {
	color: #fff;
}

#cart .content .mini-cart-info {
	max-height: 200px;
	overflow-y: auto;
	width: 340px;
}
</style>

	<style>
@media only screen and (max-width: 800px) {
	.header-wrap .menu {
		left: -375px;
	}
	.search input[type="text"] {
		width: 100% !important;
	}
}

@media only screen and (max-width: 768px) {
	.navbar-fixed-top, .navbar-fixed-bottom {
		position: static !important;
	}
	#pav-slideshow {
		display: block !important;
	}
}

@media only screen and (max-width: 600px) {
	.search input[type="text"] {
		width: 100% !important;
	}
	.topbar .wrap-topbar {
		width: 99%;
		border-top: 1px solid #333 !important;
	}
	.search {
		width: 40%;
	}
	.header-wrap #logo {
		width: 100%;
		text-align: center;
	}
}

@media only screen and (max-width: 320px) {
	.header-wrap .menu {
		left: 3px;
		top: -26px;
	}
	.header-wrap #logo {
		width: auto;
	}
	#pav-slideshow .header-wrap {
		height: 88px;
	}
	.tparrows {
		display: none;
	}
}
</style>
	<style>
.jssorb03 {
	position: absolute;
}

.jssorb03 div, .jssorb03 div:hover, .jssorb03 .av {
	position: absolute;
	/* size of bullet elment */
	width: 21px;
	height: 21px;
	text-align: center;
	line-height: 21px;
	color: white;
	font-size: 12px;
	background: url('img/b03.png') no-repeat;
	overflow: hidden;
	cursor: pointer;
}

.jssorb03 div {
	background-position: -5px -4px;
}

.jssorb03 div:hover, .jssorb03 .av:hover {
	background-position: -35px -4px;
}

.jssorb03 .av {
	background-position: -65px -4px;
}

.jssorb03 .dn, .jssorb03 .dn:hover {
	background-position: -95px -4px;
}

.jssort16 .p {
	position: absolute;
	top: 0;
	left: 0;
	width: 200px;
	height: 100px;
}

.jssort16 .t {
	position: absolute;
	top: 0;
	left: 0;
	width: 200px;
	height: 100px;
	border: none;
}

.jssort16 .p img {
	position: absolute;
	top: 0;
	left: 0;
	width: 200px;
	height: 100px;
	filter: alpha(opacity =            
		                                                         
		              55);
	opacity: .55;
	transition: opacity .6s;
	-moz-transition: opacity .6s;
	-webkit-transition: opacity .6s;
	-o-transition: opacity .6s;
}

.jssort16 .pav img, .jssort16 .pav:hover img, .jssort16 .p:hover img {
	filter: alpha(opacity =           
		                                                         
		               100);
	opacity: 1;
	transition: none;
	-moz-transition: none;
	-webkit-transition: none;
	-o-transition: none;
}

.jssort16 .pav:hover img, .jssort16 .p:hover img {
	filter: alpha(opacity =            
		                                                         
		              70);
	opacity: .7;
}

.jssort16 .title, .jssort16 .title_back {
	position: absolute;
	bottom: 0px;
	left: 0px;
	width: 200px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	color: #000;
	font-size: 20px;
}

.jssort16 .title_back {
	background-color: #fff;
	filter: alpha(opacity =            
		                                                         
		              50);
	opacity: .5;
}

.jssort16 .pav .title_back {
	background-color: #000;
	filter: alpha(opacity =            
		              50);
	opacity: .5;
}

.jssort16 .pav .title {
	color: #fff;
}

.jssort16 .p.pav:hover .title_back, .jssort16 .p:hover .title_back {
	filter: alpha(opacity =            
		                                                         
		              40);
	opacity: .4;
}

.jssort16 .p.pdn img {
	filter: alpha(opacity =           
		                                                         
		               100);
	opacity: 1;
}

/* div.image {
	width: 25px; /*width of your image*/
height










:





 





120
px










; /*height of your image*/
background










:





 





transparent





 





url










("
images
/home/feedback_logo










.jpg










");
*
/
//






	





background-image










:






		





url










("
http










:
//s3-ap-southeast-1










.amazonaws










.com
/wk-static-files/webengage/feedbacktab/~1gnmi6e










.png










")
margin










:










0; /* If you want no margin */
padding










:





 





0; /*if your want to padding */
background-position










:





 





right










;
padding-right










:





 





20
px










;
margin-left










:





 





160
px










;
margin-top










:





 





150
px










;
position










:





 





fixed










;
display










:





 





block










;
}
#image {
	width: 25px; /*width of your image*/
	height: 120px; /*height of your image*/
	background: transparent url("images/home/feedback_logo.jpg"); */ //
	background-image:
		url("http://s3-ap-southeast-1.amazonaws.com/wk-static-files/webengage/feedbacktab/~1gnmi6e.png")
		margin:0; /* If you want no margin */
	padding: 0; /*if your want to padding */
	background-position: right;
	padding-right: 20px;
	margin-left: 160px;
	margin-top: 150px;
	position: fixed;
	display: block;
}

div.img {
	color: #f9f9f9;
	letter-spacing: .1em;
	float: left;
	margin: 0;
	padding: 0;
	margin-left: 160px;
	margin-top: 150px;
	text-shadow: 0 0 1px rgba(0, 0, 0, 0.35);
	margin-top: 150px;
}

#touch {
	margin-left: 660px;
}
</style>
	<!--CHECK  -->

	<!--  -->
</head>

<body id="offcanvas-container"
	class="offcanvas-container keep-header layout-fullwidth fs12 page-home lang-en">

	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"
		style="background-color: white;">
		<div class="jumbtron">


			<div id="jssor_1" class="row"
				style="position: relative; margin: 0 auto; top: 0px; left: 0px; margin-top =100px; width: 600px; height: 400px; overflow: hidden; visibility: hidden;">
				<!-- Loading Screen -->
				<div data-u="loading"
					style="position: absolute; top: 0px; left: 0px;">
					<div
						style="filter: alpha(opacity =           70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
					<div
						style="position: absolute; display: block; background: url('img/loading.gif') no-repeat center center; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
				</div>
				<div data-u="slides"
					style="cursor: default; position: relative; top: 0px; left: 0px; width: 600px; height: 400px; overflow: hidden;">
					<div style="display: none;">
						<a href="shop.mm?main-image=1"><img data-u="image"
							src="img/electronics.jpg" /></a>
						<div data-u="thumb">
							<img src="img/electronics1.jpg" />
							<div class="title_back"></div>
							<div class="title">
								<a href="shop.mm?main-image=1">Electronics</a>
							</div>
						</div>
					</div>
					<div style="display: none;">
						<a href="shop.mm?main-image=3"><img data-u="image"
							src="img/books.png" /></a>
						<div data-u="thumb">
							<img src="img/books1.png" />
							<div class="title_back"></div>
							<div class="title">
								<a href="shop.mm?main-image=3">Books</a>
							</div>
						</div>
					</div>
					<div style="display: none;">
						<a href="shop.mm?main-image=2"><img data-u="image"
							src="img/cloths.jpg" /></a>
						<div data-u="thumb">
							<img src="img/cloths1.jpg" />
							<div class="title_back"></div>
							<div class="title">
								<a href="shop.mm?main-image=2">Cloths</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Thumbnail Navigator -->
				<div data-u="thumbnavigator" class="jssort16"
					style="position: absolute; left: 0px; bottom: 0px; width: 600px; height: 100px;"
					data-autocenter="1">
					<!-- Thumbnail Item Skin Begin -->
					<div data-u="slides" style="cursor: default;">
						<div data-u="prototype" class="p">
							<div data-u="thumbnailtemplate" class="t"></div>
						</div>
					</div>
					<!-- Thumbnail Item Skin End -->
				</div>
				<!-- Bullet Navigator -->
				<div data-u="navigator" class="jssorb03"
					style="bottom: 116px; right: 6px; margin-right: 250px;">
					<!-- bullet navigator item prototype -->
					<div data-u="prototype" style="width: 21px; height: 21px;">
						<div data-u="numbertemplate"></div>
					</div>
				</div>
				<a href="http://www.jssor.com" style="display: none">Jssor
					Slider</a>
			</div>
			<br> <br>

			<section id="page" class="offcanvas-pusher" role="main">

				<section id="sys-notification">
					<div class="container">

						<div id="notification"></div>
					</div>
				</section>

				<section id="columns">
					<div class="container">
						<div class="row">

							<section class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
								<div id="content">
									<div class="content-top">

										<style>
.product-block .image .product-zoom {
	top: 115px !important;
}

.productcarousel {
	margin-bottom: 0px;
}
</style>

										<!--							LATEST														  -->
										<div class="productcarousel">
											<div class="box-heading ">
												<span class="headding-title">Latest</span>
											</div>
											<div class="box-content">
												<div class="box-products slide"
													id="productcarousel540858319">
													<div class="carousel-controls hidden-xs">
														<a class="carousel-control left fa fa-angle-left"
															href="#productcarousel540858319" data-slide="prev"></a> <a
															class="carousel-control right fa fa-angle-right"
															href="#productcarousel540858319" data-slide="next"></a>
													</div>
													<div class="carousel-inner ">

														<div class="item active">
															<div class="row box-product">
																<c:forEach var="product" items="${pro}" >
																	<div class="col-lg-2 col-md-4 col-sm-4 col-xs-12"
																		style="padding-bottom: 10px;">
																		<div class="product-block">
																			<div class="group-item">
																				<div class="image ">


																					<div class="image_container">
																						<a href="#" class="img front"><img
																							src="image.mm?id=${product.id}"
																							title="${product.productName}"
																							alt="${product.productName}"style="height: 180px;width: 200px;"/></a> Show Swap

																					</div>

																					<a
																						class="pav-colorbox hidden-sm hidden-md hidden-xs"
																						href="index.php?route=themecontrol/product&amp;product_id=999">quick
																						view</a> <a href="#"
																						class="info-view colorbox product-zoom"
																						rel="colorbox" title="${product.productName}"><i
																						class="fa fa-search-plus"></i></a>
																				</div>



																			</div>
																			<div class="product-meta">

																				<h3 class="name">
																					<a
																						href="http://www.portofaspirations.com/objective-approach-to-mathematics-vol-2-for-engineering-entrances">
																						${product.productName}</a>
																				</h3>



																				<div class="description">${product.productName}
																				</div>
																				<div class="price">
																					<span class="price-old">${product.productOriginalPrice}</span>
																					<span class="price-new">${product.productSellingPrice}</span>
																					<span class="saving-percentage">${((product.productOriginalPrice-product.productSellingPrice )*100)/product.productOriginalPrice}.<b>%</span>
																				</div>


																				<div class="cart">
																					<div>
																						<a class="addtocart"
																							onclick="AddToCartFunction('${product.id}');"
																							data-hover="Add to cart"><span><i
																								class="fa fa-shopping-cart"></i>Add to Cart</span></a>
																					</div>
																				</div>
																				</style>

																				<div class="action">
																					<div class="action-inner">
																						<div class="wishlist pull-left">
																							<a class="fa fa-heart"
																								onclick="addToWishList('${product.id}');"
																								title="Add to Wish List"><span>Add to
																									Wish List</span></a>
																						</div>
																						<div class="compare pull-right">
																							<a class="fa fa-retweet"
																								onclick="addToCompare('${product.id}');"
																								title="Add to Compare"><span>Add to
																									Compare</span></a>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</c:forEach>

															</div>
														</div>
														
													</div>
												</div>
											</div>
										</div>

										<script type="text/javascript">
											$('#productcarousel540858319')
													.carousel({
														interval : false,
														auto : false,
														pause : 'hover'
													});
										</script>
										<script type="text/javascript">
										<!--
											$(document)
													.ready(
															function() {
																$('.colorbox')
																		.colorbox(
																				{
																					overlayClose : true,
																					opacity : 0.5,
																					rel : false,
																					onLoad : function() {
																						$(
																								"#cboxNext")
																								.remove(
																										0);
																						$(
																								"#cboxPrevious")
																								.remove(
																										0);
																						$(
																								"#cboxCurrent")
																								.remove(
																										0);
																					}
																				});
															});
										</script>

										<style>
.product-block .image .product-zoom {
	top: 115px !important;
}

.productcarousel {
	margin-bottom: 0px;
}
</style>
										<!-- 								MOST VIEWED												  -->
										<div class="productcarousel">
											<div class="box-heading ">
												<span class="headding-title">Most Viewed</span>
											</div>
											<div class="box-content">
												<div class="box-products slide"
													id="productcarousel394874194">


													<div class="carousel-controls hidden-xs">
														<a class="carousel-control left fa fa-angle-left"
															href="#productcarousel394874194" data-slide="prev"></a> <a
															class="carousel-control right fa fa-angle-right"
															href="#productcarousel394874194" data-slide="next"></a>
													</div>
													
												</div>
											</div>
										</div>

										<script type="text/javascript">
											$('#productcarousel394874194')
													.carousel({
														interval : false,
														auto : false,
														pause : 'hover'
													});
										</script>
										<script type="text/javascript">
										<!--
											$(document)
													.ready(
															function() {
																$('.colorbox')
																		.colorbox(
																				{
																					overlayClose : true,
																					opacity : 0.5,
																					rel : false,
																					onLoad : function() {
																						$(
																								"#cboxNext")
																								.remove(
																										0);
																						$(
																								"#cboxPrevious")
																								.remove(
																										0);
																						$(
																								"#cboxCurrent")
																								.remove(
																										0);
																					}
																				});
															});
										</script>

										<style>
.product-block .image .product-zoom {
	top: 115px !important;
}

.productcarousel {
	margin-bottom: 0px;
}
</style>
										<div class="productcarousel">
											<div class="box-heading ">
												<span class="headding-title">Special</span>
											</div>
											<div class="box-content">
												<div class="box-products slide"
													id="productcarousel1154326058">


													<div class="carousel-controls hidden-xs">
														<a class="carousel-control left fa fa-angle-left"
															href="#productcarousel1154326058" data-slide="prev"></a>
														<a class="carousel-control right fa fa-angle-right"
															href="#productcarousel1154326058" data-slide="next"></a>
													</div>
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>
				</section>
			</section>
		</div>


		<script type="text/javascript">
			$('#productcarousel1154326058').carousel({
				interval : false,
				auto : false,
				pause : 'hover'
			});
		</script>
		<script type="text/javascript">
		<!--
			$(document).ready(function() {
				$('.colorbox').colorbox({
					overlayClose : true,
					opacity : 0.5,
					rel : false,
					onLoad : function() {
						$("#cboxNext").remove(0);
						$("#cboxPrevious").remove(0);
						$("#cboxCurrent").remove(0);
					}
				});
			});
		</script>
	</div>
	<h1 style="display: none;">Portofaspirations</h1>
	</div>
	</section>


	</section>

	</div>
	<!-- </div></section> -->


	<script>
		$(document).ready(
				function() {
					$(".image-additional .carousel-inner a.active").click(
							function() {
								$(".image-container .image a").attr("href",
										$(this).attr("href"));
							});
				});
	</script>
	<style>
.megamenu .mega-group>a .menu-title {
	background: #fdc526:font-family: signika negative;
}

#pav-mainnav .navbar .navbar-nav>li>a {
	font-size: 15px;
	font-weight: normal;
}

.megamenu .mega-group>a .menu-title {
	font-size: 14px !important;
}

#mainnav .navbar .nav li a, .navbar .nav li a {
	font-size: 14px;
}
</style>
	<style>
@media ( min-width : 992px) and (max-width: 1199px) {
}

@media ( min-width : 768px) and (max-width: 991px) {
	.dropdown-menu-inner .mega-col>ul>li {
		float: !important;
	}
}

@media ( max-width : 767px) {
	.dropdown-menu-inner .mega-col>ul>li {
		float: none !important;
	}
	.header-wrap .menu {
		left: 45px;
	}
}

@media ( min-width :566px) and (max-width:767px) {
	body {
		overflow: hidden;
	}
	.dropdown-menu-inner .mega-col>ul>li {
		float: none !important;
	}
	.header-wrap .menu {
		left: 45px;
	}
}

@media ( min-width :480px) and (max-width:566px) {
	.dropdown-menu-inner .mega-col>ul>li {
		float: none !important;
	}
	.header-wrap .menu {
		left: 45px;
	}
}

@media ( max-width : 480px) {
	.dropdown-menu-inner .mega-col>ul>li {
		float: none !important;
	}
	.navbar-inverse .navbar-toggle {
		top: 41px;
	}
	.header-wrap .menu {
		left: 5px;
	}
}

@media ( min-width :320px) and (max-width:480px) {
	.navbar-inverse .navbar-toggle {
		top: 41px;
	}
	.header-wrap .menu {
		left: 2px;
	}
}
</style>
	<style>
.page-home .product-block .name {
	line-height: 12px;
	height: 80px;
}

.product-block .cart a span:hover {
	background: #f36b48;
}

.product-block .image .product-zoom i {
	display: none;
}

.megamenu .mega-group>a .menu-title {
	font-size: 17px;
	font-weight: bold;
	text-transform: uppercase;
}

.product-block .product-meta .name {
	min-height: inherit;
}

.product-info h2 {
	font-size: 22px;
}

.wishlist>a {
	color: #fff;
}

body, p {
	font-size: 13px;
	font-family: lato;
}

#footer .box .box-heading {
	line-height: 26px;
}

.social {
	border-bottom: none;
}

.footer-center .col-lg-3 {
	border-left: none;
}

.product-info .product-extra .wishlist a:hover, .product-info .product-extra .compare a:hover
	{
	color: #000;
}

.sidebar .box .box-heading {
	background: #fdc526;
}

.product-info {
	border: none;
}

.product-info .product-extra .wishlist:hover, .product-info .product-extra .compare:hover
	{
	background: #eee;
	border-bottom: 1px solid #ccc;
}

.product-info .cart:hover {
	background: #fdc526;
}

.price {
	font-size: 25px;
}

.product-block .price {
	color: #024205;
	font-size: 20px;
}

.product-filter .product-compare a {
	display: none;
}

.social li .stack {
	box-shadow: 0 0 0 1px #000;
}

.social li .fa {
	color: #000;
}

#footer .payment {
	color: #000;
}

#footer ul.list li a:hover {
	color: #fdc526;
}

#footer {
	background: #fff;
}

#footer ul.list li a {
	color: #024205
}

#footer .box .box-heading {
	color: #f7c913;
	font-size: 20px;
	font-family: lato;
}

#footer .box-content>p {
	font-size: 16px;
	color: #000;
	font-family: lato;
}

.topbar .quickaccess-toggle, .topbar .search .groupe-btn {
	border-left: none;
}

#pav-mainnav .navbar .navbar-nav>li>a {
	color: #000;
	font-family: signika negative;
}

.topbar .quickaccess-toggle:hover, .topbar .search .groupe-btn:hover {
	color: #fdc526;
}

#pav-mainnav .navbar .navbar-nav .dropdown-menu li a:hover {
	color: #fdc526;
}

.topbar #cart .heading {
	background: none repeat scroll 0 0 #f36b48;
}

.search  input {
	background: #fff !important;
	color: #000 !important;
}

#pav-mainnav .navbar .navbar-nav .dropdown-menu li a:hover {
	border-bottom: 1px solid #fdc526;
}

#pav-mainnav .navbar .navbar-nav>li:hover>a {
	background: #fdc526;
}
</style>
</body>
</html>

