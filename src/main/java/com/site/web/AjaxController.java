package com.site.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import antlr.collections.List;

import com.site.dao.UserManager;
import com.site.entity.Brand;
import com.site.entity.MainCategory;
import com.site.entity.Product;
import com.site.entity.Quotation;
import com.site.entity.SubCategory;
import com.site.entity.User;
import com.site.util.MailManager;
import com.site.util.QuoteOrder;

/**
 * This class is a controller to handle the Ajax requests from the Admin UI. It
 * leverages Direct Web Remoting (DWR) to simplify the Ajax coding.
 */
@Service
@RemoteProxy(name = "AjaxController")
public class AjaxController {
	public static java.util.List<Cookie> cookies = new ArrayList<Cookie>();

	HibernateTemplate ht;
	private int expireTime;

	public int getExpireTime() {
		return expireTime;
	}

	public void setExpireTime(int expireTime) {
		this.expireTime = expireTime;
	}

	@RemoteMethod
	public Long getCountFromCart(Long uid) {

		Long count = userManager.getCountOfCart();
		return count;

	}

	@RemoteMethod
	public int updateCartQty(String uid, Long qty) {

		Long id = Long.parseLong(uid);
		userManager.updatecartqty(id, qty);
		return 1;
	}

	@RemoteMethod
	public int googleFblogin(String name, String email) {
		userManager.googleFblogin(name, email);
		return 1;

	}

	@RemoteMethod
	public java.util.List<MainCategory> getmainlist() {

		java.util.List<MainCategory> items = userManager.getmainlist();
		return items;
	}

	@RemoteMethod
	public java.util.List<SubCategory> getsublist(Long mainid) {

		java.util.List<SubCategory> items = userManager.getsublist(mainid);
		return items;
	}

	@RemoteMethod
	public java.util.List<Brand> brandlistbysubidmainid(Long mainid, Long subid) {
		java.util.List<Brand> items = userManager.brandlistbymubidmainid(mainid, subid);

		return items;
	}

	/*
	 * @RemoteMethod public Long addProductToCart(Long pid, Long qty) { String
	 * name = SecurityContextHolder.getContext().getAuthentication() .getName();
	 * Long count = (long) 0; if (!name.equals(null) &&
	 * !name.equals("anonymousUser")) { int i = 0; i =
	 * userManager.checkexistingInAddToCart(pid); if (i == 1) {
	 * userManager.mergeProductToCart(pid, qty); } else {
	 * userManager.addProductToCart(pid, qty); } count =
	 * userManager.getCountOfCart(); } return count;
	 * 
	 * }
	 */

	@RemoteMethod
	public java.util.List<Product> getproductListBySubCategory(Long[] subCategoryId) {
		String query = "from Product p where p.subCategoryId=" + subCategoryId[0] + " ";
		if (subCategoryId.length > 1) {
			Long[] idlist = Arrays.copyOfRange(subCategoryId, 1, subCategoryId.length);
			for (Long long1 : idlist) {
				query += "or p.subCategoryId= " + long1 + " ";
			}
		}
		java.util.List<Product> items = userManager.getProductByFilter(query);
		return items;

	}

	@RemoteMethod
	public java.util.List<Product> getproductListByBrand(Long[] brandId) {
		String query = "from Product p where p.brandid=" + brandId[0] + " ";
		if (brandId.length > 1) {
			Long[] idlist = Arrays.copyOfRange(brandId, 1, brandId.length);
			for (Long long1 : idlist) {
				query += "or p.brandid= " + long1 + " ";
			}

		}
		java.util.List<Product> items = userManager.getProductByFilter(query);
		return items;

	}

	@RemoteMethod
	public java.util.List<String> getMainCategories(String category) {

		java.util.List<String> products = userManager.getMainCat();

		return products;

	}

	/*
	 * @RemoteMethod public java.util.List<Product> getproductInfo(Long id) {
	 * Long pid=id; java.util.List<Product> products =
	 * userManager.getProductByproductId(pid);
	 * System.out.println("searched products are "+products ); return products;
	 * 
	 * }
	 */
	/* getproductInfo */

	@RemoteMethod
	public java.util.List<Product> getProductInfo(String searchValue) {
		System.out.println();
		// Long pid=null;
		java.util.List<Product> products = userManager.getProductBySearchResult(searchValue);
		;
		System.out.println("searched products " + products);
		return products;
	}

	@RemoteMethod
	public String getValidationByUserName(String name) {
		String error;

		System.out.println();

		if (StringUtils.isBlank(name)) {
			error = "Please enter User name";
			return error;
		} else {
			error = "";

		}
		User userFromDb = userManager.getUser(name);
		if (userFromDb != null) {
			error = "User name not available";
			return error;
		}

		error = "";
		return error;
	}

	@RemoteMethod
	public String getValidationByPassword(String password, String password1) {
		String error = "";
		if (password.length() < 8) {
			error = "Please enter minimum 8 character";
			return error;
		} else if (StringUtils.isNotBlank(password1) && !password.equals(password1)) {
			error = "password and confirm password should be same";
			return error;
		} else {
			error = "";
			return error;
		}

	}

	@RemoteMethod
	public String logout() {

		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		User userFromDb = userManager.getUser(userName);
		/* userFromDb.setOnline(false); */
		userManager.mergeUser(userFromDb);
		return "yes";
	}

	@RemoteMethod
	public String getValidationByConfirmPassword(String password, String password1) {
		String error = "";
		if (password.length() < 8) {
			error = "Please enter minimum 8 character";
			return error;
		} else if (!password.equals(password1)) {
			error = "password and confirm password should be same";
			return error;
		} else {
			error = "";
			return error;
		}
	}

	public static boolean isValidEmailAddress(String str) {

		String at = "@";
		String dot = ".";
		int lat = str.indexOf(at);
		int lstr = str.length();
		int ldot = str.indexOf(dot);
		if (str.indexOf(at) == -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(at, (lat + 1)) != -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(dot, (lat + 2)) == -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(" ") != -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		return true;
	}

	@RemoteMethod
	public String getValidationByEmailAddress(String email) {
		String error;
		if (!isValidEmailAddress(email)) {
			error = "Not valied Email Address";
			return error;
		}
		User userFromDb = userManager.getEmailAddress(email);
		if (userFromDb != null) {
			error = "Email Address not available";
			return error;
		}
		error = "";
		return error;
	}

	@RemoteMethod
	public String getfeedBack(String feedback) {

		return feedback;

	}

	/*
	 * @RemoteMethod public java.util.List<Product> getSearchProduct(Long oid){
	 * Long id=oid; //userManager.getProduct(id); Product products=null;
	 * products=userManager.getProduct(id);
	 * System.out.println("Products are "+products); return null;
	 * 
	 * }
	 */

	@RemoteMethod
	public java.util.List<Product> getSearchProduct(Long oid) {
		Long id = oid;
		Product order = null;
		// String[] productIdList = { " " };
		// java.util.List<Product> productDtoList = new ArrayList<Product>();
		order = userManager.getProduct(id);
		/*
		 * String productIds = order.getProId(); if (productIds.contains(",")) {
		 * productIdList = productIds.split(",");
		 * 
		 * for (String str : productIdList)
		 * productDtoList.add(addProductToproductDto(str, order));
		 * 
		 * } else productDtoList.add(addProductToproductDto(productIds, order));
		 * 
		 * return productDtoList;
		 */
		return null;
	}

	@RemoteMethod
	public String addProductToCart(String pid, String qty) {

		Long uid;
		String uname = SecurityContextHolder.getContext().getAuthentication().getName();
		if (uname.equals("anonymousUser") || uname.equals(null)) {

			String prod = pid + "_" + qty;
			Cookie product = new Cookie("prod" + pid, prod);
			cookies.add(product);

		} else {
			User user = userManager.getUserByName(uname);
			uid = user.getId();

		}

		return qty;

	}

	@Autowired
	private UserManager userManager;

	@Autowired
	private MailManager mailManager;

	/*
	 * public Long getCount(long parseLong, long parseLong2, Long id) { // TODO
	 * Auto-generated method stub
	 * 
	 * Long count = userManager.getCountOfCart(); return count; }
	 */
	@RemoteMethod
	public Long getCount(long id1, long id2, Long id3) {

		Long count = userManager.getCountOfCart(id1);
		return count;

	}

	@RemoteMethod
	public String saveSearch(String list) {
		System.out.println();
		String[] plist = list.split(","); // plist[0]=12_12)_144; plist[1]=
		/*String design = "<table> <tr> <td> Product name </td><td> Prodcut price</td><td> Product qty</td><td>Grand Price</td></tr></table>";*/
		String design="<table style='width:100%'> <tr> <th>Product Name</th><th>Product Quantity</th> <th>Grand Price</th> </tr></table>";
		
		String table="";
		String quantity = "";
		String prdName="";
		long amount = 0;
		int length = 0;
		long grand = 0;
		long id=0;
		long totQuantity=0;
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = new User();
		user = userManager.getActiveUser(username);
		long uId = user.getId();
		String email=user.getEmailAddress();
		// u need to get logged in user, user id
		UUID token = UUID.randomUUID();
		for (int i = 0; i < plist.length; i++) {
			long amt = 0;
			String[] pdetails = plist[i].split("_"); // pdetails[0]=12;pdetails[1]=12;pdetails[2]=144;
		//	System.out.println("product details are " + pdetails);
			
			length = plist.length;
			//System.out.println("total length " + length);

			id = Long.parseLong(pdetails[2]);
		//	System.out.println("id are " + id);
			// you need get product object by product id
			
			/*prodName=pdetails[2];
			System.out.println("product name is "+prodName);*/
			
			quantity = pdetails[0];
		//	System.out.println("quantity " + quantity);
			totQuantity+=Long.parseLong(quantity);
			
			amount = Integer.parseInt(pdetails[1]);
			//System.out.println("amount is " + amount);
			long total = 0;
			total = amount * Integer.parseInt(quantity);
		//	System.out.println("total amount is " + total);
			// you need to desing html table with dynamic value
			/*
			 * table+="<tr> <td>"+ productobject.getProductNAME <td> prodcut
			 * price<td> AMT qty<td>quantity<price></td></tr>"
			 */
			grand = grand + total;
			//System.out.println("grand total is " + grand);
			String prodName="";
			Product prod=new Product();
			prod=userManager.getProduct(id);
			prodName= prod.getProductName();
			System.out.println("Product names are "+prodName);
			prdName=prdName+prodName;
			
		
			
			
		//if (user == null) {
			
			
			Quotation qu = new Quotation();
			qu.setUserId(uId);
			qu.setQuantity(quantity);             
			qu.setGrandPrice(grand);
			qu.setProductName(prodName);
			qu.setActive(false);
			qu.setToken(token);
			qu.setEmailAddress(email);
			userManager.saveQuotation(qu);
			//}
		}
		 /*table+="<tbody><tr>"+ grand +"</tr><tr>"+ quantity +"</tr><tr>"+ grand +"</tr></tbody>";*/
		/*table+="<table> <tr> <td>"+ prodName +"</td><td>"+ totQuantity +"</td><td>"+ grand +"</td></tr></table>";*/
		table="<table style='width:100%'> <tr> <th>"+ prdName +"</th> <th>"+ totQuantity +"</th> <th>"+ grand +"</th></table>";
		  Map<String, Object> data = new HashMap<String, Object>();
		  data.put("token", token);
		  data.put("design", design); 
		  data.put("table", table);
		  data.put("firstName",user.getFirstName()); 
		  String template = null;
		  template = "QuotationStatus";
		  String subject = "Quotation"+id;
		  String toName = username;
		  String toAddress = username;
		  mailManager.sendEmail(subject, data,
		  template, toName, toAddress);
		return "success";
	}
}
