package com.site.web;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.site.dao.UserManager;
import com.site.entity.AddToCart;
import com.site.entity.Brand;
import com.site.entity.ContactUs;
import com.site.entity.Feedback;
import com.site.entity.Order;
import com.site.entity.Product;
import com.site.entity.Quotation;
import com.site.entity.SubCategory;
import com.site.entity.User;
import com.site.util.MailManager;
import com.site.util.Ordertableitems;

@Controller
public class UserController {

	private static final Logger logger = Logger.getLogger(UserController.class.getName());
	AjaxController ajaxController;

	@Autowired
	MailManager mailManager;

	@Autowired(required = false)
	AuthenticationManager authManager;

	@Autowired
	private UserManager userManager;
	private static final int IMG_WIDTH = 100;
	private static final int IMG_HEIGHT = 100;
	private Object response;

	@RequestMapping(value = "/home.mm", method = RequestMethod.HEAD)
	public void doHealthCheck(HttpServletResponse response) {
		response.setContentLength(0);
		response.setStatus(HttpServletResponse.SC_OK);
	}

	@RequestMapping(value = "/admindashboard.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doAdmindashboard(ModelMap model, HttpServletRequest request, HttpSession session) {
		return null;
	}

	/*
	 * @RequestMapping(value = "/addToCart.mm", method = { RequestMethod.GET,
	 * RequestMethod.POST }) public String doAddToCart( ModelMap model,
	 * HttpServletRequest request, HttpSession session,
	 * 
	 * @RequestParam(value = "savedChanges", required = false) String
	 * savedChanges,
	 * 
	 * @RequestParam(value = "id", required = false) Long id) { String name =
	 * SecurityContextHolder.getContext().getAuthentication() .getName();
	 * List<User> list = userManager.getUserDetails(name); if (!list.isEmpty())
	 * { List<AddToCart> cartproducts = userManager.getProductfromCart(id);
	 * loginLogoutStatus(model); List<com.site.util.ShoppingCartItems>
	 * shoppingCartItems = new ArrayList<com.site.util.ShoppingCartItems>();
	 * Product product = null; for (AddToCart ac : cartproducts) {
	 * com.site.util.ShoppingCartItems sci = new
	 * com.site.util.ShoppingCartItems();
	 * sci.setQuantity(ac.getQuantity().toString());
	 * sci.setAddToCardId(ac.getId().toString()); try { product =
	 * userManager.getProduct(ac.getProductId()); } catch (Exception e) {
	 * logger.info("error fetching the product details " + e); }
	 * sci.setProductName(product.getProductName());
	 * sci.setProductPrice(product.getProductSellingPrice());
	 * sci.setImage(product.getImage());
	 * sci.setPdtId(product.getId().toString()); shoppingCartItems.add(sci); }
	 * model.addAttribute("cartproducts", shoppingCartItems); return null; }
	 * else { return "redirect:login.mm"; } }
	 */

	@RequestMapping(value = "/addToCart.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doAddToCart(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "savedChanges", required = false) String savedChanges,
			@RequestParam(value = "id", required = false) Long id) {

		List<AddToCart> cartproducts = new ArrayList<AddToCart>();
		List<com.site.util.ShoppingCartItems> shoppingCartItems = new ArrayList<com.site.util.ShoppingCartItems>();
		String uName = null;

		if (SecurityContextHolder.getContext().getAuthentication() != null)
			uName = SecurityContextHolder.getContext().getAuthentication().getName();
		System.out.println("user banme " + uName);

		if (uName != null && !uName.equals("anonymousUser") && !uName.isEmpty()) {
			System.out.println("name s " + uName);
			cartproducts = userManager.getProductfromCart(id);
			System.out.println("cart pro are " + cartproducts);
		} else {
			if (ajaxController.cookies != null && ajaxController.cookies.size() > 0 && uName != null
					&& !uName.isEmpty()) {

				for (Cookie cook : ajaxController.cookies) {
					String[] cooks = cook.getValue().split("_");
					Product product = userManager.getProduct(Long.parseLong(cooks[0]));
					com.site.util.ShoppingCartItems sci = new com.site.util.ShoppingCartItems();
					sci.setQuantity(cooks[1]);
					sci.setProductName(product.getProductName());
					sci.setProductPrice(product.getProductSellingPrice());
					sci.setImage(product.getImage());
					sci.setPdtId(product.getId().toString());
					shoppingCartItems.add(sci);
				}
			}
		}

		Product product = null;
		for (AddToCart ac : cartproducts) {
			com.site.util.ShoppingCartItems sci = new com.site.util.ShoppingCartItems();
			sci.setQuantity(ac.getQuantity().toString());
			sci.setAddToCardId(ac.getId().toString());
			try {
				product = userManager.getProduct(ac.getProductId());
			} catch (Exception e) {
				logger.info("error fetching the product details " + e);
			}
			sci.setProductName(product.getProductName());
			sci.setProductPrice(product.getProductSellingPrice());
			sci.setImage(product.getImage());
			sci.setPdtId(product.getId().toString());
			shoppingCartItems.add(sci);
			System.out.println(" carts " + sci);
		}
		model.addAttribute("cartproducts", shoppingCartItems);

		return null;
	}

	@RequestMapping(value = "/paymentconfirm.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String dopayementconfirm(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "paid", required = false) String paymentinfo) {
		if (paymentinfo.equals("yes")) {
			String uname = SecurityContextHolder.getContext().getAuthentication().getName();
			User user = userManager.getUserByName(uname);
			List<AddToCart> item = userManager.getProductfromOrderTable();
			List<Ordertableitems> items = new ArrayList<Ordertableitems>();
			int i = 0;
			String products = null;
			for (AddToCart addToCart : item) {
				Long pid = addToCart.getProductId();
				Long qty = addToCart.getQuantity();
				List<Product> pdtitems = userManager.getProductByproductId(pid);
				Product pdtitem = pdtitems.get(0);
				String pdtname = pdtitem.getProductName();
				Ordertableitems itemsingle = new Ordertableitems();
				itemsingle.setPdtname(pdtname);
				itemsingle.setQty(qty.toString());
				items.add(itemsingle);
				if (i == 0)
					products = pid + "_" + qty;
				else
					products += "," + pid + "_" + qty;
				i++;
			}
			Order order = new Order();
			order.setDate(new Date());
			order.setProId(products);
			order.setEmail(user.getEmailAddress());
			order.setStatus("pending");
			order.setUserName(user.getUserName());
			order.setPhoneNumber(user.getPhoneNumber());
			userManager.saveOrder(order);
			// userManager.setProductToOrderTable();
			model.addAttribute("paymentinfomsg",
					"payment successfully recieved,your order placed and the order details are");
			model.addAttribute("orderdetails", items);

			// USER MAILING PART

			Map<String, Object> data = new HashMap<String, Object>();
			String email = SecurityContextHolder.getContext().getAuthentication().getName();
			List<User> userdetails = userManager.getUserDetails(email);
			User detail = userdetails.get(0);
			data.put("name", detail.getFirstName());
			data.put(products, products);
			String subject = "Order confirmation";
			String template = null;
			template = "orderConfirmation";
			String toName = email;
			String toAddress = email;
			mailManager.sendEmail(subject, data, template, toName, toAddress);

			// ADMIN MAILING PART

			String templateadmin = null;
			templateadmin = "orderconfirmationadmin";
			String admin = "Admin";
			List<User> admindetails = userManager.getUserDetailsbytype(admin);
			User admindetail = admindetails.get(0);
			admindetail.getEmailAddress();
			Map<String, Object> dataadmin = new HashMap<String, Object>();
			dataadmin.put("name", admindetail.getFirstName());
			dataadmin.put("userName", detail.getFirstName());
			String toAdmin = admindetail.getEmailAddress();
			String toAdminAddress = admindetail.getEmailAddress();
			mailManager.sendEmail(subject, dataadmin, templateadmin, toAdmin, toAdminAddress);

			// SELLER MAILING PART

			String templateseller = null;
			templateseller = "orderconfirmationadmin";
			String seller = "Seller";
			List<User> sellerdetails = userManager.getUserDetailsbytype(seller);
			User sellerdetail = sellerdetails.get(0);
			sellerdetail.getEmailAddress();
			Map<String, Object> dataseller = new HashMap<String, Object>();
			dataseller.put("name", sellerdetail.getFirstName());
			dataseller.put("userName", detail.getFirstName());
			String toSeller = sellerdetail.getEmailAddress();
			String toSellerAddress = sellerdetail.getEmailAddress();
			mailManager.sendEmail(subject, dataseller, templateseller, toSeller, toSellerAddress);

		} else if (paymentinfo.equals("cod")) {
			String uname = SecurityContextHolder.getContext().getAuthentication().getName();
			User user = userManager.getUserByName(uname);
			List<AddToCart> item = userManager.getProductfromOrderTable();
			List<Ordertableitems> items = new ArrayList<Ordertableitems>();
			int i = 0;
			String products = null;
			for (AddToCart addToCart : item) {
				Long pid = addToCart.getProductId();
				Long qty = addToCart.getQuantity();
				List<Product> pdtitems = userManager.getProductByproductId(pid);
				Product pdtitem = pdtitems.get(0);
				String pdtname = pdtitem.getProductName();
				Ordertableitems itemsingle = new Ordertableitems();
				itemsingle.setPdtname(pdtname);
				itemsingle.setQty(qty.toString());
				items.add(itemsingle);

				if (i == 0)
					products = pid + "_" + qty;
				else
					products += "," + pid + "_" + qty;
				i++;
			}
			Order order = new Order();
			order.setDate(new Date());
			order.setProId(products);
			order.setEmail(user.getEmailAddress());
			order.setStatus("pending");
			order.setUserName(user.getUserName());
			order.setPhoneNumber(user.getPhoneNumber());
			order.setUserId(user.getId());
			userManager.saveOrder(order);
			// userManager.setProductToOrderTable();
			model.addAttribute("paymentinfomsg",
					"payment successfully recieved,your order placed and the order details are");
			model.addAttribute("orderdetails", items);
			/*
			 * try {
			 * 
			 * userManager.setProductToOrderTable(); } catch (Exception e) {
			 * logger.info("Error in set product to order table" + e); }
			 * model.addAttribute("paymentinfomsg",
			 * "your order successfully placed and your order details are");
			 * model.addAttribute("orderdetails", items);
			 */

			// USER MAILING PART

			Map<String, Object> data = new HashMap<String, Object>();
			String email = SecurityContextHolder.getContext().getAuthentication().getName();
			List<User> userdetails = userManager.getUserDetails(email);
			User detail = userdetails.get(0);
			data.put("name", detail.getFirstName());
			String subject = "Order confirmation";
			String template = null;
			template = "orderConfirmation";
			String toName = email;
			String toAddress = email;
			mailManager.sendEmail(subject, data, template, toName, toAddress);

			// ADMIN MAILING PART

			String templateadmin = null;
			templateadmin = "orderconfirmationadmin";
			String admin = "Admin";
			List<User> admindetails = userManager.getUserDetailsbytype(admin);
			User admindetail = admindetails.get(0);
			admindetail.getEmailAddress();
			Map<String, Object> dataadmin = new HashMap<String, Object>();
			dataadmin.put("name", admindetail.getFirstName());
			dataadmin.put("userName", detail.getFirstName());
			String toAdmin = admindetail.getEmailAddress();
			String toAdminAddress = admindetail.getEmailAddress();
			mailManager.sendEmail(subject, dataadmin, templateadmin, toAdmin, toAdminAddress);

			// SELLER MAILING PART

			String templateseller = null;
			templateseller = "orderconfirmationadmin";
			String seller = "Seller";
			List<User> sellerdetails = userManager.getUserDetailsbytype(seller);
			User sellerdetail = sellerdetails.get(0);
			sellerdetail.getEmailAddress();
			Map<String, Object> dataseller = new HashMap<String, Object>();
			dataseller.put("name", sellerdetail.getFirstName());
			dataseller.put("userName", detail.getFirstName());
			String toSeller = sellerdetail.getEmailAddress();
			String toSellerAddress = sellerdetail.getEmailAddress();
			mailManager.sendEmail(subject, dataseller, templateseller, toSeller, toSellerAddress);

		}

		return null;
	}

	@RequestMapping(value = "/shipping.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void shipping_order(

	ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	@RequestMapping(value = "/forgotPassword.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doForgotPassword(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "savedChanges", required = false) String savedChanges,
			@RequestParam(value = "id", required = false) Long id) {
		return null;
	}

	@RequestMapping(value = "/passwordRecovery.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doPasswordRecovery(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "savedChanges", required = false) String savedChanges,
			@RequestParam(value = "id", required = false) Long id) {
		return null;
	}

	@RequestMapping(value = "/adminDashboard.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doadminDashboard(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "savedChanges", required = false) String savedChanges) {
		return null;
	}

	public void loginLogoutStatus(ModelMap model) {
		String uName = null;
		if (SecurityContextHolder.getContext().getAuthentication() != null)

			uName = SecurityContextHolder.getContext().getAuthentication().getName();

		if (uName != null && !uName.equals("anonymousUser"))
			model.addAttribute("loggedin", true);
		else
			model.addAttribute("loggedin", false);

	}

	@RequestMapping(value = "/checkout.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doCheckout(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "savedChanges", required = false) String savedChanges,
			@RequestParam(value = "id", required = false) Long id) {

		User userFromDb = null;
		String uName = SecurityContextHolder.getContext().getAuthentication().getName();
		loginLogoutStatus(model);
		if (uName.equals("anonymousUser") || uName.equals(null)) {
			System.out.println("you are logged out");
			return "redirect:login.mm";

		} else {
			System.out.println("you are  logged in");
		}

		List<AddToCart> cartproducts = userManager.getProductfromCart(id);

		List<com.site.util.ShoppingCartItems> shoppingCartItems = new ArrayList<com.site.util.ShoppingCartItems>();
		Product product = null;
		for (AddToCart ac : cartproducts) {
			com.site.util.ShoppingCartItems sci = new com.site.util.ShoppingCartItems();
			sci.setQuantity(ac.getQuantity().toString());
			sci.setAddToCardId(ac.getId().toString());
			try {
				product = userManager.getProduct(ac.getProductId());
			} catch (Exception e) {
				logger.info("error fetching the product details " + e);
			}
			sci.setProductName(product.getProductName());
			sci.setProductPrice(product.getProductSellingPrice());
			sci.setImage(product.getImage());
			sci.setPdtId(product.getId().toString());
			shoppingCartItems.add(sci);
		}
		String uname = SecurityContextHolder.getContext().getAuthentication().getName();
		List<User> user = userManager.getUserDetails(uname);
		User userSingle = user.get(0);
		model.addAttribute("userDetails", userSingle);
		model.addAttribute("cartproducts", shoppingCartItems);

		return null;
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/uploadPhoto.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doUploadPhoto(ModelMap model, HttpSession session, HttpServletRequest request,
			HttpServletResponse response, @RequestParam("file") MultipartFile imageFile, @RequestParam("id") Long id)
			throws IOException, IllegalStateException, ServletException {
		byte[] photo = null;
		@SuppressWarnings("unused")
		String newFileName = null;
		/*
		 * FileInputStream fis = new FileInputStream(imageFile.toString()); Part
		 * filePart = request.getPart("photo");
		 */
		User userFDb = null;
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		if (userName != null)
			userFDb = userManager.getUser(userName);

		if (!(imageFile.isEmpty())) {
			newFileName = imageFile.getOriginalFilename().toString();

			photo = imageFile.getBytes();
			Product productFromDb = userManager.getProductByProductId(id);
			if (productFromDb != null) {
				productFromDb.setImage(photo);
				userManager.mergeProduct(productFromDb);
			}

		}

		return "redirect:welcome.mm";
	}

	@RequestMapping(value = "/image.mm", method = { RequestMethod.GET, RequestMethod.POST })
	protected void doImage(HttpServletRequest request, HttpServletResponse response,
			@RequestParam(value = "id", required = false) Long id) throws ServletException, IOException {

		byte[] image = null;
		if (id != null) {
			Product product = userManager.getProduct(id);
			image = product.getImage();
		}

		response.setContentType("image/jpeg");
		ServletOutputStream outputStream = response.getOutputStream();
		outputStream.write(image);
		outputStream.close();

	}

	private static BufferedImage resizeImage(BufferedImage originalImage, int type) {
		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();

		return resizedImage;
	}

	@RequestMapping(value = "/productDetails.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doProductDetails(ModelMap model, @RequestParam(value = "pdt-details", required = true) Long pdtid) {
		loginLogoutStatus(model);
		List<Product> products = userManager.getProductByIdSingle(pdtid);
		Product productsingle = products.get(0);
		model.addAttribute("productsingle", productsingle);

		/*
		 * List<Product> pro = userManager.getProductList();
		 * model.addAttribute("pro", pro);
		 */

	}

	@RequestMapping(value = "/shop.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doShop(ModelMap model, @RequestParam(value = "mainid", required = false) Long mainid,
			@RequestParam(value = "subid", required = false) Long subid,
			@RequestParam(value = "brandid", required = false) Long brandid,
			@RequestParam(value = "searchval", required = false) String searchValue) {
		List<Product> products = null;
		if (mainid != null && subid == null) {
			products = userManager.getProductById(mainid);
			List<SubCategory> categories = userManager.getSubCategory(mainid);
			model.addAttribute("product", products);
			model.addAttribute("subcategories", categories);
			model.addAttribute("subcategoryHeading", "Sub Category");

		} else if (mainid != null && subid != null && brandid == null) {
			products = userManager.getProductByMainidAndSubid(mainid, subid);
			List<Brand> brands = userManager.getBrandsByMainidAndSubid(mainid, subid);
			model.addAttribute("product", products);
			model.addAttribute("BrandHeading", "");
			model.addAttribute("brands", brands);

		} else if (mainid != null && subid != null && brandid != null) {
			products = userManager.getProductByMainidAndSubidAndBrandid(mainid, subid, brandid);
			model.addAttribute("product", products);
		} else {
			products = userManager.getProductBySearchResult(searchValue);

			model.addAttribute("product", products);
			model.addAttribute("categories", null);
			model.addAttribute("brands", null);
		}

		return null;
	}

	@RequestMapping(value = "/quote.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doQuote(ModelMap model, @RequestParam(value = "mainid", required = false) Long mainid,
			@RequestParam(value = "subid", required = false) Long subid,
			@RequestParam(value = "brandid", required = false) Long brandid,
			@RequestParam(value = "searchval", required = false) String searchValue) {
		List<Product> products = null;
		if (mainid != null && subid == null) {
			products = userManager.getProductById(mainid);
			List<SubCategory> categories = userManager.getSubCategory(mainid);
			model.addAttribute("product", products);
			model.addAttribute("subcategories", categories);
			model.addAttribute("subcategoryHeading", "Sub Category");

		} else if (mainid != null && subid != null && brandid == null) {
			products = userManager.getProductByMainidAndSubid(mainid, subid);
			List<Brand> brands = userManager.getBrandsByMainidAndSubid(mainid, subid);
			model.addAttribute("product", products);
			model.addAttribute("BrandHeading", "");
			model.addAttribute("brands", brands);

		} else if (mainid != null && subid != null && brandid != null) {
			products = userManager.getProductByMainidAndSubidAndBrandid(mainid, subid, brandid);
			model.addAttribute("product", products);
		} else {
			products = userManager.getProductBySearchResult(searchValue);

			model.addAttribute("product", products);
			model.addAttribute("categories", null);
			model.addAttribute("brands", null);
		}

		return null;
	}

	@RequestMapping("/welcome.mm")
	public void doWelcome(HttpSession session, ModelMap model, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		// loginLogoutStatus(model);
		List<Product> pro = userManager.getProductList();
		model.addAttribute("pro", pro);

	}

	private boolean isAuthenticated(String username, String password) {

		Authentication request = new UsernamePasswordAuthenticationToken(username, password);
		Authentication result = authManager.authenticate(request);
		SecurityContextHolder.getContext().setAuthentication(result);
		return result.isAuthenticated();
	}

	@RequestMapping(value = "/contactus.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doContactus(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "subject", required = false) String subject,
			@RequestParam(value = "message", required = false) String message,
			@RequestParam(value = "mobile", required = false) String mobile

	) {
		System.out.println();

	}

	@RequestMapping(value = "/contactSuccess.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doContactSuccess(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "subject", required = true) String subjects,
			@RequestParam(value = "message", required = true) String message,
			@RequestParam(value = "mobile", required = false) Long mobile) {

		ContactUs contact = new ContactUs();
		contact.setName(name);
		contact.setEmail(email);
		contact.setMessage(message);
		contact.setSubject(subjects);
		contact.setMobileNumber(mobile);
		userManager.stoteContact(contact);

		Map<String, Object> data = new HashMap<String, Object>();

		/* data.put("name", contact.setEmail(email)); */
		String template = null;
		template = "contactUs";

		String subject = "User Welcome";
		String toName = email;
		String toAddress = email;
		mailManager.sendEmail(subject, data, template, toName, toAddress);

		return "redirect:welcome.mm";

	}

	/*
	 * @RequestMapping(value = "/userLogin.mm", method = { RequestMethod.GET,
	 * RequestMethod.POST }) public String doUserLogin( ModelAndView modelview,
	 * ModelMap model, HttpServletRequest request,
	 * 
	 * @RequestParam(value = "j_username", required = true) String userName,
	 * 
	 * @RequestParam(value = "j_password", required = true) String password,
	 * 
	 * @RequestParam(value = "logoutparm", required = false) String logoutparm,
	 * HttpSession session) { if (logoutparm != null) { session.invalidate();
	 * SecurityContextHolder.getContext().setAuthentication(null); }
	 * 
	 * UserDetails details = userManager.getActiveUser(userName); if (details !=
	 * null && StringUtils.isNotBlank(details.getPassword()) &&
	 * details.getPassword().equals(password)) { User user =
	 * userManager.getUser(userName); if (user != null &&
	 * !user.getAccountType().equals("admin")) {
	 * 
	 * isAuthenticated(userName, password);
	 * 
	 * String uName = SecurityContextHolder.getContext()
	 * .getAuthentication().getName(); return "redirect:welcome.mm"; } else
	 * return "redirect:login.mm?error=1"; } else return
	 * "redirect:login.mm?error=1"; }
	 */
	@RequestMapping(value = "/userLogin.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doUserLogin(ModelAndView modelview, ModelMap model, HttpServletRequest request,
			@RequestParam(value = "j_username", required = false) String userName,
			@RequestParam(value = "j_password", required = false) String password,
			@RequestParam(value = "logoutparm", required = false) String logoutparm, HttpSession session) {

		String str = (String) session.getAttribute("ram");
		System.out.println(str);
		if (logoutparm != null) {

			SecurityContextHolder.getContext().setAuthentication(null);
		}
		UserDetails details = userManager.getActiveUser(userName);
		if (details != null && StringUtils.isNotBlank(details.getPassword()) && details.getPassword().equals(password)) {
			User user = userManager.getUser(userName);
			if (user != null && !user.getAccountType().equals("admin")) {

				isAuthenticated(userName, password);

				String uName = SecurityContextHolder.getContext().getAuthentication().getName();

				user = userManager.getUser(uName);
				System.out.println();

				if (ajaxController.cookies != null && ajaxController.cookies.size() > 0) {
					for (Cookie cook : ajaxController.cookies) {
						String[] prd = cook.getValue().split("_");
						AddToCart atc = null;
						atc = userManager.getAddToCartByProductId(Long.parseLong(prd[0]), user.getId());
						if (atc == null) {

							userManager.addProductToCart(Long.parseLong(prd[0]), Long.parseLong(prd[1]));

						}

						// getCount(Long.parseLong(prd[0]),
						// Long.parseLong(prd[1]), user.getId());
					}
					ajaxController.cookies.clear();
				}
				return "redirect:welcome.mm";
			} else
				return "redirect:login.mm?error=1";
		} else
			request.setAttribute("errormsg", "Invalid Credentials");
		return "redirect:login.mm?error=1";
		// String errormsg="Invalid Credentials !";
		// map.put("errMsg",errormsg);
		/* RedirectAttributes.addFlashAttribute("errMsg",errormsg); */
		/* ModelAndView("errmsg",errormsg); */
		// request.setAttribute("errorMessage",
		// "Amount of items ordered is too big. No more than 100 is currently available.");

	}

	@RequestMapping(value = "/signup.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doSignup(ModelMap model, HttpServletRequest request, HttpSession session) {
	}

	public Long getCount(long id1, long id2, Long id3) {

		Long count = userManager.getCountOfCart(id1);
		return count;

	}

	@RequestMapping(value = "/register.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doRegister(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "firstName", required = true) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "mobileNo", required = true) String mobileNo,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "confirmPassword", required = true) String confirmPassword,
			@RequestParam(value = "permanentAddress", required = false) String permanentAddress,
			@RequestParam(value = "shippingAddress", required = true) String shippingAddress, HttpSession session) {
		System.out.println();
		User userFromDb = null;
		userFromDb = userManager.getEmailAddress(email);
		UUID token = UUID.randomUUID();
		if (userFromDb == null) {

			User user = new User();
			Date date = new Date();
			user.setActive(false);
			user.setToken(token);
			user.setAccountType("User");
			user.setCreateDate(date);
			user.setPassword(password);
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmailAddress(email);
			user.setUserName(email);
			user.setPermanentAddress(permanentAddress);
			user.setShippingAddress(shippingAddress);
			user.setPhoneNumber(mobileNo);
			userManager.saveUser(user);
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("token", token);
			data.put("firstName", user.getFirstName());
			String template = null;
			template = "userWelcome";
			String subject = "User Welcome";
			String toName = email;
			String toAddress = email;
			mailManager.sendEmail(subject, data, template, toName, toAddress);

		}

	}

	@RequestMapping(value = "/userTypes.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doLogin(ModelMap model, HttpServletRequest request,

	HttpSession session) {

		System.out.println();
	}

	@RequestMapping(value = "/userTypes.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doUserTypes(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "user", required = false) UUID token, HttpSession session) {
		model.addAttribute("user", token);
		System.out.println();
	}

	@RequestMapping(value = "/activation.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView doActivation(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "token", required = true) UUID token, HttpSession session) {
		User user = null;
		user = userManager.getUserByToken(token);
		if (user != null) {
			user.setActive(true);

			userManager.mergeUser(user);
		} else
			return new ModelAndView("redirect:welcome.mm");

		return new ModelAndView("redirect:login.mm");

	}

	@RequestMapping(value = "/forgot_password.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doForgot_password(

	ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	@RequestMapping(value = "/forgot_username.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doForgot_username(

	ModelMap model, HttpServletRequest request, HttpSession session) {

	}

	@RequestMapping(value = "/password_recovery_request.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doPassword_recovery_request(@RequestParam(value = "email", required = true) String email,
			ModelMap model, HttpServletRequest request, HttpSession session) {
		// String username = email.toString();
		User userFromDb = userManager.getEmailAddress(email);

		if (userFromDb == null) {
			return "redirect:forgot_password.mm?error=1";
		}

		String subject = "Your password recovery request ";
		Map<String, Object> data = new HashMap<String, Object>();

		data.put("token", userFromDb.getToken());
		String template = "resetpassword";
		String toName = email;
		String toAddress = email;
		mailManager.sendEmail(subject, data, template, toName, toAddress);
		return "password_recovery_request";
	}

	@RequestMapping(value = "/resetpassword_new.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doResetpassword_new(@RequestParam(value = "token", required = true) UUID token,
			@RequestParam(value = "error", required = false) String error, ModelMap model, HttpServletRequest request,
			HttpSession session) {
		User userFromDb = userManager.getUserByToken(token);
		String email = userFromDb.getEmailAddress();
		model.addAttribute("email", email);
	}

	@RequestMapping(value = "/resetpassword.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doResetpassword(@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "cpassword", required = false) String cpassword, ModelMap model,
			HttpServletRequest request, HttpSession session) {
		User userFromDB = userManager.getEmailAddress(email);
		UUID token = userFromDB.getToken();
		if (password != null && password.length() < 8)
			return "redirect:resetpassword_new.mm?token=" + token + "&error=2";
		else if (cpassword != null && cpassword.length() < 8)
			return "redirect:resetpassword_new.mm?token=" + token + "&error=3";
		else if (!(password.equals(cpassword)) || password.equals("") || password.equals(null))
			return "redirect:resetpassword_new.mm?token=" + token + "&error=1";

		else {

			userFromDB.setPassword(password);
			userManager.mergeUser(userFromDB);
			isAuthenticated(userFromDB.getUserName(), password);
			return "redirect:home.mm";
		}
	}

	@RequestMapping(value = "/thanksForUserAccount.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doThanksForUserAccount(ModelMap model, HttpServletRequest request, HttpSession session) {
	}

	@RequestMapping(value = "/welcome.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doWelcome(ModelMap model, HttpServletRequest request) {
		loginLogoutStatus(model);

		List<Product> pro = userManager.getProductList();
		model.addAttribute("pro", pro);

	}

	@RequestMapping(value = "/login.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doLogin(@RequestParam(value = "resetPassword", required = false) String reset, ModelMap model) {
		Boolean resetPassword = false;
		if (reset != null && reset.equals("success"))
			resetPassword = true;
		model.addAttribute("resetPassword", resetPassword);

	}

	public static boolean isValidEmailAddress(String str) {

		String at = "@";
		String dot = ".";
		int lat = str.indexOf(at);
		int lstr = str.length();
		int ldot = str.indexOf(dot);
		if (str.indexOf(at) == -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(at, (lat + 1)) != -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(dot, (lat + 2)) == -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		if (str.indexOf(" ") != -1) {
			System.out.println("Invalid E-mail ID");
			return false;
		}

		return true;
	}

	@RequestMapping(value = "/notActivate.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doNotActivate(ModelMap model, HttpServletRequest request) throws UnsupportedEncodingException {
		System.out.println();
	}

	public void logout(String userName) {
		User userFromDb = userManager.getUser(userName);
		/* userFromDb.setOnline(false); */
		userManager.mergeUser(userFromDb);
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

	@RequestMapping(value = "/userOrderList.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doUserList(ModelMap model, @RequestParam(value = "orderId", required = false) Long id,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "userName", required = false) String userName) {
		List<Order> orderList = new ArrayList<Order>();
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		List<User> user = userManager.getUserDetails(name);
		System.out.println("user is " + user);
		if (user != null)
			orderList = userManager.getOrderList(id, status);
		model.addAttribute("orderList", orderList);

	}

	@RequestMapping(value = "/QuotationList.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doQuotationList(ModelMap model, @RequestParam(value = "orderId", required = false) Long id,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "userName", required = false) String userName) {
		// List<Order> orderList = new ArrayList<Order>();
		List<Quotation> quotation = new ArrayList<Quotation>();
		String name = SecurityContextHolder.getContext().getAuthentication().getName();
		List<User> user = userManager.getUserDetails(name);

		System.out.println("user is " + user);
		if (user != null)
			// orderList = userManager.getOrderList(id, status);
			quotation = userManager.getQuotationList(name);
		model.addAttribute("quotation", quotation);

	}

	@RequestMapping(value = "/feedbacks.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doFeedbacks(ModelMap model) {
	}

	@RequestMapping(value = "/cancel.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doUserOrderList(ModelMap model, @RequestParam(value = "orderId", required = false) Long oId,
			@RequestParam(value = "tosId", required = false) String status) {

		Long id = oId;
		Order order = userManager.getOrderById(id);
		order.setStatus("Cancelled");
		userManager.mergeOrder(order);
		if (order.getStatus().equals("Cancelled")) {
			Map<String, Object> data = new HashMap<String, Object>();

			System.out.println("Email is " + order.getEmail());
			String cancel = "cancelled";
			data.put("name", cancel);
			String template = null;
			template = "shipping";
			String subject = "Order Cancel confirmation";
			String toName = order.getUserName();
			String toAddress = order.getEmail(); // String toAddress = email;
			mailManager.sendEmail(subject, data, template, toName, toAddress);

			return "redirect:cancel.mm";
		}

		return null;
	}

	@RequestMapping(value = "/quotation.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doQuotation(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "searchval", required = false) String searchValue,
			@RequestParam(value = "qutDetails", required = false) String qutDetails, HttpSession session) {
		List<Product> products = null;
		if (StringUtils.isNotBlank(searchValue)) {
			products = userManager.getProductBySearchResult(searchValue);
		}
		model.addAttribute("product", products);
		model.addAttribute("categories", null);
		model.addAttribute("brands", null);
		if (qutDetails != null)
			model.addAttribute("qutDetails", qutDetails);
	}

	@RequestMapping(value = "sendfeedback.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doSubmitFeed(HttpServletRequest request,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "number", required = false) String mobile,
			@RequestParam(value = "category", required = true) String category,
			@RequestParam(value = "ratings", required = true) String ratings,
			@RequestParam(value = "message", required = true) String message, HttpSession session) {
		Feedback fb = new Feedback();
		Date date = new Date();
		fb.setEmail(email);
		fb.setMobile(mobile);
		fb.setCategory(category);
		fb.setRating(ratings);
		fb.setMessage(message);
		fb.setDate(date);
		userManager.saveFeedback(fb);
		return "redirect:welcome.mm";
	}

	@RequestMapping(value = "/cancelled.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String cancel_order(

	ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "orderId", required = false) String oId,
			@RequestParam(value = "username", required = false) String email) {
		Long id = Long.parseLong(oId);
		Order order = userManager.getOrderById(id);
		order.setStatus("Cancelled");
		userManager.mergeOrder(order);
		if (order.getStatus().equals("Cancelled")) {
			Map<String, Object> data = new HashMap<String, Object>();

			System.out.println("Email is " + order.getEmail());
			String cancel = "cancelled";
			data.put("name", cancel);
			String template = null;
			template = "shipping";
			String subject = "Order Cancel confirmation";
			String toName = order.getUserName();
			String toAddress = order.getEmail();
			// String toAddress = email;
			mailManager.sendEmail(subject, data, template, toName, toAddress);

			return "redirect:cancel.mm";
		}
		return null;
	}

	@RequestMapping(value = "/studentreg.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doStudentRegister(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "firstName", required = true) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "mobileNo", required = true) String mobileNo,
			@RequestParam(value = "password", required = true) String password,
			@RequestParam(value = "confirmPassword", required = true) String confirmPassword,
			@RequestParam(value = "permanentAddress", required = false) String permanentAddress,
			@RequestParam(value = "shippingAddress", required = true) String shippingAddress, HttpSession session) {
		System.out.println();
		User userFromDb = null;
		userFromDb = userManager.getEmailAddress(email);
		UUID token = UUID.randomUUID();
		if (userFromDb == null) {

			User user = new User();
			Date date = new Date();
			user.setActive(false);
			user.setToken(token);
			user.setAccountType("User");
			user.setCreateDate(date);
			user.setPassword(password);
			user.setFirstName(firstName);
			user.setLastName(lastName);
			user.setEmailAddress(email);
			user.setUserName(email);
			user.setPermanentAddress(permanentAddress);
			user.setShippingAddress(shippingAddress);
			user.setPhoneNumber(mobileNo);
			userManager.saveUser(user);

			Map<String, Object> data = new HashMap<String, Object>();

			data.put("token", token);
			data.put("firstName", user.getFirstName());
			String template = null;
			template = "userWelcome";

			String subject = "User Welcome";
			String toName = email;
			String toAddress = email;
			mailManager.sendEmail(subject, data, template, toName, toAddress);

		}

	}

	@RequestMapping(value = "/qu.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public Object doQu(@RequestParam(value = "", required = false) String reset,
			@RequestParam(value = "searchval", required = false) String searchValue, ModelMap model) {
		List<Product> products = null;
		/*
		 * if (mainid != null && subid == null) { products =
		 * userManager.getProductById(mainid); List<SubCategory> categories =
		 * userManager.getSubCategory(mainid); model.addAttribute("product",
		 * products); model.addAttribute("subcategories", categories);
		 * model.addAttribute("subcategoryHeading", "Sub Category");
		 * 
		 * } else if (mainid != null && subid != null && brandid == null) {
		 * products = userManager.getProductByMainidAndSubid(mainid, subid);
		 * List<Brand> brands = userManager.getBrandsByMainidAndSubid(mainid,
		 * subid); model.addAttribute("product", products);
		 * model.addAttribute("BrandHeading", ""); model.addAttribute("brands",
		 * brands);
		 * 
		 * } else if (mainid != null && subid != null && brandid != null) {
		 * products = userManager.getProductByMainidAndSubidAndBrandid(mainid,
		 * subid, brandid); model.addAttribute("product", products); } else {
		 */
		products = userManager.getProductBySearchResult(searchValue);

		model.addAttribute("product", products);
		model.addAttribute("categories", null);
		model.addAttribute("brands", null);
		/* } */

		return null;

	}

	@RequestMapping(value = "/student.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doStudent(@RequestParam(value = "resetPassword", required = false) String reset, ModelMap model) {
		Boolean resetPassword = false;
		if (reset != null && reset.equals("success"))
			resetPassword = true;
		model.addAttribute("resetPassword", resetPassword);

	}

	@RequestMapping(value = "/teacher.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doTeacher(@RequestParam(value = "resetPassword", required = false) String reset, ModelMap model) {
		Boolean resetPassword = false;
		if (reset != null && reset.equals("success"))
			resetPassword = true;
		model.addAttribute("resetPassword", resetPassword);

	}

	@RequestMapping(value = "quotationItem.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doQuationItem(ModelMap model) {
		// List<Product> product=userManager.getProduct(id);

	}

	/*
	 * @RequestMapping(value = "quotesubmit.mm", method = { RequestMethod.GET,
	 * RequestMethod.POST }) public void doQuotesubmit(HttpServletRequest
	 * request,
	 * 
	 * @RequestParam(value = "name", required = true) String productName,
	 * 
	 * @RequestParam(value = "availableQuantity", required = false) String
	 * availableQuantity,
	 * 
	 * @RequestParam(value = "quantity", required = true) String quantity,
	 * 
	 * @RequestParam("image") MultipartFile image, @RequestParam(value =
	 * "amount", required = true) String price,
	 * 
	 * @RequestParam(value = "totalAmount", required = true) String totalPrice,
	 * HttpSession session) throws IOException { if (!(image.isEmpty())) {
	 * userManager.addQuotation(productName, availableQuantity, quantity, image,
	 * price, totalPrice); } }
	 */

	@RequestMapping(value = "/delete.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doDelete(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "id", required = false) Long id) {
		int i = userManager.delete(id);
		if (i == 1) {
			return "redirect:checkout.mm";
		}
		return null;
	}

	@RequestMapping(value = "order.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doOrder(ModelMap model) {
		// List<Product> product=userManager.getProduct(id);
		// List<com.site.util.ShoppingCartItems> shoppingCartItems = new
		// ArrayList<com.site.util.ShoppingCartItems>();

		String uname = SecurityContextHolder.getContext().getAuthentication().getName();
		List<User> user = userManager.getUserDetails(uname);
		User userSingle = user.get(0);
		model.addAttribute("userDetails", userSingle);
		// model.addAttribute("cartproducts", shoppingCartItems);

		return null;

	}

	@RequestMapping(value = "shippingDetails.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doShippingDetails(ModelMap model) {
		// List<Product> product=userManager.getProduct(id);
		String uname = SecurityContextHolder.getContext().getAuthentication().getName();
		List<User> user = userManager.getUserDetails(uname);
		User userSingle = user.get(0);
		model.addAttribute("userDetails", userSingle);
		// model.addAttribute("cartproducts", shoppingCartItems);

		return null;
	}

	@RequestMapping(value = "order_Summary.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doOrder_Summary(ModelMap model, @RequestParam(value = "id", required = false) Long id) {

		User userFromDb = null;
		String uName = SecurityContextHolder.getContext().getAuthentication().getName();
		loginLogoutStatus(model);
		if (uName.equals("anonymousUser") || uName.equals(null)) {
			System.out.println("you are logged out");
			return "redirect:login.mm";

		} else {
			System.out.println("you are  logged in");
		}

		List<AddToCart> cartproducts = userManager.getProductfromCart(id);

		List<com.site.util.ShoppingCartItems> shoppingCartItems = new ArrayList<com.site.util.ShoppingCartItems>();
		Product product = null;
		for (AddToCart ac : cartproducts) {
			com.site.util.ShoppingCartItems sci = new com.site.util.ShoppingCartItems();
			sci.setQuantity(ac.getQuantity().toString());
			sci.setAddToCardId(ac.getId().toString());
			try {
				product = userManager.getProduct(ac.getProductId());
			} catch (Exception e) {
				logger.info("error fetching the product details " + e);
			}
			sci.setProductName(product.getProductName());
			sci.setProductPrice(product.getProductSellingPrice());
			sci.setImage(product.getImage());
			sci.setPdtId(product.getId().toString());
			shoppingCartItems.add(sci);
		}
		String uname = SecurityContextHolder.getContext().getAuthentication().getName();
		List<User> user = userManager.getUserDetails(uname);
		User userSingle = user.get(0);
		model.addAttribute("userDetails", userSingle);
		model.addAttribute("cartproducts", shoppingCartItems);

		return null;

	}

	@RequestMapping(value = "order_Payment.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doOrder_Payment(ModelMap model) {
		// List<Product> product=userManager.getProduct(id);

	}

	@RequestMapping(value = "paymentBank.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doPaymentBank(ModelMap model) {
		// List<Product> product=userManager.getProduct(id);

	}

	@RequestMapping(value = "/deleteFromCart.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public String doDeleteFromCart(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "id", required = false) Long prdId) {

		String uName = null;
		if (SecurityContextHolder.getContext().getAuthentication() != null)
			uName = SecurityContextHolder.getContext().getAuthentication().getName();
		if (uName != null && !uName.equals("anonymousUser")) {
			User user = userManager.getUser(uName);
			userManager.removeProductFromCart(prdId, user.getId());
		} else {
			for (Cookie cook : ajaxController.cookies) {
				if (cook.getValue().contains(prdId.toString())) {
					ajaxController.cookies.remove(cook);
				}
			}
		}

		return "redirect:addToCart.mm";

	}

	@RequestMapping(value = "/aboutUs.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doAboutUs(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "id", required = false) Long id) {

	}

	@RequestMapping(value = "/demo.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doDemo(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "id", required = false) Long id) {

	}

	@RequestMapping(value = "/saveQuotation.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doSaveQuotation(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "id", required = false) Long id) {

	}

	@RequestMapping(value = "/quotationSubmit.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public void doQuotationSubmit(ModelMap model, HttpServletRequest request, HttpSession session,
			@RequestParam(value = "id", required = false) Long id) {

	}

	@RequestMapping(value = "/QuotationActivation.mm", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView doQuotationActivation(ModelMap model, HttpServletRequest request,
			@RequestParam(value = "token", required = true) UUID token, HttpSession session) {
		Quotation user = null;
		user = userManager.getQuotationByToken(token);
		if (user != null) {
			user.setActive(true);

			// userManager.mergeUser(user);
			userManager.mergeQuotation(user);
		} else
			return new ModelAndView("redirect:welcome.mm");

		return new ModelAndView("redirect:QuotationList.mm");

	}

}
