<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="leftnavigation">
      <div id="categories">
      <c:forEach var="category" items="${categories}" varStatus="status">
	      <c:choose>
				<c:when test="${category.getParentId() == '0'}">
					     <div class="categorytitle">${category.getName()}</div>
				</c:when>
				<c:otherwise>
					<span class="subCategoryMenuLink" id="${category.getId()}">${category.getName()}</span>
				</c:otherwise>
		</c:choose>
      </c:forEach>
      </div>
    </div>
