<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Shipping to customer</title>
</head>
<body>
	<p>
	<h1 style="    margin-top: 200px;"><b>Shipping</b></h1>

<b>What are the delivery charges?</b></br>
Delivery charge varies with each Seller.</br>

Sellers incur relatively higher shipping costs on low value items. In such cases, charging a nominal delivery charge helps them offset logistics costs. The delivery charge may be waived off by some Sellers, if you shop with them for a minimum predefined value.</br>
For example, seller WS Retail charges Rs 40 for delivery per item if the order value is less than Rs 500. While, orders of Rs 500 or above are delivered free. Please check with individual Sellers to understand their delivery charges.</br></br>

<b>Why does the delivery date not correspond to the delivery timeline of X-Y business days?</b></br>
It is possible that the Seller or our courier partners have a holiday between the day your placed your order and the date of delivery, which is based on the timelines shown on the product page. In this case, we add a day to the estimated date. Some courier partners and Sellers do not work on Sundays and this is factored in to the delivery dates.</br></br>

<b>What is the estimated delivery time?</b></br>
Sellers generally procure and ship the items within the time specified on the product page. Business days exclude public holidays and Sundays.</br>

Estimated delivery time depends on the following factors:</br>
The Seller offering the product</br>
Product's availability with the Sell</br>er
The destination to which you want the order shipped to and location of the Seller.</br></br>

<b>Are there any hidden costs (sales tax, octroi etc) on items sold by Sellers on MIS?</b></br>
There are NO hidden charges when you make a purchase on MIS. List prices are final and all-inclusive. The price you see on the product page is exactly what you would pay.</br>

Delivery charges are not hidden charges and are charged (if at all) extra depending on the Seller's shipping policy.</br></br>
</br></br>
<b>Why does the estimated delivery time vary for each seller?</b></br>
You have probably noticed varying estimated delivery times for sellers of the product you are interested in. Delivery times are influenced by product availability, geographic location of the Seller, your shipping destination and the courier partner's time-to-deliver in your location.</br>
Please enter your default pin code on the product page (you don't have to enter it every single time) to know more accurate delivery times on the product page itself.</br></br>

<b>Seller does not/cannot ship to my area. Why?</b></br>
Please enter your pincode on the product page (you don't have to enter it every single time) to know whether the product can be delivered to your location.</br>

If you haven't provided your pincode until the checkout stage, the pincode in your shipping address will be used to check for serviceability.</br>

Whether your location can be serviced or not depends on</br>
Whether the Seller ships to your location</br>
Legal restrictions, if any, in shipping particular products to your location</br>
The availability of reliable courier partners in your location</br>

At times Sellers prefer not to ship to certain locations. This is entirely at their discretion.</br></br>

<b>Why is the CoD option not offered in my location?</b></br>
Availability of CoD depends on the ability of our courier partner servicing your location to accept cash as payment at the time of delivery.</br></br>

Our courier partners have limits on the cash amount payable on delivery depending on the destination and your order value might have exceeded this limit. Please enter your pin code on the product page to check if CoD is available in your location.</br>
</br>
<b>I need to return an item, how do I arrange for a pick-up?</b></br>
Returns are easy. Contact Us to initiate a return. You will receive a call explaining the process, once you have initiated a return.</br>
Wherever possible Ekart Logistics will facilitate the pick-up of the item. In case, the pick-up cannot be arranged through Ekart, you can return the item through a third-party courier service. Return fees are borne by the Seller.</br>
</br>
<b>What do the different tags like "In Stock", "Available" mean?</b></br>

'In Stock'</br>
FFor items listed as "In Stock", Sellers will mention the delivery time based on your location pincode (usually 2-3 business days, 4-5 business days or 4-6 business days in areas where standard courier service is available). For other areas, orders will be sent by Registered Post through the Indian Postal Service which may take 1-2 weeks depending on the location.
</br>
'Available'</br>
The Seller might not have the item in stock but can procure it when an order is placed for the item. The delivery time will depend on the estimated procurement time and the estimated shipping time to your location.
</br>
'Preorder' or 'Forthcoming'</br>
Such items are expected to be released soon and can be pre-booked for you. The item will be shipped to you on the day of it's official release launch and will reach you in 2 to 6 business days. The Preorder duration varies from item to item. Once known, release time and date is mentioned. (Eg. 5th May, August 3rd week)
 </br>
'Out of Stock'</br>
Currently, the item is not available for sale. Use the 'Notify Me' feature to know once it is available for purchase.
</br>
'Imported'</br>
Sometimes, items have to be sourced by Sellers from outside India. These items are mentioned as 'Imported' on the product page and can take at least 10 days or more to be delivered to you.
</br>
'Back In Stock Soon'</br>
The item is popular and is sold out. You can however 'book' an order for the product and it will be shipped according to the timelines mentioned by the Seller.
</br>
'Temporarily Unavailable'</br>
The product is currently out of stock and is not available for purchase. The product could to be in stock soon. Use the 'Notify Me' feature to know when it is available for purchase.
</br>
'Permanently Discontinued'</br>
This product is no longer available because it is obsolete and/or its production has been discontinued.
</br>
'Out of Print'</br>
This product is not available because it is no longer being published and has been permanently discontinued.
</br>
</br>
<b>Does MIS deliver internationally?</b></br>
As of now, MIS doesn't deliver items internationally.</br>

You will be able to make your purchases on our site from anywhere in the world with credit/debit cards issued in India and 21 other countries, but please ensure the delivery address is in India.

	
	</p>
</body>
</html>