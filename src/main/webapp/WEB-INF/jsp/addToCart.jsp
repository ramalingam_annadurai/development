<!DOCTYPE html>
<html lang="en">
<head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Cart | MIS</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/search-box.css" rel="stylesheet" />
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->
<br><br>
<body>
	<!-- <header id="header">
		header

		<div class="header-bottom">
			header-bottom
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span> <span
									class="icon-bar"></span> <span class="icon-bar"></span> <span
									class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="shop.mm">Home</a></li>
								<li class="dropdown"><a href="#">Shop<i
										class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
										<li><a href="shop.html">Products</a></li>
										<li><a href="checkout.html">Checkout</a></li>
										<li><a href="cart.html" class="active">Cart</a></li>
										<li><a href="login.html">Login</a></li>
									</ul></li>
								<li><a href="contact-us.html">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search" />
						</div>
					</div>
				</div>
			</div>
		</div> -->
		<!-- header-bottom -->
		<script type="text/javascript">
		function  updateQuantity(a){
			  
		    	var orderid=a;
		    	 var qty=document.getElementById("qtychgval"+orderid).value;
		    	AjaxController.updatecartqty(orderid,qty,responsefeed);
		    }
		   function responsefeed(a){
			   location.reload();
		   }
		</script>
		<script type="text/javascript">
		 
			function addqty(){
				
				alert("hello");
				
		</script>
	
	<!--/header-->

	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<c:set var="sum" value="${0}" />
						<c:forEach var="products" items="${cartproducts}">
							<c:set var="sumone"
								value="${products.productPrice*products.quantity}" />
							<c:set var="sum" value="${sum + sumone}" />
							<tr>
								<td class="cart_product"><a href=""><img
										src="image.mm?id=${products.pdtId}" alt=""></a></td>
								<td class="cart_description">
									<h4>
										<a href="">${products.productName}</a>
									</h4>

								</td>
								<td class="cart_price">
									<p>${products.productPrice}</p>
								</td>
								<td class="cart_quantity">
									<div class="cart_quantity_button">
										<!-- <a class="cart_quantity_up" href=""> - </a>  -->
										<c:set var="changeqtyid" value="${products.addToCardId}" />

										<input class="cart_quantity_input" type="text"
											style="width: 40px;" name="quantity"
											value="${products.quantity}" id="qtychgval${changeqtyid}"
											onchange="updateQuantity(<c:out value='${changeqtyid}'/>)"
											autocomplete="off" size="2">

										<!-- <a class="cart_quantity_down" onClick="addqty()" href="#"> + </a> -->
									</div>
								</td>
								<td class="cart_total">
									<p class="cart_total_price">${products.productPrice*products.quantity}</p>
								</td>
								 <td class="cart_delete"><a class="cart_quantity_delete"
									href="deleteFromCart.mm?id=${products.pdtId}"><i class="fa fa-times"></i></a></td> 

								  <%-- <td class="cart_delete">
									<c:set var="idofbrand" value="${AddToCart.id}" /> <a
									class="cart_quantity_delete"
									href="deleteFromCart.mm?id=${products.pdtId}"><i
										class="fa fa-times"></i></a>
								</td>   --%>

							</tr>
						</c:forEach>

					</tbody>
				</table>
			</div>
		</div>
	</section>
	<!--/#cart_items-->

	<section id="do_action">
		<div class="container">
			<!-- <div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want
					to use or would like to estimate your delivery cost.</p>
			</div> -->
			<!-- <div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li><input type="checkbox"> <label>Use
									Coupon Code</label></li>
							<li><input type="checkbox"> <label>Use Gift
									Voucher</label></li>
							<li><input type="checkbox"> <label>Estimate
									Shipping & Taxes</label></li>
						</ul>
						<ul class="user_info">
							<li class="single_field"><label>Country:</label> <select>
									<option>India</option>
							</select></li>
							<li class="single_field"><label>Region / State:</label> <select>
									<option>Select</option>
									<option>Bangalore</option>
									<option>Chennai</option>
									<option>Bangalore</option>
									<option>Mumbai</option>
							</select></li>
							<li class="single_field zip-field"><label>Zip Code:</label>
								<input type="text"></li>
						</ul>

						<a class="btn btn-default check_out" href="home.mm">Continue
							Shopping</a>
					</div>
				</div> -->
			<div class="col-sm-6 pull-right">
				<div class="total_area">
					<ul>
						<li>Cart Sub Total <span>${sum}</span></li>
						<li>Eco Tax <span>20</span></li>
						<li>Shipping Cost <span>Free</span></li>
						<li>Total <span>${sum+20}</span></li>
					</ul>

					<a class="btn btn-default check_out" href="checkout.mm">Check
						Out</a>
				</div>
			</div>
		</div>
		</div>
	</section>
	<!--/#do_action-->




	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
</body>
</html>