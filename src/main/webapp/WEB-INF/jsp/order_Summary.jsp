<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/stylesheet.css" rel="stylesheet" />
<link href="css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<link href="css/animation.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<link href="css/forcheckout.css" rel="stylesheet" />
<link href="css/check-out-responsive.css" rel="stylesheet" />
<link href="css/pavmegamenu.css" rel="stylesheet" />
<style>
.login-signup, .social-label, .new-address, .step_summary {
	color: #024205;
}
</style>
<script type="text/javascript">
	function myPayment() {
		window.location.href = 'order_Payment.mm';

	}
</script>
<script type="text/javascript">
	function myOrder() {
		window.location.href = 'order.mm';
	}
</script>
<script type="text/javascript">
	function myShipping() {
		window.location.href = 'shippingDetails.mm';
	}
</script>
<script type="text/javascript">
   function  updateQuantity(a){
	   
    	var orderid=a;
    	 var qty=document.getElementById("qtychg"+orderid).value;
    	AjaxController.updatecartqty(orderid,qty,responsefeed);
    }
   function responsefeed(a){
	   location.reload();
   }
    </script>
</head>
<body>
	<div class="forcheckout">
		<div class="steps">




			<ul class="step-tabs">
				<li class="login-unsel login-hover" onclick="myOrder();">&nbsp;</li>
				<li class="arrow-next">&nbsp;</li>
				<li class="address-unsel address-hover" onclick="myShipping();">&nbsp;</li>
				<li class="arrow-prev">&nbsp;</li>
				<li class="summary-sel" onclick="myOrder_Sum()">&nbsp;</li>
				<li class="arrow-current">&nbsp;</li>
				<li class="payment-unsel">&nbsp;</li>
			</ul>
			<div class="clear"></div>
			<div class="step_summary">
				<div class="heading">Review your Order</div>
				<table width="700" cellspacing="0" cellpadding="0" align="left">
					<tbody>
						<tr>
							<th class="hcol_0">&nbsp;</th>
							<th class="htype col_1">Item</th>
							<th align="left" class="hitem">Item Description</th>
							<th align="left" class="hdelivery">Model</th>
							<th align="center" class="hprice">Price</th>
							<th align="center" class="hqty">Qty.</th>
							<th align="center" class="hsubtotal">Subtotal</th>
							<th align="left" class="hremove">&nbsp;</th>
						</tr>
						<c:set var="sum" value="${0}" />
						<c:forEach var="products" items="${cartproducts}">
						<c:set var="sumone"
										value="${products.productPrice*products.quantity}" />
									<c:set var="sum" value="${sum + sumone}" />
							<tr class="dummy"></tr>
							<tr class="cart-item">
								<td class="col_0"></td>
								<td class="col_1"><a href=""><img
										src="image.mm?id=${products.pdtId}" alt=""></a></td>
								<td><span><a href="">${products.productName}</a></span></td>
								<td>9788121927499</td>
								<td class="price">${products.productPrice}</td>
								<c:set var="changeqtyid" value="${products.addToCardId}" />
								<td align="center"><input class="cart_quantity_input"
									type="text" style="width: 40px;" name="quantity"
									value="${products.quantity}" id="qtychg${changeqtyid}"
									onchange="updateQuantity(<c:out value='${changeqtyid}'/>)"
									autocomplete="off" size="2"></td>
								<td class="subtotal">${products.productPrice*products.quantity}</td>
								<td class="remove"><span><a
										href="javascript:void(0);"
										onclick="remove_product('http://www.portofaspirations.com/index.php?route=checkout/confirm&amp;remove=821')">&nbsp;</a></span></td>
							</tr>
						</c:forEach>
						<tr class="dummy"></tr>
						<tr>
							<td colspan="10">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="8">
								<div class="content">
									<p>Choose if you have a coupon code or gift voucher or
										reward points .</p>
									<table class="radio">
										<tbody>
											<tr class="highlight">
												<td><input type="radio" name="next" value="coupon"
													id="use_coupon"></td>
												<td><label for="use_coupon">Use Coupon Code</label></td>
											</tr>
											<tr class="highlight">
												<td><input type="radio" name="next" value="voucher"
													id="use_voucher"></td>
												<td><label for="use_voucher">Use Gift Voucher</label></td>
											</tr>

											<!-- Gift Wrapping -->
										</tbody>
									</table>
								</div>
								<div class="cart-module">
									<div id="coupon" class="content" style="display: none;">
										<form
											action="http://www.portofaspirations.com/index.php?route=checkout/cart"
											method="post" enctype="multipart/form-data">
											Enter your coupon here:&nbsp; <input type="text"
												name="coupon" value=""> <input type="hidden"
												name="next" value="coupon"> &nbsp; <input
												id="button-checkout-coupon" type="button"
												value="Apply Coupon" class="button">
										</form>
									</div>
									<div id="voucher" class="content" style="display: none;">
										<form
											action="http://www.portofaspirations.com/index.php?route=checkout/cart"
											method="post" enctype="multipart/form-data">
											Enter your gift voucher code here:&nbsp; <input type="text"
												name="voucher" value=""> <input type="hidden"
												name="next" value="voucher"> &nbsp; <input
												id="button-checkout-voucher" type="button"
												value="Apply Voucher" class="button">
										</form>
									</div>
									<div id="reward" class="content" style="display: none;">
										<form
											action="http://www.portofaspirations.com/index.php?route=checkout/cart"
											method="post" enctype="multipart/form-data">
											Points to use (Max 0):&nbsp; <input type="text" name="reward"
												value=""> <input type="hidden" name="next"
												value="reward"> &nbsp; <input
												id="button-checkout-reward" type="button"
												value="Apply Points" class="button">
										</form>
									</div>

									<div id="giftWrapping" class="content" style="display: none;">
										<form
											action="http://www.portofaspirations.com/index.php?route=checkout/cart"
											method="post" enctype="multipart/form-data">
											<input type="button" value="Yes" name="giftWrapping"
												class="button" id="button-checkout-addGiftoption"> <input
												type="button" value="No" name="removeGiftWrapping"
												class="button" id="button-checkout-removeGiftoption">
										</form>
										<div style="text-align: right; font-weight: bold;">Gift
											Wrap For Rs.0</div>
									</div>

								</div>

							</td>
						</tr>
						<tr class="dummy"></tr>
						<tr>
							<td class="sep-bottom" colspan="10">&nbsp;</td>
						</tr>
						<tr class="totals">
							<td class="head" colspan="6">Sub-Total</td>
							<td colspan="2">${sum}</td>
						</tr>
						<tr class="totals">
							<td class="head" colspan="6">Total</td>
							<td colspan="2">${sum}</td>
						</tr>
						<tr class="payable">
							<td class="head" colspan="5">Amount Payable</td>
							<td colspan="3">${sum}</td>
						</tr>
						<tr class="dummy"></tr>
						<tr>
							<td class="sep-bottom" colspan="10">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<div class="summary-bottom">
					<form method="post" action="">
						<table width="95%" cellspacing="0" cellpadding="0">
							<tbody>
								<tr align="right">
									<td align="left"><label class="confirmation_label">Order
											Confirmation SMS alert to +91 <input type="text"
											maxlength="10" name="send_sms_mobile" size="10"
											id="send_sms_mobile" value="7795570698">
									</label> <br> <input type="checkbox" name="agree" id="text_agree"
										value="1"> <label for="text_agree">I have read
											and agree to the <a class="fancybox" id="agree-condition"
											href="http://www.portofaspirations.com/index.php?route=information/information/info&amp;information_id=6"
											alt="Shipping and Returns"><b>Shipping and Returns</b></a>
									</label></td>

									<td align="right"><input id="button-payment-method"
										class="btn orange continue" type="button" value="Continue"
										onclick="myPayment()"></td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</div>


		</div>

		<div class="summary-right">
			<!--<div class="summary-order"></div>
			<div class="summary-address"></div>-->
			<div class="summary-cod"></div>
		</div>
	</div>
</body>
</html>