<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title>Login | MIS</title>
<<!-- script type="text/javascript" src="js/jquery.js"></script> -->
<!-- <script src="js/jquery-1.9.1.min.js"></script> -->
<script src="js/jquery.validate.js"></script>
<!-- <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script> -->
<!-- <script type="text/javascript" src="js/loginvalidation.js"></script> --> 
<script type="text/javascript" src="js/signupvalidation.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">

<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<!-- <link href="css/login.css" rel="stylesheet"> -->
<link href="css/search-box.css" rel="stylesheet" />
<link href="css/login_form.css" rel="stylesheet" />
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">

<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript">
      (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();
</script>
<script type="text/javascript">
 function signup(){
	 AjaxController.getEmail(email);
 }
 function email(email){
	 document.getElementById("here").innerHTML="Email is already exist";
 }

</script>
<!-- <script type="text/javascript">
	function checkSubmit() {
		if (document.getElementById('policy').checked == false) {
			document.getElementById("add").innerHTML = "In order to use our services, you must agree to companies's Terms of Service.";
			return false;
		} else {
			return true;
		}
	}
	
	
	function onLoadCallback()
	{
	    gapi.client.setApiKey('YOUR_API_KEY'); //set your API KEY
	    gapi.client.load('plus', 'v1',function(){});//Load Google + API
	}
</script> -->
<script>
	function validateform() {
		var name = document.loginForm.j_username.value;
		var password = document.loginForm.j_password.value;

		if (name == null || name == "") {
			document.getElementById("name").innerHTML = "Email is required !";
			return false;
		} else if (password.length < 6) {
			document.getElementById("name").innerHTML = "Password is required !"
			return false;
		}
	}
</script>
<!-- <script type="text/javascript">
	function checkCredentials(){
		alert("hi");
		
	}

</script> -->
<style type="text/css">
#name {
	color: red;
}
</style>
</head>
<!--/head-->

<body style="margin-top: -190px;" >
	<header id="header">
		<!--header-->
		<div class="header-bottom">
			<!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span> <span
									class="icon-bar"></span> <span class="icon-bar"></span> <span
									class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.html">Home</a></li>
								<li class="dropdown"><a href="#">Shop<i
										class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
										<li><a href="#">Checkout</a></li>
										<li><a href="#">Cart</a></li>
									</ul></li>
								<li><a href="contact-us.html">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header-bottom-->
	</header>
	<!--/header-->
	<section id="form" style="margin-bottom: -65px;">
		<!--form-->
		<!-- <div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form">
						login form
						<h2>Login to your account</h2>
						<span id="name" id="error"></span>
						<form action="userLogin.mm" method="post" name="loginForm"
							id="loginForm" onsubmit="return validateform()">

							<input name="j_username" id="j_username" tabindex="1" type="text"
								placeholder="User Email" autofocus="autofocus"/> <input name="j_password"
								id="j_password" tabindex="2" type="password"
								placeholder="Password" /> <span> <input type="checkbox"
								class="checkbox"> Keep me signed in
							</span> <br /> <span> <a href="forgotPassword.mm"> Forgot
									password? </a>
							</span> <input type="submit" value="Login" class="btn btn-default" id="login" onclick="submitForm();">
						</form>
					</div>
					/login form
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form">
						sign up form
						<h2>New User Signup!</h2>
						<span> <a href="signup.mm"> signup here </a>
						</span>
					</div>
					/sign up form
				</div>
			</div>
		</div> -->
		<div class="login-box">
			<div class="lb-header"><label style="color: red;">${errormsg}</label>
				<a href="#" class="active" id="login-box-link">Login</a> <a href="#"
					id="signup-box-link">Sign Up</a>
			</div>
			<div class="social-login">
				<!-- <a href="#"> <i class="fa fa-facebook fa-lg"></i> Login with
					facebook
				</a> -->  <!-- <a href="#"> <i class="fa fa-google-plus fa-lg"></i> Login with
					Google
				</a> -->
			</div>
			<form class="email-login" action="userLogin.mm" method="post"
				name="loginForm" id="loginForm" onsubmit="return validateform()">
				<span id="name" ></span>
				<div class="u-form-group">
					<input type="email" placeholder="Email" name="j_username"
						id="j_username" tabindex="1" autofocus="autofocus" />
				</div>
				<div class="u-form-group">
					<input type="password" placeholder="Password" name="j_password"
						id="j_password" tabindex="2" />
				</div>
				<div class="u-form-group">
					<!-- <button>Log in</button> -->
					<input type="submit" value="Login" class="btn btn-default"
						id="login">
				</div>
				<div class="u-form-group">
					<a href="forgotPassword.mm" class="forgot-password">Forgot
						password?</a>
				</div>
			</form>
			<form class="email-signup" action="register.mm" method="post"
				id="SignupForm" name="SignupForm" >
				<span id="fname" style="color: red"></span>
				<div class="u-form-group">
					<input name="firstName" id="firstName" tabindex="1" type="text"
						placeholder="First Name" autofocus="autofocus"
						placeholder="First Name" style="height: 42px;width: 230px;"/> <span id="nameloc" style="color: red"></span>
				</div>
				<span id="lname" style="color: red"></span>
				<div class="u-form-group">
					<input type="text" name="lastName" id="lastName" tabindex="2"
						placeholder="Last Name" style="height: 42px;width: 230px;"/> <span id="lastloc"></span>
				</div>
				<span id="email" style="color: red"></span>
				<div class="u-form-group">
					<input type="email" name="email" tabindex="3" placeholder="Email" onkeyup="signup()" />
					<span id="here"></span>

				</div>
				<span id="number" style="color: red"></span>
				<div class="u-form-group">
					<input type="text" name="mobileNo" id="mobileNo" tabindex="4"
						placeholder="Phone Number" style="height: 42px;width: 230px;"/> <span id="numloc"
						style="color: red"></span>
				</div>
				<span id="perm" style="color: red"></span>
				<div class="u-form-group">
					<textarea rows="2" cols="10" tabindex="6" name="permanentAddress"
						id="address" placeholder="permanent Address"
						name="permanentAddress" style="width: 240px;"></textarea>
				</div>
				<span id="address" style="color: red"></span>
				<diV class="u-form-group">
					<textarea rows="2" cols="10" tabindex="7" name="shippingAddress"
						id="shippingAddress" placeholder="Shipping Address" style="width: 240px;"></textarea>
				</diV>
				<span id="pass" style="color: red"></span>
				<div class="u-form-group">
					<input type="password" name="password" id="password" tabindex="8"
						placeholder="Password" />
				</div>
				<div class="u-form-group">
					<input type="password" name="confirmPassword" id="confirmPassword"
						tabindex="9" placeholder="Confirm Password" />
				</div>
				<!-- <div class="u-form-group">
				<input type="checkbox" name="policy" value="yes" id="policy">I agree to the company Terms of Service and Privacy Policy<br> 
								 <span id="add"></span>
				</div> -->
				<div class="u-form-group">
					<!-- <button>Sign Up</button> -->
					<input type="submit" value="Submit" class="btn btn-default"
						id="login">
				</div>
			</form>
		</div>
	</section>
	<!-- <script src="js/jquery.js"></script> -->
	<script src="js/price-range.js"></script>
	<!-- <script src="js/jquery.scrollUp.min.js"></script> -->
	<script src="js/bootstrap.min.js"></script>
	<!-- <script src="js/jquery.prettyPhoto.js"></script> -->
	<script src="js/main.js"></script>
	
	<script>
		$(".email-signup").hide();
		$("#signup-box-link").click(function() {
			$(".email-login").fadeOut(100);
			$(".email-signup").delay(100).fadeIn(100);
			$("#login-box-link").removeClass("active");
			$("#signup-box-link").addClass("active");
		});
		$("#login-box-link").click(function() {
			$(".email-login").delay(100).fadeIn(100);
			;
			$(".email-signup").fadeOut(100);
			$("#login-box-link").addClass("active");
			$("#signup-box-link").removeClass("active");
		});
		$("#signup-box-link").click(function() {
			$(".login-box").css('height', '690px', 'important');
		});
		$("#login-box-link").click(function() {
			$(".login-box").css('height', '360px', 'important');
		});
	</script>
	<!--  <script src="js/jquery.js"></script>  -->
	<script src="js/price-range.js"></script>
	<!--  <script src="js/jquery.scrollUp.min.js"></script> -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
</body>
<br><br><br>
</html>