<!DOCTYPE  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Shop | MIS</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">

<!-- <script src="js/jquery-1.7.1.min.js"></script> -->
<script src="js/jquery-ui.js"></script>
<script src="js/jquery-ui-1.8.15.custom.min.js"></script>
<script src="js/jquery-1.8.3.js"></script>
<script src="js/jquery.ui.core.min.js"></script>
<script src="js/jquery.ui.widget.min.js"></script>
<script src="js/jquery.ui.position.min.js"></script>
<script src="js/jquery.ui.autocomplete.min.js"></script>
<script
	src="http://bainternet-js-cdn.googlecode.com/svn/trunk/js/jQuery%20BlockUI%20Plugin/2.39/jquery.blockUI.js"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript">
	function getCheckedCheckboxesForSub(subCategoryName) {
		var checkboxes = document.querySelectorAll('input[name="'
				+ subCategoryName + '"]:checked'), values = [];
		Array.prototype.forEach.call(checkboxes, function(el) {
			values.push(el.value);
		});

		AjaxController.getproductListBySubCategory(values, productList);
	}

	function productList(productList) {
		var result = "";
		//alert("NO");
		//alert(productList);
		//alert(productList.length);
		if (productList.length < 1)
			document.getElementById("productList").innerHTML = "No Product Available !";
		else {
			for ( var i = 0; i < productList.length; i++) {
				var prodName = productList[i].productName;

				var prodId = productList[i].id;
				result += "<form action='productDetails.mm' method='post'><input type='hidden' name='pdt-details' value='"+prodId+"'>";
				result += "<div class='col-sm-4'><div class='product-image-wrapper'><div class='single-products'><div class='productinfo text-center'>";
				result += "<img style='width:100%;height:250px;' src='image.mm?id="
						+ prodId + "' /><p >" + prodName + "</p>";
				result += "<input type='submit' class='btn btn-default add-to-cart' value='Deatils'></input></div></div>";
				result += "</div></div></form>";
			}
		}
	}

	function getCheckedCheckboxesForBrand(brandName) {
		var checkboxes = document.querySelectorAll('input[name="' + brandName
				+ '"]:checked'), values = [];
		Array.prototype.forEach.call(checkboxes, function(el) {
			values.push(el.value);
		});

		AjaxController.getproductListByBrand(values, productList);
	}
</script>
</head>
<!--/head-->

<body>

	<header id="header">
		<!--header-->



		<div class="header-bottom">
			<!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span> <span
									class="icon-bar"></span> <span class="icon-bar"></span> <span
									class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.html">Home</a></li>
								<li class="dropdown"><a href="#" class="active">Shop<i
										class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
										<li><a href="shop.html" class="active">Products</a></li>
										<li><a href="product-details.html">Product Details</a></li>
										<li><a href="checkout.html">Checkout</a></li>
										<li><a href="addToCart.mm">Cart</a></li>
										<li><a href="login.mm">Login</a></li>
									</ul></li>

								<li><a href="contact-us.html">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>


	<section style="margin-top: 110px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">

						<h2>${subcategoryHeading}</h2>
						<div class="panel-group category-products" id="accordian">


							<c:forEach var="category" items="${subcategories}">

								<div class="panel panel-default">
									<div class="panel-heading">

										<h4 class="panel-title" id="categorysearch">
											<input type="checkbox" name="subcategory"
												onchange="getCheckedCheckboxesForSub('subcategory')"
												id="${category.subCategoryName}" value="${category.id}"><a
												href="#"> <label style="text-transform: capitalize;"
												for="${category.subCategoryName}">${category.subCategoryName}</label>
											</a>
										</h4>
									</div>
								</div>

							</c:forEach>

						</div>
						<!--/category-productsr-->


						<div class="panel-group category-products">
							<!--brands_products-->
							<h2>${BrandHeading}</h2>
							<div class="panel-group category-products" id="accordian">


								<c:forEach var="brand" items="${brands}">

									<div class="panel panel-default">
										<div class="panel-heading">

											<h4 class="panel-title" id="brandsearch">
												<input type="checkbox" name="brandname"
													onchange="getCheckedCheckboxesForBrand('brandname')"
													id="${brand.brandName}" value="${brand.id}"><a
													href="#"> <label for="${brand.brandName}">${brand.brandName}</label>
												</a>
											</h4>
										</div>
									</div>

								</c:forEach>

							</div>

						</div>
						<!--/brands_products-->


						<!-- <div class="price-range"> -->



						<div class="shipping text-center">
							<!--shipping-->
							<img src="images/home/shipping.jpg" alt="" />
						</div>
						<!--/shipping-->

					</div>
				</div>

				<div class="col-sm-9 padding-right">

					<div class="features_items">
						<!--features_items-->
						<h2 class="title text-center">Featured Items</h2>
						<span id="productList"> <c:choose>
								<c:when test="${empty product}">
									<h4 class="title text-center">No Products found</h4>
								</c:when>
								<c:otherwise>
									<c:forEach var="product" items="${product}">

										<form action="productDetails.mm" method="get">

											<input type="hidden" name="pdt-details" value="${product.id}" />

											<div class="col-sm-4">
												<div class="product-image-wrapper">
													<div class="single-products">
														<div class="productinfo text-center">
															<c:set var="productidval" value="${product.id}" />
															<a href="productDetails.mm?pdt-details=${product.id}">
																<img style="width: 100%; height: 250px;"
																src="image.mm?id=${product.id}" />
															</a>

															<p>${product.productName}</p>

															<input type="submit" class="btn btn-default add-to-cart"
																value="Deatils"></input>
														</div>

													</div>

												</div>
											</div>


										</form>

									</c:forEach>

								</c:otherwise>
							</c:choose>
						</span>










					</div>
					<!--features_items-->
				</div>
			</div>
		</div>
	</section>




	<script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
</body>
</html>