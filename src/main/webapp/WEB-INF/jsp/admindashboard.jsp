
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Login | MIS</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="/css/styles.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
	<script type="text/javascript">
	function mymenus(menus){
		
		AjaxController.getOrderProduct(menus,productList);
	}
		function productList(productList) {
			var result="hello";
		/*	for ( var i = 0; i < productList.length; i++)
			{
				var proname=productList[i].productName;
				result+="hello"; 
			}
			
		*/
		document.getElementById("productList").innerHTML = result;
	}

	</script>
</head>
<!--/head-->

<body>
	<header id="header">
		<!--header-->
		

		<div class="header-middle">
			<!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<!-- <div class="logo pull-left">
							<a href="index.html"><img src="images/home/logo.png" alt="" /></a>
						</div> -->
						<div class="btn-group pull-right"></div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><a href=""><i class="fa fa-user"></i> Logout</a></li>
							

							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header-middle-->

		<div class="header-bottom">
			<!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span> <span
									class="icon-bar"></span> <span class="icon-bar"></span> <span
									class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="#">Home</a></li>
								<li><a href="contactus.mm">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search" />
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/header-bottom-->
	</header>
	<!--/header-->

	<div class="container">

		<!-- upper section -->
		<div class="row">
			<div class="col-sm-3">
				<!-- left -->
				<!--  <h3><i class="glyphicon glyphicon-briefcase"></i> Toolbox</h3> -->
				<hr>

				<ul class="nav nav-stacked">
					<li><a href="#" onClick="mymenus('Order')"><i
							class="glyphicon glyphicon-flash"></i> Order</a></li>
					<li><a href="#"><i
							class="glyphicon glyphicon-link"></i> Add Product</a></li>
					<li><a href="#" ><i
							class="glyphicon glyphicon-list-alt"></i> Shipping Details</a></li>

				</ul>

				<hr>

			</div>
			<!-- /span-3 -->
			<div class="col-sm-9" >

				<!-- column 2 -->
				<h3>
					<i class="glyphicon glyphicon-dashboard"></i> Dashboard
				</h3>

				<hr>

				<div class="row">
					<!-- center left-->
					<div class="col-md-7">
						<div class="well">
							Inbox Messages <span class="badge pull-right">3</span>
						</div>

						<hr>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h4>Processing Status</h4>
							</div>
							<div class="panel-body">

								<small>Complete</small>
								<div class="progress">
									<div class="progress-bar progress-bar-success"
										role="progressbar" aria-valuenow="72" aria-valuemin="0"
										aria-valuemax="100" style="width: 72%">
										<span class="sr-only">72% Complete</span>
									</div>
								</div>
								<small>In Progress</small>
								<div class="progress">
									<div class="progress-bar progress-bar-info" role="progressbar"
										aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
										style="width: 20%">
										<span class="sr-only">20% Complete</span>
									</div>
								</div>
								<small>At Risk</small>
								<div class="progress">
									<div class="progress-bar progress-bar-danger"
										role="progressbar" aria-valuenow="80" aria-valuemin="0"
										aria-valuemax="100" style="width: 80%">
										<span class="sr-only">80% Complete</span>
									</div>
								</div>

							</div>
							<!--/panel-body-->
						</div>
						<!--/panel-->

					</div>
					<!--/col-->

					<!--center-right-->
					<div class="col-md-5" id="productList">

						<ul class="nav nav-justified">
							<li><a href="#"><i class="glyphicon glyphicon-cog"></i></a></li>
							<li><a href="#"><i class="glyphicon glyphicon-heart"></i></a></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"><i
									class="glyphicon glyphicon-comment"></i><span class="count">3</span></a>
							<ul class="dropdown-menu" role="menu">
									<li><a href="#">1. Is there a way..</a></li>
									<li><a href="#">2. Hello, admin. I would..</a></li>
									<li><a href="#"><strong>All messages</strong></a></li>
								</ul></li>
							<li><a href="#"><i class="glyphicon glyphicon-user"></i></a></li>
							<li><a title="Add Widget" data-toggle="modal"
								href="#addWidgetModal"><span
									class="glyphicon glyphicon-plus-sign"></span></a></li>
						</ul>

						<hr>

						<p>
							This is a responsive dashboard-style layout that uses <a
								href="http://www.getbootstrap.com">Bootstrap 3</a>. You can use
							this template as a starting point to create something more
							unique.
						</p>
						<p>
							Visit the Bootstrap Playground at <a
								href="http://www.bootply.com">Bootply</a> to tweak this layout,
							or discover 1000's of Bootstrap code examples and snippets.
						</p>

						<hr>

						<div class="btn-group btn-group-justified">
							<a href="#" class="btn btn-info col-sm-3"> <i
								class="glyphicon glyphicon-plus"></i><br> Service
							</a> <a href="#" class="btn btn-info col-sm-3"> <i
								class="glyphicon glyphicon-cloud"></i><br> Cloud
							</a> <a href="#" class="btn btn-info col-sm-3"> <i
								class="glyphicon glyphicon-cog"></i><br> Tools
							</a> <a href="#" class="btn btn-info col-sm-3"> <i
								class="glyphicon glyphicon-question-sign"></i><br> Help
							</a>
						</div>

					</div>
					<!--/col-span-6-->

				</div>
				<!--/row-->
			</div>
			<!--/col-span-9-->

		</div>
		<!--/row-->

</div>
		<footer id="footer">
			<!--Footer-->
			<div class="footer-top">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<div class="companyinfo">
								<!-- <h2><span>e</span>-shopper</h2> -->

							</div>
						</div>
						<div class="col-sm-7">
							<div class="col-sm-3">
								<div class="video-gallery text-center">
									<a href="#">
										<div class="iframe-img">
											<img src="images/home/iframe1.png" alt="" />
										</div>
										<div class="overlay-icon">
											<i class="fa fa-play-circle-o"></i>
										</div>
									</a>
									<p>Circle of Hands</p>
									<h2>24 DEC 2014</h2>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="video-gallery text-center">
									<a href="#">
										<div class="iframe-img">
											<img src="images/home/iframe2.png" alt="" />
										</div>
										<div class="overlay-icon">
											<i class="fa fa-play-circle-o"></i>
										</div>
									</a>
									<p>Circle of Hands</p>
									<h2>24 DEC 2014</h2>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="video-gallery text-center">
									<a href="#">
										<div class="iframe-img">
											<img src="images/home/iframe3.png" alt="" />
										</div>
										<div class="overlay-icon">
											<i class="fa fa-play-circle-o"></i>
										</div>
									</a>
									<p>Circle of Hands</p>
									<h2>24 DEC 2014</h2>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="video-gallery text-center">
									<a href="#">
										<div class="iframe-img">
											<img src="images/home/iframe4.png" alt="" />
										</div>
										<div class="overlay-icon">
											<i class="fa fa-play-circle-o"></i>
										</div>
									</a>
									<p>Circle of Hands</p>
									<h2>24 DEC 2014</h2>
								</div>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="address">
								<img src="images/home/map.png" alt="" />
								<!-- <p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p> -->
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="footer-widget">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Service</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Online Help</a></li>
									<li><a href="">Contact Us</a></li>
									<li><a href="">Order Status</a></li>
									<li><a href="">Change Location</a></li>
									<li><a href="">FAQ’s</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Quock Shop</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">T-Shirt</a></li>
									<li><a href="">Mens</a></li>
									<li><a href="">Womens</a></li>
									<li><a href="">Gift Cards</a></li>
									<li><a href="">Shoes</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Policies</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Terms of Use</a></li>
									<li><a href="">Privecy Policy</a></li>
									<li><a href="">Refund Policy</a></li>
									<li><a href="">Billing System</a></li>
									<li><a href="">Ticket System</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>About MIS</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Company Information</a></li>
									<li><a href="">Careers</a></li>
									<li><a href="">Store Location</a></li>
									<li><a href="">Affillate Program</a></li>
									<li><a href="">Copyright</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3 col-sm-offset-1">
							<div class="single-widget">
								<h2>About MIS</h2>
								<form action="#" class="searchform">
									<input type="text" placeholder="Your email address" />
									<button type="submit" class="btn btn-default">
										<i class="fa fa-arrow-circle-o-right"></i>
									</button>
									<p>
										Get the most recent updates from <br />our site and be
										updated your self...
									</p>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<p class="pull-left">Copyright © 2013 E-SHOPPER Inc. All
							rights reserved.</p>
						<p class="pull-right">
							Designed by <span><a target="_blank" href="#"></a></span>
						</p>
					</div>
				</div>
			</div>

		</footer>
		<!--/Footer-->



		<script src="js/jquery.js"></script>
		<script src="js/price-range.js"></script>
		<script src="js/jquery.scrollUp.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.prettyPhoto.js"></script>
		<script src="js/main.js"></script>
		<script
			src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
</body>
</html>