<br><br><br><br><br><br><br><br><br><br><br>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/stylesheet.css" rel="stylesheet" />
<link href="css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<!-- <link href="css/animation.css" rel="stylesheet" /> -->
<link href="css/font-awesome.min.css" rel="stylesheet" />
<!-- <link href="css/forcheckout.css" rel="stylesheet" /> -->
<!-- <link href="css/check-out-responsive.css" rel="stylesheet" /> -->
<link href="css/pavmegamenu.css" rel="stylesheet" />
<style>
#footer{
background:#fff;
}
#footer .box .box-heading {
color: #f7c913 !important;
    font-size: 20px;
    font-family: lato;
}
#footer ul.list li a:hover {
    color: #fdc526;
}
#footer ul.list li a {
    color: #024205;
}
.social{
border-bottom: 1px solid #fff !important;
}
.footer-center .col-lg-3 {
border-left: 1px solid #fff !important;
}
</style>
</head>

<body>
<div class="col-md-3"></div>
<div class="col-md-6">
<div class="box">
						<div class="box-heading"><span>ABOUT US</span></div>
			<div class="box-content">
<p>Ours is a first of its kind store in entire Navi Mumbai catering the essentials 'under one roof' from a very spacious and air-conditioned store with excellent service.</p>

<ul class="list">
	<li><span class="fabox">&nbsp;</i></span> <span>Shop No:6-7, Neel Plaza,Sector 3,Near D A V School,New Panvel </span></li>
	<li><span class="fabox">&nbsp;</i></span> <span>+91-750 651 30 37 / +91-222- 748 00 45</span></li>
	<li><span class="fabox">&nbsp;</i></span> <span>maestroinfotech@gmail.com</span></li>
</ul>
</div>
		</div>

</div>
<div class="col-md-3"></div>
	
</body>
</html>