<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/stylesheet.css" rel="stylesheet" />
<link href="css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<link href="css/animation.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<link href="css/forcheckout.css" rel="stylesheet" />
<link href="css/check-out-responsive.css" rel="stylesheet" />
<link href="css/pavmegamenu.css" rel="stylesheet" />
<style>
.login-signup, .social-label, .new-address, .step_summary, .step_payment
	{
	color: #024205;
}
</style>
<script type="text/javascript">
	function myOrder_Sum() {
		window.location.href = 'order_Summary.mm';
	}
</script>
<script type="text/javascript">
	function myOrder() {
		window.location.href = 'order.mm';
	}
</script>
<script type="text/javascript">
	function myShipping() {
		window.location.href = 'shippingDetails.mm';
	}
</script>
<script type="text/javascript">
	function myBank() {
		window.location.href = 'paymentBank.mm';
	}
</script>
</head>
<body>

	<div class="forcheckout">
		<div class="steps">




			<ul class="step-tabs">
				<li class="login-unsel login-hover" onclick="myOrder();">&nbsp;</li>
				<li class="arrow-next">&nbsp;</li>
				<li class="address-unsel address-hover" onclick="myShipping();">&nbsp;</li>
				<li class="arrow-next">&nbsp;</li>
				<li class="summary-unsel summary-hover" onclick="myOrder_Sum();">&nbsp;</li>
				<li class="arrow-prev">&nbsp;</li>
				<li class="payment-sel">&nbsp;</li>
			</ul>
			<div class="clear"></div>
			<div class="step_payment">
				<div class="heading">Choose your mode of payment</div>

				<div class="payment-bg">
					<div id="payment-tabs" class="payment-all">
						<ul>
							<li class="selected">Shipping Options</li>

							<li onclick="myBank()">Bank Transfer</li>

							<li><form action="paymentconfirm.mm" method="get">
									<input type="hidden" name="paid" value="cod" /><input
										type="submit" value="Cash on Delivery"
										style="background: none; border: 0; color: #ff0000">
								</form></li>
						</ul>
						<div id="shipping-method" style="display: block;">
							<p>Please select the preferred shipping method to use on this
								order.</p>
							<table class="radio">
								<tbody>
									<tr>
										<td colspan="3"><b>Weight Based Shipping</b></td>
									</tr>
									<tr class="highlight">
										<td><input type="radio" name="shipping_method"
											value="weight.weight_5" id="weight.weight_5"
											checked="checked"></td>
										<td><label for="weight.weight_5">India Standard
												(Weight: 0.64kg)</label></td>
										<td style="text-align: right;"><label
											for="weight.weight_5">Rs.125</label></td>
									</tr>
								</tbody>
							</table>
							<br> <b>Add Comments About Your Order</b>
							<textarea name="comment" rows="8" style="width: 98%;"></textarea>
							<br> <br>
							<div class="buttons">
								<div class="right">
									<input type="button" value="Continue"
										id="button-shipping-method" class="btn orange continue">



								</div>
							</div>
						</div>

						<div id="bank_transfer" style="display: none;">
							<div
								style="text-align: center; width: '100%'; height: '400px'; padding-top: 50px;">
								<img
									src="catalog/view/theme/default/image/forcheckout/loading-checkout.gif">
							</div>
						</div>

						<div id="citruspay" style="display: none;">
							<div
								style="text-align: center; width: '100%'; height: '400px'; padding-top: 50px;">
								<img
									src="catalog/view/theme/default/image/forcheckout/loading-checkout.gif">
							</div>
						</div>
					</div>
					<div style="display: none" id="payment-confirm"></div>
				</div>
			</div>
			<div id="loading-payments"
				style="border: 1px solid black; display: none">
				<div
					style="text-align: center; width: '100%'; height: '400px'; padding-top: 50px;">
					<img
						src="catalog/view/theme/default/image/forcheckout/loading-checkout.gif">
				</div>
			</div>

		</div>

		<div class="summary-right">
			<!--<div class="summary-order"></div>
			<div class="summary-address"></div>-->
			<div class="summary-cod"></div>
		</div>
	</div>
</body>
</html>