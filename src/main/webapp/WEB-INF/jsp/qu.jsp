<!DOCTYPE>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Shop | MIS</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/search-box.css" rel="stylesheet" />
<link href="css/login_form.css" rel="stylesheet" />
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">

<!-- <script src="js/jquery-1.7.1.min.js"></script> -->
<!-- <script src="http://code.jquery.com/jquery-1.4.2.min.js" type="text/javascript"></script> -->
<!-- <script src="js/jquery.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery-ui-1.8.15.custom.min.js"></script>
<script src="js/jquery-1.8.3.js"></script>
<script src="js/jquery.ui.core.min.js"></script>
<script src="js/jquery.ui.widget.min.js"></script>
<script src="js/jquery.ui.position.min.js"></script>
<script src="js/jquery.ui.autocomplete.min.js"></script> -->
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"
	type="text/Javascript"></script>
<!-- <script type="text/javascript">
	function checkFunc(ctrl,txtId){
		alert("inside checkbox");
		alert(ctrl.value);
		alert(txtId.value);
		if (ctrl.checked)
		    $('#' + txtId).removeAttr('disabled');
		else
		    $('#' + txtId).attr('disabled', 'disabled');
		
	}
	</script> -->

<!-- <script type="text/javascript">
		
			$(function(){
				$("#checkbox").change(function(){
					var st=this.checked;
					if(st){
						$("#qty").prop("disabled",false);
					}else{
						$("#qty").prop("disabled",true);
					}
				})
			})
		
	
	</script> -->
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<!-- <script type="text/javascript" src="js/checkbox.js"></script> -->
<!-- <script
	src="http://bainternet-js-cdn.googlecode.com/svn/trunk/js/jQuery%20BlockUI%20Plugin/2.39/jquery.blockUI.js"></script>
<script src="js/jquery-ui.js"></script> -->
<script type="text/javascript">
   function  updateQuantity(a, b){
	   alert(b);
	   var amt=document.getElementById("amt"+b).value;
	   var price=a.value*amt;
    	document.getElementById("new"+b).innerHTML=price;
    	AjaxController.updatecartqty(b,a.value,price,responsefeed);
    }
   function responsefeed(a){
	   location.reload();
   }
    </script>
<script type="text/javascript">
	function getCheckedCheckboxesForSub(subCategoryName) {
		var checkboxes = document.querySelectorAll('input[name="'
				+ subCategoryName + '"]:checked'), values = [];
		Array.prototype.forEach.call(checkboxes, function(el) {
			values.push(el.value);
		});

		AjaxController.getproductListBySubCategory(values, productList);
	}

	function productList(productList) {
		var result = "";
		//alert("NO");
		//alert(productList);
		//alert(productList.length);
		if (productList.length < 1)
			document.getElementById("productList").innerHTML = "No Product Available !";
		else {
			for ( var i = 0; i < productList.length; i++) {
				var prodName = productList[i].productName;

				var prodId = productList[i].id;
				result += "<form action='productDetails.mm' method='post'><input type='hidden' name='pdt-details' value='"+prodId+"'>";
				result += "<div class='col-sm-4'><div class='product-image-wrapper'><div class='single-products'><div class='productinfo text-center'>";
				result += "<img style='width:100%;height:250px;' src='image.mm?id="
						+ prodId + "' /><p >" + prodName + "</p>";
				result += "<input type='submit' class='btn btn-default add-to-cart' value='Deatils'></input></div></div>";
				result += "</div></div></form>";
			}
		}
	}
	var productList = [];
	function toggleCheckbox(chk, element)
	 {
		 var lfckv = document.getElementById("checkbox"+element).checked;
         if(lfckv){
         productList.push(element);
         }
         else{
        	 productList.pop(element);
         }
        
         $("#qty"+element).prop("disabled",false);
	 }

	function getCheckedCheckboxesForBrand(brandName) {
		var checkboxes = document.querySelectorAll('input[name="' + brandName
				+ '"]:checked'), values = [];
		Array.prototype.forEach.call(checkboxes, function(el) {
			values.push(el.value);
		});

		AjaxController.getproductListByBrand(values, productList);
	}
</script>
<!-- <script type="text/javascript">
	function sublist() {
		alert("submitting");
			
		var productList=${product};
		alert(productList);
		
		/* window.location.href="quotesubmit.mm"; */
	}

</script> -->
<script type="text/javascript">

	function clickme(){
		var list = "";
		 for(var i=0; i<productList.length; i++){
			 var qty = document.getElementById("qty"+productList[i]);
			 var amt = document.getElementById("amt"+productList[i]);
		 
			 if(i==0){
				 list=qty+"_"+amt;
			 }
			 else{
				 list+=","+qty+"_"+amt;
			 }
			 
		 }
        	 alert(productList[i]);
		 //AjaxController.saveSearch(list); 
	}
</script>
</head>
<!--/head-->

<body>

	<header id="header">
		<!--header-->



		<div class="header-bottom">
			<!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span> <span
									class="icon-bar"></span> <span class="icon-bar"></span> <span
									class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.html">Home</a></li>
								<li class="dropdown"><a href="#" class="active">Shop<i
										class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
										<li><a href="shop.html" class="active">Products</a></li>
										<li><a href="product-details.html">Product Details</a></li>
										<li><a href="checkout.html">Checkout</a></li>
										<li><a href="addToCart.mm">Cart</a></li>
										<li><a href="login.mm">Login</a></li>
									</ul></li>

								<li><a href="contact-us.html">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>


	<section style="margin-top: 110px;">
		<div class="container">
			<div class="row">
				<%-- <div class="col-sm-3">
					<div class="left-sidebar">

						<h2>${subcategoryHeading}</h2>
						<div class="panel-group category-products" id="accordian">


							<c:forEach var="category" items="${subcategories}">

								<div class="panel panel-default">
									<div class="panel-heading">

										<h4 class="panel-title" id="categorysearch">
											<input type="checkbox" name="subcategory"
												onchange="getCheckedCheckboxesForSub('subcategory')"
												id="${category.subCategoryName}" value="${category.id}"><a
												href="#"> <label style="text-transform: capitalize;"
												for="${category.subCategoryName}">${category.subCategoryName}</label>
											</a>
										</h4>
									</div>
								</div>

							</c:forEach>

						</div>
						<!--/category-productsr-->


						<div class="panel-group category-products">
							<!--brands_products-->
							<h2>${BrandHeading}</h2>
							<div class="panel-group category-products" id="accordian">


								<c:forEach var="brand" items="${brands}">

									<div class="panel panel-default">
										<div class="panel-heading">

											<h4 class="panel-title" id="brandsearch">
												<input type="checkbox" name="brandname"
													onchange="getCheckedCheckboxesForBrand('brandname')"
													id="${brand.brandName}" value="${brand.id}"><a
													href="#"> <label for="${brand.brandName}">${brand.brandName}</label>
												</a>
											</h4>
										</div>
									</div>

								</c:forEach>

							</div>

						</div>
						<!--/brands_products-->


						<!-- <div class="price-range"> -->



						<!-- <div class="shipping text-center">
							shipping
							<img src="images/home/shipping.jpg" alt="" />
						</div> -->
						<!--/shipping-->

					</div>
				</div> --%>

				<div class="col-sm-12 padding-right">

					<div class="features_items">
						<!--features_items-->
						<!-- <h2 class="title text-center">Featured Items</h2> -->

						<span id="productList">
							<div class="table-responsive">
								<table class="table">
									<thead>
										<tr>
											<th>#</th>
											<th>Image</th>
											<th>Product Name</th>
											<th>Available Quantity</th>
											<th>Quantity</th>
											<th>Amount</th>
											<th>Total</th>
											<!-- <th>Estimated Delivery Date</th> -->
										</tr>
									</thead>

									<tbody>
										<c:choose>
											<c:when test="${empty product}">
												<h4 class="title text-center">No Products found</h4>
											</c:when>
											<c:otherwise>

												<c:forEach var="product" items="${product}"
													varStatus="count">
													<%-- <form action="productDetails.mm" method="post"> --%>
													<tr>
														<td><input type="checkbox" name="checkbox"
															id="checkbox${product.id}"
															onchange="toggleCheckbox(this, ${product.id})" /></td>
														<td><input type="hidden" name="pdt-details"
															value="${product.id}" /><img
															style="width: 100%; height: 250px;"
															src="image.mm?id=${product.id}" /></td>
														<td>${product.productName}</td>
														<td>${product.productQuantity}</td>
														<td><c:set var="changeqtyid" value="${product.id}" />
															<input type="text" name="quantity" disabled="disabled"
															value="${product.productQuantity}" id="qty${product.id}"
															onchange="updateQuantity(this, ${product.id})"
															autocomplete="off" size="2"></td>
														<td>${product.productSellingPrice}<input
															type="hidden" id="amt${product.id}"
															value="${product.productSellingPrice}" /></td>
														<td><span id="new${product.id}"></span><input
															type="hidden" id="tot${product.id}"><span
															id="new"></span></td>

													</tr>
												</c:forEach>
											</c:otherwise>

										</c:choose>

									</tbody>
								</table>
								<input type="button" class="btn btn-success btn-2"
									value="submit" onclick="clickme()">
								<!-- <input type="button" class="btn btn-success btn-2"
									value="submit" onclick="clickme();"> -->
								<!-- 	<input type="button" class="btn btn-success btn-2"
									onclick="clickme();" value="Submit"> -->

							</div>
						</span>
					</div>
					<!--features_items-->
				</div>
			</div>
		</div>
	</section>
	<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<!-- <script src="js/jquery.js"></script> -->
	<script src="js/price-range.js"></script>
	<!-- <script src="js/jquery.scrollUp.min.js"></script> -->
	<script src="js/bootstrap.min.js"></script>
	<!-- <script src="js/jquery.prettyPhoto.js"></script> -->
	<!-- <script src="js/main.js"></script> -->

</body>
</html>