	<!-- <link rel="shortcut icon" href="images/ico/favicon.ico">
	<footer id="footer">
		Footer
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="companyinfo">
							<h2>
								<span>MIS</span>
							</h2>
						</div>
					</div>
					<div class="col-sm-7">
						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe1.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p><b>Trends</b></p>
								
							</div>
						</div>

						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe2.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Stylish</p>
								
							</div>
						</div>

						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe3.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Formals</p>
								
							</div>
						</div>

						<div class="col-sm-3">
							<div class="video-gallery text-center">
								<a href="#">
									<div class="iframe-img">
										<img src="images/home/iframe4.png" alt="" />
									</div>
									<div class="overlay-icon">
										<i class="fa fa-play-circle-o"></i>
									</div>
								</a>
								<p>Fashions</p>
								
							</div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="address">
							<img src="images/home/map.png" alt="" />
							<p>505 S Atlantic Ave Virginia Beach, VA(Virginia)</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
		<div class="row pull-right"><img style="    width: 450px;
    height: 75px;" alt="SECURED WITH PAYPAL" src="images/paypalSecured.jpg">
		</div>
</div>
		<div class="footer-widget">
			<div class="container">
				<div class="row">
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Services</h2>
							<ul class="nav nav-pills nav-stacked">
							   <li><a href="contactus.mm">Contact Us</a></li>
								<li><a href="userOrderList.mm">Order Status</a></li>
								<li><a href="shipping.mm">shipping</a></li>
								
							</ul>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>Quick Shop</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="shop.mm?mainid=1">Books</a></li>
								<li><a href="quotation.mm">Quotation</a></li>
							</ul>
						</div>
					</div>
					
					<div class="col-sm-2">
						<div class="single-widget">
							<h2>About MIS</h2>
							<ul class="nav nav-pills nav-stacked">
								<li><a href="contactus.mm">About Us</a></li>
								
							</ul>
						</div>
					</div>
					<div class="col-sm-3 col-sm-offset-1">
						<div class="single-widget">
							<h2>About MIS</h2>
							<form action="#" class="searchform">
								<input type="text" placeholder="Your email address" />
								<button type="submit" class="btn btn-default">
									<i class="fa fa-arrow-circle-o-right"></i>
								</button>
								<p>
									Get the most recent updates from <br />our site and be updated
									your self...
								</p>
							</form>
							
						</div>
					</div>
					

				</div>
				
			</div>
			
		</div>
		
		
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2015 MIS. All rights
						reserved.</p>
				</div>
			</div>
		</div>

	</footer>
	/Footer
</body>
</html> -->
<html>
<head>
<link href="css/font-awesome.min.css" rel="stylesheet" />
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/style.css">
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700">
<!-- <link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/stylesheet.css" rel="stylesheet" />
<link href="css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<link href="css/animation.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<link href="css/forcheckout.css" rel="stylesheet" />
<link href="css/check-out-responsive.css" rel="stylesheet" />
<link href="css/pavmegamenu.css" rel="stylesheet" /> -->

<style>
#footer {
	background: #fff;
	 min-height: 100%;
  height: auto !important;
  height: 100%;
  /* Negative indent footer by its height */
  margin: 0 auto -60px;
  /* Pad bottom by footer height */
  padding: 0 0 60px;
}

#footer .box .box-heading {
	color: #f7c913 !important;
	font-size: 20px;
	font-family: lato;
}

#footer ul.list li a:hover {
	color: #fdc526;
}

#footer ul.list li a {
	color: #024205;
}

.social {
	border-bottom: 1px solid #fff !important;
}

.footer-center .col-lg-3 {
	border-left: 1px solid #fff !important;
}
</style>
</head>
<body>
	<section id="footer">
		<div class="footer-center">
			<div class="container">
				<div class="row">
					<div class="col-md-3" id="footer_information">
						<div class="box">
							<div class="box-heading">
								<span style="margin: -10px;">Information</span>
							</div>

							<ul class="list">
								<li><a href="http://www.portofaspirations.com/terms-of-use">Terms
										of Use</a></li>
								<li><a href="aboutUs.mm">About Us</a></li>
								<li><a
									href="http://www.portofaspirations.com/shipping-and-returns">Shipping
										and Returns</a></li>
								<li><a
									href="http://www.portofaspirations.com/privacy-policy">Privacy
										Policy</a></li>
								<li><a
									href="http://www.portofaspirations.com/affiliate-terms">Affiliate
										Terms</a></li>
								<li><a
									href="http://www.portofaspirations.com/tell-us-about-yourself--701">Tell
										Us About Yourself !</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3" id="customer_service">
						<div class="box">
							<div class="box-heading">
								<span style="margin: -5px;">Customer Service</span>
							</div>
							<ul class="list">
								<li><a href="contactus.mm">&nbsp;&nbsp;Contact Us</a></li>
								<li><a href="http://www.portofaspirations.com/return">&nbsp;&nbsp;Returns</a></li>
								<li><a href="http://www.portofaspirations.com/sitemap">&nbsp;&nbsp;Site
										Map</a></li>
								<li><a href="http://www.portofaspirations.com/manufacturer">&nbsp;&nbsp;Brands</a></li>
								<li><a href="http://www.portofaspirations.com/voucher">&nbsp;&nbsp;Gift
										Vouchers</a></li>
								<li><a
									href="http://www.portofaspirations.com/affiliate-account">&nbsp;&nbsp;Affiliates</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3" id="footer_account">
						<div class="box">
							<div class="box-heading">
								<span style="margin: -15px;">My Account</span>
							</div>
							<ul class="list">
								<li><a href="http://www.portofaspirations.com/my-account">&nbsp;&nbsp;My
										Account</a></li>
								<li><a href="userOrderList.mm">&nbsp;&nbsp;Order
										History</a></li>
								<li><a href="http://www.portofaspirations.com/wishlist">&nbsp;&nbsp;Wish
										List</a></li>
								<li><a href="http://www.portofaspirations.com/newsletter">&nbsp;&nbsp;Newsletter</a></li>
								<li><a href="http://www.portofaspirations.com/specials">&nbsp;&nbsp;Specials</a></li>
								<li><a href="quotation.mm">&nbsp;&nbsp;Quotation</a></li>
								<li><a href="demo.mm">&nbsp;&nbsp;Demo</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-3" id="footer_contact">
						<div class="box">
							<div class="box-heading">
								<span style="margin: 10px;">Keep in touch</span>
							</div>
							<a href="https://www.facebook.com/maestroinfotechsystems"
								target="_blank"
								class="btn azm-social azm-size-64 azm-circle azm-gradient azm-facebook"><i
								class="fa fa-facebook"></i></a> <a
								href="https://plus.google.com/u/0/114518331184059499673/posts"
								target="_blank"
								class="btn azm-social azm-size-64 azm-circle azm-gradient azm-twitter"><i
								class="fa fa-twitter"></i></a> <a
								href="https://mobile.twitter.com/maestro_sys/followers"
								target="_blank"
								class="btn azm-social azm-size-64 azm-circle azm-gradient azm-google-plus"><i
								class="fa fa-google-plus"></i></a> <a
								href="https://www.linkedin.com/profile/view?id=AAIAABHsvfkBmDi3YkVXhQsbB3tDljymE5e7KL4&trk=nav_responsive_tab_profile"
								target="_blank"
								class="btn azm-social azm-size-64 azm-circle azm-gradient azm-linkedin"><i
								class="fa fa-linkedin"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


</body>
</html>



