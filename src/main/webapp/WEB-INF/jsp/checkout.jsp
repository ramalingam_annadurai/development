<!DOCTYPE html>
<html lang="en">
<head>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Checkout | MIS</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
<script type="text/javascript">
   function  updateQuantity(a){
	   
    	var orderid=a;
    	 var qty=document.getElementById("qtychg"+orderid).value;
    	AjaxController.updatecartqty(orderid,qty,responsefeed);
    }
   function responsefeed(a){
	   location.reload();
   }
    </script>
    <script type="text/javascript">
    	function myOrder() {
			window.location.href='order.mm';
		}
    
    </script>
</head>
<!--/head-->
<body>
	<!-- <header id="header">
		header
		<div class="header-bottom">
			header-bottom
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span> <span
									class="icon-bar"></span> <span class="icon-bar"></span> <span
									class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.html">Home</a></li>
								<li class="dropdown"><a href="#">Shop<i
										class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
										<li><a href="shop.html">Products</a></li>
										<li><a href="product-details.html">Product Details</a></li>
										<li><a href="checkout.html" class="active">Checkout</a></li>
										<li><a href="cart.html">Cart</a></li>
										<li><a href="login.html">Login</a></li>
									</ul></li>
								<li><a href="contact-us.html">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search" />
						</div>
					</div>
				</div>
			</div>
		</div>
		/header-bottom
	</header> -->
	<!--/header-->
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Check out</li>
				</ol>
			</div>

			<!--/breadcrums-->


			<!-- <div class="checkout-options">
				<h3>New User</h3>
				<p>Checkout options</p>
				<ul class="nav">
					<li>
						<label><input type="checkbox"> Register Account</label>
					</li>
					<li>
						<label><input type="checkbox"> Guest Checkout</label>
					</li>
					<li>
						<a href=""><i class="fa fa-times"></i>Cancel</a>
					</li>
				</ul>
			</div> -->
			<!--/checkout-options-->

			<!-- <div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div> -->
			<!--/register-req-->
			<section id="cart_items">
				<div class="container">
					<!-- <div class="breadcrumbs">
				<ol class="breadcrumb">
					<li><a href="userOrderList.mm">Orderlist</a></li>
				</ol>
			</div> -->
					<div class="review-payment">
						<h2>Review & Payment</h2>
					</div>


					<div class="table-responsive cart_info">
						<table class="table table-condensed">
							<thead>
								<tr class="cart_menu">
									<td class="image">Item</td>
									<td class="description"></td>
									<td class="price">Price</td>
									<td class="quantity">Quantity</td>
									<td class="total">Total</td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								<c:set var="sum" value="${0}" />
								<c:forEach var="products" items="${cartproducts}">
									<c:set var="sumone"
										value="${products.productPrice*products.quantity}" />
									<c:set var="sum" value="${sum + sumone}" />
									<tr>
										<td class="cart_product"><a href=""><img
												src="image.mm?id=${products.pdtId}" alt=""></a></td>
										<td class="cart_description">
											<h4>
												<a href="">${products.productName}</a>
											</h4>
											<p>Web ID: 1089772</p>
										</td>
										<td class="cart_price">
											<p>${products.productPrice}</p>
										</td>
										<td class="cart_quantity">
											<div class="cart_quantity_button">
												<!-- <a class="cart_quantity_up" href=""> + </a> -->
												<c:set var="changeqtyid" value="${products.addToCardId}" />

												<input class="cart_quantity_input" type="text"
													style="width: 40px;" name="quantity"
													value="${products.quantity}" id="qtychg${changeqtyid}"
													onchange="updateQuantity(<c:out value='${changeqtyid}'/>)"
													autocomplete="off" size="2">
												<!-- <a class="cart_quantity_down" href=""> - </a> -->
											</div>
										</td>
										<td class="cart_total">
											<p class="cart_total_price">${products.productPrice*products.quantity}</p>
										</td>
										<td class="cart_delete">
										
										<%-- <c:set var="idofbrand" value="${AddToCart.id}" /> --%>
										
										<a class="cart_quantity_delete"
											href="delete.mm?id=${products.pdtId}"><i class="fa fa-times"></i></a></td>
									</tr>

								</c:forEach>
								<tr>
									<td colspan="4">&nbsp;</td>
									<td colspan="2">
										<table class="table table-condensed total-result">
											<tr>
												<td>Cart Sub Total</td>
												<td>${sum}</td>
											</tr>
											<tr>
												<td>Exo Tax</td>
												<td>${sum*0.06}</td>
											</tr>
											<tr class="shipping-cost">
												<td>Shipping Cost</td>
												<td>Free</td>
											</tr>
											<tr>
												<td>Total</td>
												<td><span>${sum+sum*0.06}</span></td>
											</tr>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<%-- <div class="shopper-informations">
						<div class="row">

							<div class="col-sm-5 clearfix pull-right">
								<div class="bill-to">
									<p>Bill To</p>
									<div class="form-one">
										<form>
											<input type="text" placeholder="Email *"
												value="${userDetails.userName}"> <input type="text"
												placeholder="Name *" value="${userDetails.firstName}">
											<textarea name="comment" placeholder="Address *">${userDetails.shippingAddress}</textarea>

										</form>
									</div>
									<div class="form-two">
										<form>
											<input type="text" placeholder="Zip / Postal Code *">
											<select>
												<option>-- Country --</option>
												<option>United States</option>
												<option>Bangladesh</option>
												<option>UK</option>
												<option>India</option>
												<option>Pakistan</option>
												<option>Ucrane</option>
												<option>Canada</option>
												<option>Dubai</option>
											</select> <select>
												<option>-- State / Province / Region --</option>
												<option>United States</option>
												<option>Bangladesh</option>
												<option>UK</option>
												<option>India</option>
												<option>Pakistan</option>
												<option>Ucrane</option>
												<option>Canada</option>
												<option>Dubai</option>
											</select> <input type="text" placeholder="Phone*">


										</form>
									</div>
								</div>
							</div>

						</div>
					</div> --%>

					<%-- <div class="payment-options pull-right row">
						<span class="col-sm-6">
							<form action="paymentconfirm.mm" method="get">
								<input type="hidden" name="paid" value="cod"> <input
									type="submit" value="Cash on Delivery">
							</form>
						</span> <span class="col-sm-6">
							<form action="https://www.sandbox.paypal.com/cgi-bin/webscr"
								method="post">
								<input type="hidden" name="cmd" value="_xclick"> <input
									type="hidden" name="business"
									value="ramalingamvp-facilitator@gmail.com">
								<input type="hidden" name="payer_email" value="<%= request.getParameter("userName") %>">
								<input type="hidden" name="payer_email" value="ramalingamvp">
								<input type="hidden" name="txn_type" value="cart">
								<input type="hidden" id="amount" name="amount" value="<%= request.getParameter("price") %>"></input>
								<c:forEach var="products" items="${cartproducts}">
									<input type="hidden" id="amount" name="amount"
										value="${products.productPrice*products.quantity+20.939999999999998}"></input>
									<!-- ${sum+sum*0.06} -->
								</c:forEach>
								<input type="hidden" name="bn"
									value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHostedGuest">
								<input type="hidden" name="button_subtype" value="services">
								<input type="hidden" name="return"
									value="http://localhost:8080/paymentconfirm.mm?paid=yes">
								<p>
									<input type="image" class="btn_nav fourCorners shadow"
										src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif"
										target="_blank" border="0" name="button"
										alt="PayPal - The safer, easier way to pay online!">
								</p>
								<!-- <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" 
        width="1" height="1"> -->
							</form>
						</span>
					</div> --%>
					<div class="">
					<input id="" class="btn btn-success btn2 " type="button" value="Continue" onclick="myOrder()"/>
	
				</div>
				</div>
			</section>
			<!--/#cart_items-->
			<script src="js/jquery.js"></script>
			<script src="js/bootstrap.min.js"></script>
			<script src="js/jquery.scrollUp.min.js"></script>
			<script src="js/jquery.prettyPhoto.js"></script>
			<script src="js/main.js"></script>
</body>
</html>