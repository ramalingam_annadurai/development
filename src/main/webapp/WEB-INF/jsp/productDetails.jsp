<%-- <!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core"%>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
 
 <script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript" src="src/jquery.simpleGallery.js"></script>
<script type="text/javascript" src="src/jquery.simpleLens.js"></script>

<script type="text/javascript">
	function AddToCartFunction(d) {
		var productid = d;
		var productquantity = document.getElementById("pdtquantity").value;
		AjaxController
				.addProductToCart(productid, productquantity, productList);
	}
	function productList(productList) {
		var count = productList;
		alert(count);
		if (count == 0)
			window.location.href = "login.mm";
		document.getElementById("cartnotification").innerHTML = productList;
	}
</script>
<script>
	$(document).ready(function() {
		$('#demo-1 .simpleLens-thumbnails-container img').simpleGallery({
			loading_image : 'demo/images/loading.gif'
		});

		$('#demo-1 .simpleLens-big-image').simpleLens({
			loading_image : 'demo/images/loading.gif'
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('#demo-2 .simpleLens-thumbnails-container img').simpleGallery({
			loading_image : 'demo/images/loading.gif',
			show_event : 'click'
		});

		$('#demo-2 .simpleLens-big-image').simpleLens({
			loading_image : 'demo/images/loading.gif',
			open_lens_event : 'click'
		});
	});
</script>
<!-- <script type="text/javascript">
	     
	    	
	   	function AddToCartFunction1(m)
	   		{
	    		var productid=m;
	    		var productquantity=document.getElementById("pdtquantity").value;
	    		AjaxController.addProductToCart(productid,productquantity, productList);
    		}
    	function productList(productList) 
    		{
    			var count=productList;
    			//alert(count);
    			window.location.href="checkout.mm";
    			if(count == 0)
    				window.location.href="login.mm";
     			document.getElementById("cartnotification").innerHTML=productList;
    		}
    </script> -->
</head>
<!--/head-->

<body>
	<header id="header"><!--header-->
		
	
		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.html">Home</a></li>
								<li class="dropdown"><a href="#">Shop<i class="fa fa-angle-down"></i></a>
                                    <ul role="menu" class="sub-menu">
                                        <li><a href="shop.html">Products</a></li>
										<li><a href="productDetails.mm" class="active">Product Details</a></li> 
										<li><a href="checkout.html">Checkout</a></li> 
										<li><a href="cart.html">Cart</a></li> 
										<li><a href="login.html">Login</a></li> 
                                    </ul>
                                </li> 
							
								
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search"/>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->
	
	<section style="  margin-top: 125px;  margin-bottom: 100px;">
		<div class="container">
			<div class="row">
				
				
				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							 <div class="view-product">
								<img style="width:100%;height:250px;" src="image.mm?id=${productsingle.id}" />
								
							</div> 
							

								  <!-- Controls -->
								 <!--  <a class="left item-control" href="#similar-product" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								  </a>
								  <a class="right item-control" href="#similar-product" data-slide="next">
									<i class="fa fa-angle-right"></i>
								  </a>
							</div> -->

						</div>
						
						<div class="col-sm-7" style="float:right;" >
							<div class="product-information"><!--/product-information-->
								
								<h2>${productsingle.productName}</h2>
								<!-- <p>Web ID: 1089772</p> -->
								<!-- <img src="images/product-details/rating.png" alt="" /> -->
								<p><b>MRP:${productsingle.productOriginalPrice}</b></p>
								<p><b>save:</b>${productsingle.productOriginalPrice - productsingle.productSellingPrice}</p>
								<span>
											
									<span>Pay:${productsingle.productSellingPrice}</span>
									
									<input type="hidden" value="1" id="pdtquantity"/>
									<br/>
									<button type="button" class="btn btn-fefault cart" onclick="AddToCartFunction('${productsingle.id}')">
										<i class="fa fa-shopping-cart"></i>
										Add to cart
									</button>

								<a class="btn btn-fefault cart" onclick="Function1('${productsingle.id}')">	Buy Now</a>
								</span>

								<p><b>Availability:</b> ${productsingle.productAvailability}</p>
								<p><b>Condition:</b> ${productsingle.productCondition}</p>
								<!-- <p><b>Brand:</b> E-SHOPPER</p> -->
								<!-- <a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a> -->
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					
				
				</div>

				
				<!-- <div class="col-sm-3">
					<div class="left-sidebar">
						
						
						<div class="shipping text-center">shipping
							<img src="images/home/shipping.jpg" alt="" />
						</div>/shipping
						
					</div>
				</div> -->
				
			</div>
		</div>
	</section>
	
	
	

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
<body>
<section style="  margin-top: 125px;  margin-bottom: 100px;">
<div class="col-sm-5">

	<article>
   
    <div class="simpleLens-gallery-container" id="demo-1">
        <div class="simpleLens-container">
            <div class="simpleLens-big-image-container">
                <a class="simpleLens-lens-image" data-lens-image="demo/large/benvado_elisa_bianco.jpg">
                    <img src="demo/medium/benvado_elisa_bianco.jpg" class="simpleLens-big-image">
                </a>
            </div>
        </div>

        <div class="simpleLens-thumbnails-container">
            <a href="#" class="simpleLens-thumbnail-wrapper"
               data-lens-image="demo/large/benvado_elisa_bianco.jpg"
               data-big-image="demo/medium/benvado_elisa_bianco.jpg">
                <img src="demo/thumbnail/benvado_elisa_bianco.jpg">
            </a>

            <a href="#" class="simpleLens-thumbnail-wrapper"
               data-lens-image="demo/large/camper_21926_red.jpg"
               data-big-image="demo/medium/camper_21926_red.jpg">
                <img src="demo/thumbnail/camper_21926_red.jpg">
            </a>

            <a href="#" class="simpleLens-thumbnail-wrapper"
               data-lens-image="demo/large/flylondon_most_red.jpg"
               data-big-image="demo/medium/flylondon_most_red.jpg">
                <img src="demo/thumbnail/flylondon_most_red.jpg">
            </a>
			
        </div>
		
    </div>
	
</article>

</div>
<div class="col-sm-7" style="float: right;">
		
	
	<div class="product-information">
		<!--/product-information-->

		<h2>The Moor's Account</h2>
		<!-- <p>Web ID: 1089772</p> -->
		<!-- <img src="images/product-details/rating.png" alt="" /> -->
		<p>
			<b>MRP:349</b>
		</p>
		<p>
			<b>save:</b>0
		</p>
		<span> <span>Pay:349</span> <input type="hidden" value="1"
			id="pdtquantity"> <br>
			<button type="button" class="btn btn-fefault cart"
				onclick="AddToCartFunction('8')">
				<i class="fa fa-shopping-cart"></i> Add to cart
			</button> <a href="checkout.mm" class="btn btn-fefault cart"> Buy Now</a>
		</span>

		<p>
			<b>Availability:</b>
		</p>
		<p>
			<b>Condition:</b>
		</p>
		<!-- <p><b>Brand:</b> E-SHOPPER</p> -->
		<!-- <a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a> -->
	</div>
	<!--/product-information-->
</div></section>



<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

</body>
</html> --%>


<!-- <!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Retail: Login</title>
<link href="css/font-awesome.min.css" rel="stylesheet" />
<link href="css/style.css">
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="css/bootstrap-responsive.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="css/tutoring-portal-main.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="css/tutoring-portal-form.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="css/elements.css" rel="stylesheet" type="text/css"
	media="all" />
<link href="mediaqueries/mediaqueries.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="mediaqueries/mediaqueries-tablet.css" rel="stylesheet"
	type="text/css" media="all" />
<link href="mediaqueries/mediaqueries-desktop.css" rel="stylesheet"
	type="text/css" media="all" />
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
	
</script>
<script type="text/javascript" src="js/jquery.simplyscroll.js"></script>
<link rel="stylesheet" href="css/jquery.simplyscroll.css" media="all"
	type="text/css">
<script type="text/javascript">
	function myFunction() {

		AjaxController.logout();
	}
	(function($) {
		$(function() {
			$("#scroller").simplyScroll();
		});
	})(jQuery);
</script>
</head>
<body onunload="myFunction()"> -->
<!-- <!DOCTYPE html>
<html lang="en"> -->
<!-- <head>
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id"
	content="677483487788-233tqbkb7qhruue5s4691ai7436nltmp.apps.googleusercontent.com">

<script src="https://apis.google.com/js/platform.js" async defer></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Welcome | MIS</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/search-box.css" rel="stylesheet">
[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>

<script type="text/javascript">
	function signOut() {
		var auth2 = gapi.auth2.getAuthInstance();
		auth2.signOut().then(function() {
			// Person is now logged out from Google
		});
		FB.logout(function(response) {
			// Person is now logged out from fb
		});

	}

	function forSearch() {
		var searchval = document.getElementById("searchbarbox").value;
		window.location.href = "shop.mm?searchval=" + searchval;
	}

	function onloadcart() {
		var uid = 1;
		AjaxController.getCountFromCart(uid, listofcart);

	}

	function listofcart(List) {

		document.getElementById("cartnotification").innerHTML = List;
	}
	function maincategorylist() {
		AjaxController.getmainlist(mainlist);
	}

	function mainlist(list) {
		var result = "";

		for (var i = 0; i < list.length; i++) {
			result += '<li style="width: 250px;"><a  href="shop.mm?mainid='
					+ list[i].mainId + '" onmouseover="subcategorylist('
					+ list[i].mainId + ')">' + list[i].mainCategoryName
					+ '</a><ul id="mainid'+list[i].mainId+'"></ul></li>';

		}
		document.getElementById("mainlist").innerHTML = result;

	}
	function subcategorylist(mainid) {

		AjaxController.getsublist(mainid, sublist);
	}
	function sublist(list) {
		result = "";
		for (var i = 0; i < list.length; i++) {
			result += '<li style="width: 220px;"><a  style="height: 200px;" href="shop.mm?mainid='
					+ list[i].mainId
					+ '&subid='
					+ list[i].id
					+ '" onmouseover="brandlistBySubidMainid('
					+ list[i].mainId
					+ ','
					+ list[i].id
					+ ')" >'
					+ list[i].subCategoryName
					+ '</a><ul id="subid'+list[i].mainId+list[i].id+'"></ul></li>';

			var id = "mainid" + list[i].mainId;
			document.getElementById(id).innerHTML = result;
		}
	}

	function brandlistBySubidMainid(mainid, subid) {

		AjaxController.brandlistbysubidmainid(mainid, subid, listofbrands);
	}
	function listofbrands(list) {
		var result = "";
		for (var i = 0; i < list.length; i++) {
			result += '<li style="width: 220px;"><a style="width: 220px;" href="shop.mm?mainid='
					+ list[i].mainCategoryId
					+ '&subid='
					+ list[i].subCategoryId
					+ '&brandid='
					+ list[i].id
					+ '">'
					+ list[i].brandName + '</a></li>';
			var id = "subid" + list[i].mainCategoryId + list[i].subCategoryId;
			document.getElementById(id).innerHTML = result;
		}

	}

	window.onload = onloadcart;
	window.onload = maincategorylist;
</script>
<style type="text/css">
nav ul ul {
	display: none;
	padding-left: 200px;
}

nav ul li:hover>ul {
	display: block;
}

nav ul {
	list-style: none; p;
	osition: relative;
	display: inline-table;
}

nav ul:after {
	content: "";
	clear: both;
	display: block;
}

nav ul li {
	float: left;
}

nav ul li:hover {
	backgroud: #4b545f;
	background: linear-gradient(top, #4f5964 0%, #5f 6975 40%);
	background: -moz-linear-gradient(top, #4f5964 0%, #5f6975 40%);
	background: -webkit-linear-gradient(top, #4f5964 0%, #5f6975 40%);
}

nav ul li:hover a {
	color: #fff;
}

nav ul li:a {
	display: block;
	padding: 25px 40px;
	color: #757575;
	text-decoration: none;
}

nav ul ul {
	background: green;
	border-radius: 0px;
	padding: 0;
	position: absolute;
	top: 100%;
}

nav ul ul li {
	float: none;
	border-top: 1px solid #6b727c;
	border-bottom: 1 px solid #575f6a;
	position: relative;
}

nav ul ul li a {
	padding: 15 px 40 px;
	color: #fff;
}

nav ul ul li a:hover {
	background: #4b545f;
	width: 100%;
}

nav ul ul ul {
	position: absolute;
	left: 100%;
	top: 0;
}

article {
	margin-top: 12%;
}

.simpleLens-mouse-cursor {
	height: 50px !important;
	 width: 40px !important;
	    background-color: #CCC;
    opacity: 0.2;
    filter: alpha(opacity = 20);
    position: absolute;
     top: 100px;
    left: 100px; 
    border: 1px solid #999;
    box-shadow: 0 0 2px 2px #999;
    cursor: none;
}

#footer {
	margin-top: 10%;
}
</style>

<link rel="stylesheet" type="text/css" href="css/jquery.simpleLens.css">
<link rel="stylesheet" type="text/css"
	href="css/jquery.simpleGallery.css">
</head>

<body>
	<header id="header">
		header
		<div class="header-bottom">
			header-bottom
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span> <span
									class="icon-bar"></span> <span class="icon-bar"></span> <span
									class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.html">Home</a></li>
								<li class="dropdown"><a href="#">Shop<i
										class="fa fa-angle-down"></i></a>
									<ul role="menu" class="sub-menu">
										<li><a href="#">Checkout</a></li>
										<li><a href="#">Cart</a></li>
									</ul></li>
								<li><a href="contact-us.html">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<input type="text" placeholder="Search" />
						</div>
					</div>
				</div>
			</div>
		</div>
		/header-bottom
	</header>
				header-bottom<br><br><br>
				<div class="container">
					<div class="row">
						<div class="mainmenu pull-left">
							<nav>
								<ul class="nav navbar-nav collapse navbar-collapse">
									<li><a href="welcome.mm">Home</a></li>
									<li><a href="#">Categories</a>
										<ul id="mainlist">
										</ul></li>
									<li><a href="#">Shop</a>
										<ul>
											<li><a href="addToCart.mm">Cart</a></li>
											<li><a href="checkout.mm">Checkout</a></li>
											<li><a href="shipping.mm">Shipping</a></li>
											<li></li>
										</ul></li>
									<li><a href="contactus.mm">Contact Us</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
			/header-bottom
		</div>
	</header>

	<!DOCTYPE html>
<html lang="en"> -->
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<title>Login | MIS</title>
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/loginvalidation.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<!-- <!-- <link href="css/login.css" rel="stylesheet"> -->
-->
<link href="css/search-box.css" rel="stylesheet" />
<!-- <link htef="css/jquery.simpleLens.css" rel="stylesheet"> -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<style type="text/css">
</style>
<script type="text/javascript">
	function AddToCartFunction(d) {
		var productid = d;
		var productquantity = document.getElementById("pdtquantity").value;
		AjaxController
				.addProductToCart(productid, productquantity, productList);
	}
	function productList(productList) {
		var count = productList;
		alert(count);

		document.getElementById("cartnotification").innerHTML = productList;
	}
</script>

<script type="text/javascript">
	function AddToBuy(d) {
		var productid = d;
		var productquantity = document.getElementById("pdtquantity").value;
		AjaxController.addProductToCart(productid, productquantity,
				productList1);
	}
	function productList1(productList1) {
		var count = productList1;
		/* alert(count); */
		document.getElementById("cartnotification").innerHTML = productList1;
		window.location.href = 'checkout.mm';
	}
</script>

</head>
<br>
<br>
<br>
<br>
<br>
<!--/head-->
<div class="col-sm-7" style="float: right;">
	<div class="product-information">
		<!--/product-information-->

		<h2>${productsingle.productName}</h2>
		<!-- <p>Web ID: 1089772</p> -->
		<!-- <img src="images/product-details/rating.png" alt="" /> -->
		<p>
			<b>MRP:${productsingle.productOriginalPrice}</b>
		</p>


		<p>
			<b>save:</b>${productsingle.productOriginalPrice - productsingle.productSellingPrice}</p>



		<c:if
			test="${productsingle.productOriginalPrice - productsingle.productSellingPrice>0}">

			<b> discount:</b>${((productsingle.productOriginalPrice-productsingle.productSellingPrice )*100)/productsingle.productOriginalPrice}.<b>%</b>

		</c:if>
		<html>
<head>
<title>JSTL fmt:formatNumber Tag</title>
</head>
<body>

</body>
		</html>










		<span> <span>Pay:${productsingle.productSellingPrice}</span> <input
			type="hidden" value="1" id="pdtquantity" /> <br />
			<button type="button" class="btn btn-fefault cart"
				onclick="AddToCartFunction('${productsingle.id}')">
				<i class="fa fa-shopping-cart"></i> Add to cart
			</button> <a class="btn btn-fefault cart"
			onclick="AddToBuy('${productsingle.id}')">Buy Now</a>
		</span>

		<p>
			<b>Availability:</b> ${productsingle.productAvailability}
		</p>
		<p>
			<b>Condition:</b> ${productsingle.productCondition}
		</p>
		<!-- <p><b>Brand:</b> E-SHOPPER</p> -->
		<!-- <a href=""><img src="images/product-details/share.png" class="share img-responsive"  alt="" /></a> -->
	</div>
	<!--/product-information-->
</div>
</div>
<!--/product-details-->



</div>
<article>

	<div class="simpleLens-gallery-container" id="demo-1">
		<div class="simpleLens-container">
			<div class="simpleLens-big-image-container">
				<a class="simpleLens-lens-image"
					data-lens-image="image.mm?id=${productsingle.id}"
					style="height: 1600px; width: 1061px;"> <img
					src="image.mm?id=${productsingle.id}" class="simpleLens-big-image">
				</a>

			</div>
		</div>

		<div class="simpleLens-thumbnails-container">
			<a href="#" class="simpleLens-thumbnail-wrapper"
				data-lens-image="image.mm?id=${productsingle.id} "
				data-big-image="image.mm?id=${productsingle.id}"> <img
				src="image.mm?id=${productsingle.id}"
				style="height: 66px; width: 43px">&nbsp;
			</a> <a href="#" class="simpleLens-thumbnail-wrapper"
				data-lens-image="image.mm?id=${productsingle.id}"
				data-big-image="image.mm?id=${productsingle.id}"> <img
				src="image.mm?id=${productsingle.id}"
				style="height: 66px; width: 43px">&nbsp;
			</a> <a href="#" class="simpleLens-thumbnail-wrapper"
				data-lens-image="image.mm?id=${productsingle.id}"
				data-big-image="image.mm?id=${productsingle.id}"> <img
				src="image.mm?id=${productsingle.id}"
				style="height: 66px; width: 43px">&nbsp;
			</a>

		</div>
		<!--  <div class="simpleLens-thumbnails-container">
			<a href="#" class="simpleLens-thumbnail-wrapper"
				data-lens-image="demo/large/benvado_elisa_bianco.jpg"
				data-big-image="demo/medium/benvado_elisa_bianco.jpg"> <img
				src="demo/thumbnail/benvado_elisa_bianco.jpg">
			</a> <a href="#" class="simpleLens-thumbnail-wrapper"
				data-lens-image="demo/large/camper_21926_red.jpg"
				data-big-image="demo/medium/camper_21926_red.jpg"> <img
				src="demo/thumbnail/camper_21926_red.jpg">
			</a> <a href="#" class="simpleLens-thumbnail-wrapper"
				data-lens-image="demo/large/flylondon_most_red.jpg"
				data-big-image="demo/medium/flylondon_most_red.jpg"> <img
				src="demo/thumbnail/flylondon_most_red.jpg">
			</a>

		</div>  -->

	</div>

</article>


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script type="text/javascript" src="src/jquery.simpleGallery.js"></script>
<script type="text/javascript" src="src/jquery.simpleLens.js"></script>

<script>
	$(document).ready(function() {
		$('#demo-1 .simpleLens-thumbnails-container img').simpleGallery({
			loading_image : 'demo/images/loading.gif'
		});

		$('#demo-1 .simpleLens-big-image').simpleLens({
			loading_image : 'demo/images/loading.gif'
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('#demo-2 .simpleLens-thumbnails-container img').simpleGallery({
			loading_image : 'demo/images/loading.gif',
			show_event : 'click'
		});

		$('#demo-2 .simpleLens-big-image').simpleLens({
			loading_image : 'demo/images/loading.gif',
			open_lens_event : 'click'
		});
	});
</script>

</body>

</html>
