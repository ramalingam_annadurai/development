<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


 </!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Retail: Login</title>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap-responsive.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/tutoring-portal-main.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/tutoring-portal-form.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/elements.css" rel="stylesheet" type="text/css" media="all" />
<link href="mediaqueries/mediaqueries.css" rel="stylesheet" type="text/css" media="all" />
<link href="mediaqueries/mediaqueries-tablet.css" rel="stylesheet" type="text/css" media="all" />
<link href="mediaqueries/mediaqueries-desktop.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
</script>
<script type="text/javascript" src="js/jquery.simplyscroll.js"></script>
<link rel="stylesheet" href="css/jquery.simplyscroll.css" media="all" type="text/css">
<script type="text/javascript">
function myFunction()
{

AjaxController.logout();
}
(function($) {
	$(function() {
		$("#scroller").simplyScroll();
	});
})(jQuery);
</script>
</head>
<body onunload="myFunction()">
	<!DOCTYPE html>

<html lang="en">
<head>
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id"
	content="677483487788-233tqbkb7qhruue5s4691ai7436nltmp.apps.googleusercontent.com">

<script src="https://apis.google.com/js/platform.js" async defer></script>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Welcome | MIS</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/search-box.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>

<script type="text/javascript">
	function signOut() {
		var auth2 = gapi.auth2.getAuthInstance();
		auth2.signOut().then(function() {
			// Person is now logged out from Google
		});
		FB.logout(function(response) {
			// Person is now logged out from fb
		});

	}

	function forSearch() {
		var searchval = document.getElementById("searchbarbox").value;
		window.location.href = "shop.mm?searchval=" + searchval;
	}

	function onloadcart() {
		var uid = 1;
		AjaxController.getCountFromCart(uid, listofcart);

	}

	function listofcart(List) {

		document.getElementById("cartnotification").innerHTML = List;
	}
	function maincategorylist() {
		AjaxController.getmainlist(mainlist);
	}

	function mainlist(list) {
		var result = "";

		for ( var i = 0; i < list.length; i++) {
			result += '<li style="width: 250px;"><a  href="shop.mm?mainid='
					+ list[i].mainId + '" onmouseover="subcategorylist('
					+ list[i].mainId + ')">' + list[i].mainCategoryName
					+ '</a><ul id="mainid'+list[i].mainId+'"></ul></li>';

		}
		document.getElementById("mainlist").innerHTML = result;

	}
	function subcategorylist(mainid) {

		AjaxController.getsublist(mainid, sublist);
	}
	function sublist(list) {
		result = "";
		for ( var i = 0; i < list.length; i++) {
			result += '<li style="width: 220px;"><a  style="height: 200px;" href="shop.mm?mainid='
					+ list[i].mainId
					+ '&subid='
					+ list[i].id
					+ '" onmouseover="brandlistBySubidMainid('
					+ list[i].mainId
					+ ','
					+ list[i].id
					+ ')" >'
					+ list[i].subCategoryName
					+ '</a><ul id="subid'+list[i].mainId+list[i].id+'"></ul></li>';

			var id = "mainid" + list[i].mainId;
			document.getElementById(id).innerHTML = result;
		}
	}

	function brandlistBySubidMainid(mainid, subid) {

		AjaxController.brandlistbysubidmainid(mainid, subid, listofbrands);
	}
	function listofbrands(list) {
		var result = "";
		for ( var i = 0; i < list.length; i++) {
			result += '<li style="width: 220px;"><a style="width: 220px;" href="shop.mm?mainid='
					+ list[i].mainCategoryId
					+ '&subid='
					+ list[i].subCategoryId
					+ '&brandid='
					+ list[i].id
					+ '">'
					+ list[i].brandName + '</a></li>';
			var id = "subid" + list[i].mainCategoryId + list[i].subCategoryId;
			document.getElementById(id).innerHTML = result;
		}

	}

	window.onload = onloadcart;
	window.onload = maincategorylist;
</script>
<style type="text/css">
nav ul ul {
	display: none;
	padding-left: 200px;
}

nav ul li:hover>ul {
	display: block;
}

nav ul {
	list-style: none; p;
	osition: relative;
	display: inline-table;
}

nav ul:after {
	content: "";
	clear: both;
	display: block;
}

nav ul li {
	float: left;
}

nav ul li:hover {
	backgroud: #4b545f;
	background: linear-gradient(top, #4f5964 0%, #5f 6975 40%);
	background: -moz-linear-gradient(top, #4f5964 0%, #5f6975 40%);
	background: -webkit-linear-gradient(top, #4f5964 0%, #5f6975 40%);
}

nav ul li:hover a {
	color: #fff;
}

nav ul li:a {
	display: block;
	padding: 25px 40px;
	color: #757575;
	text-decoration: none;
}

nav ul ul {
	background: green;
	border-radius: 0px;
	padding: 0;
	position: absolute;
	top: 100%;
}

nav ul ul li {
	float: none;
	border-top: 1px solid #6b727c;
	border-bottom: 1 px solid #575f6a;
	position: relative;
}

nav ul ul li a {
	padding: 15 px 40 px;
	color: #fff;
}

nav ul ul li a:hover {
	background: #4b545f;
	width: 100%;
}

nav ul ul ul {
	position: absolute;
	left: 100%;
	top: 0;
}
.mainmenu {
margin-top:-4%;
}
</style>
</head>
<!--/head-->

<body>
	<header id="header">
		<!--header-->
		<div class="navbar-fixed-top">
			<div class="header_top " style="background-color: darkorange;">
				<!--header_top-->
				<div class="container">
					<div class="row">
						<div class="col-sm-6">
							<div class="contactinfo">
								<ul class="nav nav-pills">
									<li><a href="#" style="color: white; font-size: 16px;"><i
											class="fa fa-phone"></i> +2 95 01 88 821</a></li>
									<li><a href="#" style="color: white; font-size: 16px;"><i
											class="fa fa-envelope"></i> info@maestroinfotech.in</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="social-icons pull-right">
								<ul class="nav navbar-nav">

									<li><a href="checkout.mm"
										style="color: white; background-color: darkorange; font-size: 16px;"><i
											class="fa fa-crosshairs"></i> Checkout</a></li>

									<Li style="margin-right: 10px; margin-left: 10px;"><fb:login-button
											scope="public_profile,email" onlogin="checkLoginState();">
										</fb:login-button></Li>
									<Li><div class="g-signin2" data-onsuccess="onSignIn"
											data-theme="dark"></div></Li>
									
										
										

											<li><a href="login.mm"
												style="color: white; background-color: darkorange; font-size: 16px;"><i
													class="fa fa-lock"></i> Login</a></li>
										

									

									

									<!--facebook login script  -->
									<script>
										window.fbAsyncInit = function() {
											FB.init({
												appId : '449419121911243',
												xfbml : true,
												version : 'v2.5'
											});
										};

										(function(d, s, id) {
											var js, fjs = d
													.getElementsByTagName(s)[0];
											if (d.getElementById(id)) {
												return;
											}
											js = d.createElement(s);
											js.id = id;
											js.src = "//connect.facebook.net/en_US/sdk.js";
											fjs.parentNode
													.insertBefore(js, fjs);
										}(document, 'script', 'facebook-jssdk'));
									</script>
									<script>
										// This is called with the results from from FB.getLoginStatus().
										function statusChangeCallback(response) {
											console.log('statusChangeCallback');
											console.log(response);
											// The response object is returned with a status field that lets the
											// app know the current login status of the person.
											// Full docs on the response object can be found in the documentation
											// for FB.getLoginStatus().
											if (response.status === 'connected') {
												// Logged into your app and Facebook.
												testAPI();
											} else if (response.status === 'not_authorized') {
												// The person is logged into Facebook, but not your app.
												document
														.getElementById('status').innerHTML = 'Please log '
														+ 'into this app.';
											} else {
												// The person is not logged into Facebook, so we're not sure if
												// they are logged into this app or not.
												document
														.getElementById('status').innerHTML = 'Please log '
														+ 'into Facebook.';
											}
										}

										// This function is called when someone finishes with the Login
										// Button.  See the onlogin handler attached to it in the sample
										// code below.
										function checkLoginState() {
											FB
													.getLoginStatus(function(
															response) {
														statusChangeCallback(response);
													});
										}
										window.fbAsyncInit = function() {
											FB.init({
												appId : '1504336839865551',
												cookie : true, // enable cookies to allow the server to access 
												// the session
												xfbml : true, // parse social plugins on this page
												version : 'v2.2' // use version 2.2
											});

											// Now that we've initialized the JavaScript SDK, we call 
											// FB.getLoginStatus().  This function gets the state of the
											// person visiting this page and can return one of three states to
											// the callback you provide.  They can be:
											//
											// 1. Logged into your app ('connected')
											// 2. Logged into Facebook, but not your app ('not_authorized')
											// 3. Not logged into Facebook and can't tell if they are logged into
											//    your app or not.
											//
											// These three cases are handled in the callback function.

											FB
													.getLoginStatus(function(
															response) {
														statusChangeCallback(response);
													});

										};
										// Load the SDK asynchronously
										(function(d, s, id) {
											var js, fjs = d
													.getElementsByTagName(s)[0];
											if (d.getElementById(id))
												return;
											js = d.createElement(s);
											js.id = id;
											js.src = "//connect.facebook.net/en_US/sdk.js";
											fjs.parentNode
													.insertBefore(js, fjs);
										}(document, 'script', 'facebook-jssdk'));

										// Here we run a very simple test of the Graph API after login is
										// successful.  See statusChangeCallback() for when this call is made.
										function testAPI() {
											console
													.log('Welcome!  Fetching your information.... ');
											FB
													.api(
															'/me?fields=email,name',
															function(response) {

																var email = response.email;
																var name = response.name;

																AjaxController
																		.googleFblogin(
																				name,
																				email,
																				respnseFb);
															});
										}
										function respnseFb(val) {

										}
									</script>
									<!--/facebook login script  -->
									<!--google login script  -->
									<script>
										var flag = true;
										function onSignIn(googleUser) {
											// Useful data for client-side scripts:

											var profile = googleUser
													.getBasicProfile();
											var uname = profile.getName();
											var mail = profile.getEmail();

											AjaxController.googleFblogin(uname,
													mail, respnse);
											// The ID token :
											var id_token = googleUser
													.getAuthResponse().id_token;
										};
										function respnse(val) {
										}
									</script>
									<!--/google login script  -->
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/header_top-->
			<div class="header_middle" style="background-color: green;">
				<!--header-middle-->
				<div class="container" style="background-color: green;">
					<div class="row">
						<div class="col-sm-3">
							<!-- <div class="logo pull-left">
							<a href="index.html"><img src="images/home/logo.png" alt="" /></a>
						</div> -->
							<!-- 						<div class="btn-group pull-right">
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									USA
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Canada</a></li>
									<li><a href="#">UK</a></li>
								</ul>
							</div>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle usa" data-toggle="dropdown">
									DOLLAR
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Canadian Dollar</a></li>
									<li><a href="#">Pound</a></li>
								</ul>
							</div>
						</div> -->
						</div>
						<div class="col-sm-9">
							<div class="shop-menu pull-right">
								<ul class="nav navbar-nav">
									<!-- <li><a href="admindashboard.mm" style="color:white;background-color:green;"><i class="fa fa-user"></i>
										Account</a></li>
										<li><a href="#" style="color:white;background-color:green;"><i class="fa fa-star"></i> Wishlist</a></li> -->
									<!-- <Li> <input type="text" placeholder="search here"
										style="width: 600px; height: 30px;margin-top: 20px; border-radius: 15px;padding: 20px;"></Li> -->
									<li style="margin-top:5px;margin-bottom:4px;">
										<div>
						<form id="searchbox" action="">
											<input id="search" type="text" placeholder="Type here">
											<input id="submit" type="submit" value="Search">
										</form>
						</div>
									</li>
									<li style="margin-top:-5px;"><a href="addToCart.mm"
										style="color: white; PADDING: 10PX; border-radius: 30px; PADDING-LEFT: 5PX; padding-right: 5px; background-color: #CABC18; font-size: 22px;">
											<i class="fa fa-shopping-cart"></i> Cart <small
											class="circleCount" id="cartnotification">0</small>
									</a></li>
								</ul>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<!--/header-middle-->
			<!--header bottom navs  -->
			<div class="header-bottom">
				<!--header-bottom-->
				<div class="container">
					<div class="row">
						<div class="mainmenu pull-left">
							<nav>
								<ul class="nav navbar-nav collapse navbar-collapse">
									<li><a href="welcome.mm">Home</a></li>
									<li><a href="#">Categories</a>
										<ul id="mainlist">
										</ul></li>
									<li><a href="#">Shop</a>
										<ul>
											<li><a href="addToCart.mm">Cart</a></li>
											<li><a href="checkout.mm">Checkout</a></li>
											<li><a href="shipping.mm">Shipping</a></li>
											<li></li>
										</ul></li>
									<li><a href="contactus.mm">Contact Us</a></li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
			<!--/header-bottom-->
		</div>
	</header>