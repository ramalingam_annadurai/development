<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link href="css/bootstrap.css" rel="stylesheet" />
<link href="css/stylesheet.css" rel="stylesheet" />
<link href="css/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<link href="css/animation.css" rel="stylesheet" />
<link href="css/font-awesome.min.css" rel="stylesheet" />
<link href="css/forcheckout.css" rel="stylesheet" />
<link href="css/check-out-responsive.css" rel="stylesheet" />
<link href="css/pavmegamenu.css" rel="stylesheet" />
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<style>
.login-signup, .social-label{
color:#024205;
}
</style>
<script type="text/javascript">
 function myShipping() {
	window.location.href='shippingDetails.mm';
}
</script>
<script type="text/javascript">
    	function myOrder() {
			window.location.href='order.mm';
		}
    
    </script>
</head>
<body>
<div class="forcheckout">
		<div class="steps">
			
<ul class="step-tabs">
	<li class="login-sel" onclick="step_login();">&nbsp;</li>
	<li class="arrow-current">&nbsp;</li>
	<li class="address-unsel">&nbsp;</li>
	<li class="arrow-next">&nbsp;</li>
	<li class="summary-unsel" onclick="order_summary.html">&nbsp;</li>
	<li class="arrow-next">&nbsp;</li>
	<li class="payment-unsel">&nbsp;</li>
</ul>
<div class="clear"></div>
<div class="step_login">
<div class="login">
	<form id="login-form" method="post" action="">
		<div class="login-signup">
			<div class="label-text">
				<label for="email-id">
					Email Address <span class="required">*</span>
				</label>
			</div>
			<div class="details">
				<div>
				
					<input type="text" class="email-id" id="email-id" name="email" value="${userDetails.userName}" autocomplete="off">
					<span class="info email">Your order details will be sent to this email address.</span>
				</div>
								<div class="guest-login margintop10">
					<input id="guest" type="radio" value="guest" name="login_type" class="radio-input" checked="checked">
					<label>Continue without password</label>
					<span class="info">(You do not need a password)</span>
				</div>
								<div class="member-login margintop10">
					<input id="member" type="radio" value="account" name="login_type" class="radio-input">
					<label>I have a Port of Aspirations account and password</label>
					<span class="info">Sign in to your account and checkout faster</span>
					<div class="password margintop10" style="display:none;">
						<div class="label-text">
							<label>Password</label>
						</div>
						<div class="lastdetail">
							<input type="password" id="password" name="password" autocomplete="off"><br>
							<span class="margintop10"><a class="forgot" href="http://www.portofaspirations.com/index.php?route=account/forgotten">Forgot your password?</a></span>
						</div>
					</div>
				</div>
				<div class="margintop10">
					<input id="button-login" class="btn orange continue" type="button" value="Continue" onclick="myShipping()">
	
				</div>
			</div>
		</div>
	</form>
</div>
<div class="or"></div>
<div class="social-login">
	<div class="login-box">
		<div class="social-label"><label>Sign in with</label></div>

		<div class="fb-login margintop10">
			<a onclick="fbtrack();" href="javascript:void(0)" class="fb-btn"></a>
		</div><br>
		<div class="fb-login margintop10">
			<a href="http://www.portofaspirations.com/index.php?route=account/register" class="forcheckout-register"></a>
		</div>
			</div>
</div>
</div>
</div>
		  
		<div class="summary-right">
			<!--<div class="summary-order"></div>
			<div class="summary-address"></div>-->
			<div class="summary-cod"></div>			
		</div>
			</div>
</body>
</html>