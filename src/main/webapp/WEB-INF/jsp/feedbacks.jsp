<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>This is order list page</title>
<!-- <script type="text/javascript" src="/js/jquery-1.8.3.js"></script> -->
<!-- <script type="text/javascript" src="js/jquery.ui.core.min.js"></script> -->
<!-- <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script> -->
<script src="js/bootstrap.min.1530170614.js"></script>
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/ordervalidation.js"></script>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<script type="text/javascript" src="dwr/interface/AjaxController.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script type="text/javascript">
function cancelOrder(oid){
	alert("yes");
	window.location.href="cancel.mm?orderId="+oid;
}
/* <!-- 	function myFunc() {
		
		var res1 = "";
		for (var i = 0; i < productDetails.length; i++) {
			var orderId = productDetails[i].orderId;
			alert(orderId);
		}
		res1 += 'href="pick.mm?orderId='
				+ orderId +'">pick</a>';

		document.getElementById("pick").innerHTML = res1;
	}--> */
</script>

<style type="text/css">
.bs-example {
	margin: 20px;
}
</style>
</head>
<body>
	<!-- <div align="center">
		<div class="container">
			<div class="row">
				<div class="panel-body">
					<form name="orderform" id="orderform" action="userOrderList.mm" method="post"  onsubmit="return validateForm()">
				</form>

				</div>
				<span id="a"></span>
			</div>
		</div>
	</div> -->
	 <br>
	<br>
	<br>
	<br>
	<br>
	<br>

	<nav class="navbar-collapse collapse" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<div class="panel panel-default">
						<div class="panel-heading">
							<!-- <h3 align="left">User Orders</h3> -->
						</div>
						<div class="panel-body">
							<div class="form-group">
								<form action="cancel.mm">
									<table style="width: 100%" class="table table-hover">
										<tr>
											<th>Name</th>
											<th>Phone Number</th>
											<th>Order Id</th>
											<th>Status</th>
											<!-- <th>User Id</th> -->
											<th>Date</th>
											<th>Cancel</th>
										</tr>
										<c:forEach var="order" items="${orderList}">
											<tr>
												<td>${order.userName}</td>
												<td>${order.phoneNumber}</td>
												<td>${order.orderId}<input name="orderId" type="hidden"
													value="${order.orderId}" /></td>
												<td id="tosId">${order.status}</td>

												<td>${order.date}</td>
												<c:if test="${order.status eq 'pending'}">
													<td><input type="button"
														onclick="cancelOrder(${order.orderId})" value="Cancel"></td>
												</c:if>
												<c:if test="${order.status eq 'dispatched'}">
													<td><input type="submit" value="Return"></td>
												</c:if>
												<td></td>
												<td>
											</tr>
										</c:forEach>
									</table>
								</form>
							</div>

						</div>
					</div>
				</div>

			</div>
			<div class="col-xs-4"></div>
			<div class="col-xs-4">
				<input type="submit" onclick="window.location.href='welcome.mm'"
					value="Continue Shopping"
					style="color: green; margin-right: -10px; margin-top: -3px;"
					class="btn btn-3">
				<div class="col-xs-4"></div>
			</div>
		</div>

	</nav>
	<br>
</body>
</html>