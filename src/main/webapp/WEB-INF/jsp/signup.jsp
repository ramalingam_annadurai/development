<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Login | MIS</title>
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="js/signupvalidation.js"></script>
<script src="js/validate.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/login.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->

<link rel="shortcut icon" href="images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="images/ico/apple-touch-icon-57-precomposed.png">
<script type="text/javascript">
function checkSubmit() {
 if (document.getElementById('policy').checked == false) {
  document.getElementById("add").innerHTML="In order to use our services, you must agree to companies's Terms of Service.";
  return false;
 } else {
  return true;
 }
}
</script>
</head>
<!--/head-->
<body>
	<section id="form">
		<!--form-->
		<div class="container">
			<div class="row">
				<h4 style="">Please register here</h4>
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form ">
						<!--login form-->
						<form action="register.mm" method="post" name="loginForm"
							id="loginForm" onsubmit="return checkSubmit()">
							<input name="firstName" id="firstName" tabindex="1" type="text"
								placeholder="First Name" autofocus="autofocus" /><span id="nameloc"
								style="color: red"></span> <input name="lastName" id="lastName"
								tabindex="2" type="text" placeholder="Last Name" /><span
								id="lastloc"></span> <input name="email" tabindex="3"
								type="email" placeholder="Email" /> <input name="mobileNo"
								id="mobileNo" tabindex="4" type="text"
								placeholder="Mobile Number" /><span id="numloc"
								style="color: red"></span><br>
							<textarea rows="4" cols="10" tabindex="6" name="permanentAddress"
								id="address" placeholder="permanent Address"></textarea>
							<br> <br>
							<textarea rows="4" cols="10" tabindex="7" name="shippingAddress"
								id="address" placeholder="Shipping Address"></textarea>
							<br> <br> <input name="password" id="password"
								tabindex="8" type="password" placeholder="Password" /> <input
								name="confirmPassword" id="confirmPassword" tabindex="9"
								type="password" placeholder="Confirm Password" />
								<em></em><input type="checkbox" name="policy" value="yes" id="policy">I agree to the company Terms of Service and Privacy Policy<br> 
								 <!-- <span role="alert" class="errormsg" id="errormsg_0_TermsOfService">In order to use our services, you must agree to companies's Terms of Service.</span> -->
								 <span id="add"></span>
								 <input
								type="submit" value="Submit" class="btn btn-success" id="login">
						</form>
					</div>
					<!--/login form-->
				</div>
	<div class="col-sm-2 col-sm-offset-1">
	
		<a href="student.mm"><h4>Student here</h4></a>
	
	</div>
	<div class="col-sm-2 col-sm-offset-1">
	
		<a href="teacher.mm"><h4>Teacher here</h4></a>
	
	</div>
				<!--sign up form-->
			</div>
		</div>
	</section>
	<!--/form-->
	<!--  <script src="js/jquery.js"></script>  -->
	<script src="js/price-range.js"></script>
	<!--  <script src="js/jquery.scrollUp.min.js"></script> -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.prettyPhoto.js"></script>
	<script src="js/main.js"></script>
</body>
</html>