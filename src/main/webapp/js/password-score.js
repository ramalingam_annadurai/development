$(document).ready(function () {
	$('.subCategoryMenuLink').click(function () {
		window.location = '/subCategory.do?id=' + $(this).attr('id') + '&parentId=1';
	});
});

function populateCategoriesFromJSON(parentId, parentImageName) {
	var catImageURL = "https://s3.amazonaws.com/images.mistminds.com/category_" + parentId + "_" + parentImageName + ".jpg";
	var categoriesHTML = '<img src="' + catImageURL+'"/><div id="subCategoryLinks">';
	
	$.each(categoriesJson.categories, function (i, category) {
		if(parentId == category.parentId){
		subCatImageURL = "http://images.mistminds.com.s3.amazonaws.com/subcategory_" + category.id + "_" + category.imageName + ".jpg";
		categoriesHTML = categoriesHTML + '<div class="subcategoryImageDiv">';
		categoriesHTML = categoriesHTML + '<a href=""><img border="0" src="' + subCatImageURL + '"></a>';
		categoriesHTML = categoriesHTML + '<div class="subcategoryText">';
		categoriesHTML = categoriesHTML + '<a href="subCategory.do?id=2&amp;parentId=1&amp;imageName='+category.imageName+'">'+category.name+'</a>';
		categoriesHTML = categoriesHTML + '</div></div>';
		}
	});
	categoriesHTML = categoriesHTML+'</div>';
	$('#womenbanner').html(categoriesHTML);
}
