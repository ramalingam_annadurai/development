$(document).ready(function() {
	$("#feed").validate({
		rules : {
			email : {
				required : true,
				email:true
			},
			number : {
				//required : true,
				minlength:10,
				maxlength:10
			},
			category : {
				//required : true,
			},
			ratings : {
				//required : true,
			},
			message : {
				required : true,
				minlength : 3,
				maxlength : 500

		},
		messages : {
			email : {
				required : "You can't leave this empty!",
				minlength : "Minimum 3 characters required!",
				maxlength : "Maximum 20 characters allowed!",
				email:"Enter valid name",
				lettersonly:jQuery.format("letters only mate")
			},
			number : {
				required : "You can't leave this empty!",
				minlength : 'Minimum 10 character is required',
				maxlength : 'Maximum 10 characters are allowed',
				email:"Enter valid last name"	
			},
			category:{
				required:"You can't leave this empty!",
				email:"Enter valid email"
			},
			ratings:{
				required:"You can't leave this empty!",
				
			},
			message : {
				required : "You can't leave this empty!",
				minlength : 'Minimum 3 character is required',
				maxlength : 'Maximum 50 characters are allowed'
			}
		}
		}
	});
});

$(document).ready(function () {
	  //called when key is pressed in textbox
	  $("#mobileNo").keypress(function (e) {
	     //if the letter is not digit then display error and don't type anything
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        //display error message
	        $("#numloc").html("Digits Only").show().fadeOut("slow");
	               return false;
	    }
	   });
	});

$(document).ready(function () {
$('#firstName').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    else
    {
    e.preventDefault();
    $("#nameloc").html("Alphabets Only").show().fadeOut("slow");
    return false;
    }
});
});
$(document).ready(function () {
	$('#lastName').keypress(function (e) {
	    var regex = new RegExp("^[a-zA-Z]+$");
	    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	    if (regex.test(str)) {
	        return true;
	    }
	    else
	    {
	    e.preventDefault();
	    $("#lastloc").html("Alphabets Only").show().fadeOut("slow");
	    return false;
	    }
	});
	});