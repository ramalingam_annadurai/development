$(document).ready(function() {
	$("#loginForm").validate({
		rules : {
			firstName : {
				required : true,
				minlength : 3,
				maxlength : 50
			},
			lastName : {
				required : true,
				minlength : 1,
				maxlength : 50
			},
			email : {
				required : true,
				email:true
			},
			mobileNo : {
				required : true,
				minlength:10,
				maxlength:10
			},
			permanentAddress : {
				required : true,
				minlength : 5,
				maxlength : 50
			},
			shippingAddress : {
				required : true,
				minlength : 5,
				maxlength : 50
			},
			password : {
				required : true,
				minlength : 6,
				maxlength : 15
			},
			confirmPassword : {
				required : true,
				
				equalTo:'#password'
			}

		},
		messages : {
			firstName : {
				required : "You can't leave this empty!",
				minlength : "Minimum 3 characters required!",
				maxlength : "Maximum 20 characters allowed!",
				email:"Enter valid name",
				lettersonly:jQuery.format("letters only mate")
			},
			lastName : {
				required : "You can't leave this empty!",
				minlength : "Minimum 1 characters required!",
				maxlength : "Maximum 50 characters allowed!",
				email:"Enter valid last name"	
			},
			email:{
				required:"You can't leave this empty!",
				email:"Enter valid email"
			},
			mobileNo:{
				required:"You can't leave this empty!",
				minlength : 'Minimum 10 character is required',
				maxlength : 'Maximum 10 characters are allowed'
			},
			permanentAddress : {
				required : "You can't leave this empty!",
				minlength : 'Minimum 5 character is required',
				maxlength : 'Maximum 50 characters are allowed'
			},
			shippingAddress : {
				required : "You can't leave this empty!",
				minlength : "Minimum 5 characters required!",
				maxlength : "Maximum 50 characters allowed!"
			},
			password : {
				required : "You can't leave this empty!",
				minlength : "Minimum 6 characters required!",
				maxlength : "Maximum 10 characters allowed!"
			},
			confirmPassword : {
				required : "You can't leave this empty!",
				email:"Enter correct password"
				
			},
		}
	});
});

$(document).ready(function () {
	  //called when key is pressed in textbox
	  $("#mobileNo").keypress(function (e) {
	     //if the letter is not digit then display error and don't type anything
	     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
	        //display error message
	        $("#numloc").html("Digits Only").show().fadeOut("slow");
	               return false;
	    }
	   });
	});

$(document).ready(function () {
$('#firstName').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }
    else
    {
    e.preventDefault();
    $("#nameloc").html("Alphabets Only").show().fadeOut("slow");
    return false;
    }
});
});
$(document).ready(function () {
	$('#lastName').keypress(function (e) {
	    var regex = new RegExp("^[a-zA-Z]+$");
	    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	    if (regex.test(str)) {
	        return true;
	    }
	    else
	    {
	    e.preventDefault();
	    $("#lastloc").html("Alphabets Only").show().fadeOut("slow");
	    return false;
	    }
	});
	});