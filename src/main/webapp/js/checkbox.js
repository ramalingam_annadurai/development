$(document).ready(function($) {
	$("input[name='checkbox']").click(function() {
		if ($(this).is(':checked')) {
			$('input.textbox:text').attr("disabled", false);
		} else if ($(this).not(':checked')) {
			var ok = confirm('Are you sure you want to remove all data?');
			if (ok) {
				var remove = '';
				$('input.textbox:text').attr('value', remove);
				$('input.textbox:text').attr("disabled", true);
			}
		}
	});
});